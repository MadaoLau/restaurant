export class HttpError extends Error {
    constructor(public message: string, public status: number) {
        super()
    }
}

//beeno's throw error use as usual
//We can use this for "throw new HttpError("....", 404)" or just res.status(400)