export interface DishItem {
  id: number;
  name: string;
  price: string;
  img?: string;
  category: string;
}

export interface User {
  id: number;
  username: string;
  password_hash: string;
  gender?: string;
  is_admin: boolean;
  status?: string;
  contact?: string;
  shift?: string;
}

export interface Store {
  id: number;
  name: string;
}

// In James SW code on FRD009
interface ReqUser {
  id: number;
  username: string;
  is_admin: boolean;
}
declare global {
  namespace Express {
    interface Request {
      user?: ReqUser;
    }
  }
}

export type NewDish = {
  name: string;
  price: number;
  img: string;
  category: string;
};

export type newOrder = {
  table_no?: string;
  status?: number;
  quantity?: string;
  discount?: string;
  amount: number;
  dish_id: number;
  order: number;
  price: number;
  id: number;
  // created_at: string;
  // updated_at: string;
};
