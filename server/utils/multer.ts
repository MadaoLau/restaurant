import aws from "aws-sdk";
import multer from "multer";
import multerS3 from "multer-s3";

export const iniUpload = () => {
  const s3 = new aws.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: "ap-southeast-1",
  });

  return multer({
    storage: multerS3({
      s3: s3,
      bucket: "cdn.tecky.hk",
      metadata: (req, file, cb) => {
        cb(null, { fieldName: file.fieldname });
      },
      key: (req, file, cb) => {
        cb(
          null,
          `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`
        );
      },
    }),
  });
};
