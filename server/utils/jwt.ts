//Use beeno's example in FRD009, related to .env for security (Tecky CMS not do this, only hard code the jwtSecret)

import env from "../env";

export let jwtConfig = {
    jwtSecret: env.JWT_SECRET,
    jwtSession: {
        session: false
    }
}