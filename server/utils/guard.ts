import express from "express";
import { Bearer } from "permit";
import jwtSimple from "jwt-simple";
import { userService } from "../routers/userRoutes";
import { jwtConfig } from "./jwt";
// import { User } from './models';

//Almost same as Tecky CMS

const permit = new Bearer({
  query:"access_token"
});

//checking if is login or not, special function permission for staff or admin
export async function isLoggedIn(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const token = permit.check(req);
    if (!token) {
      return res.status(401).json({ message: "Permission Denied, no token" });
    }
    const payload = jwtSimple.decode(token, jwtConfig.jwtSecret!);
    const user = await userService.getUserByUsername(payload.username);
    if (user) {
      req.user = user;
      return next();
    } else {
      return res.status(401).json({ message: "Permission Denied, no user" });
    }
  } catch (e) {
    return res.status(401).json({ message: "Permission Denied, wrong token", e });
  }
}

export async function isAdmin(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  try {
    const token = permit.check(req);
    if (!token) {
      return res.status(401).json({ message: "Permission Denied, no token" });
    }
    const payload = jwtSimple.decode(token, jwtConfig.jwtSecret!);
    //payload to user
    const user = await userService.getUserByUsername(payload.username);
    if (user?.is_admin == true) {
      req.user = user;
      return next();
    } else {
      return res.status(401).json({ message: "Permission Denied, no user" });
    }
  } catch (e) {
    return res.status(401).json({ message: "Permission Denied, wrong token", e });
  }
}
