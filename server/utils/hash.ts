import bcrypt from "bcryptjs";

//Almost same as Tecky CMS 
//bcrypt the password for protection

let ROUNDS = 12;

export async function hashPassword(password: string) {
  const hash = await bcrypt.hash(password, ROUNDS);
  return hash;
}

export async function checkPassword(password: string, hashPassword: string):Promise<boolean>
{
  const isMatch:boolean = await bcrypt.compare(password, hashPassword);
  return isMatch;
}