import { Knex } from "knex";
import { DishItem, newOrder } from "../utils/models";

export class OrderService {
  constructor(private knex: Knex) {}

  async getAllDishes() {
    const result = await this.knex<DishItem>("dish").select("*");
    return result;
  }

  async sendOrder(newOrder: newOrder[], tableNo: string, clientId: string) {
    //do some logic to match database;s structure
    // let resClientNo:any
    // try {
    //   let a:any = await this.knex("client_id").select("*").where("clientId",clientId)
    //   console.log("aaaaaaaaaa:"+a)
    //   if(!a){
    //     let clientResult:any = await this.knex("client_id").insert({clientId:clientId}).returning("id")
    //     resClientNo = clientResult.rows[0].id
    //   }else{
    //     resClientNo = a[0].id
    //   }
    // } catch (error) {
    //   return({ message: "Post: clinetId Database Error" })
    // }
    try {
      let tableNoData = await this.knex("table_no")
        .select("id")
        .where("id", +tableNo)
        .returning("id");
      let resTableNo = tableNoData[0].id;
      let resOrder;
      for (let item of newOrder) {
        resOrder = await this.knex("orders")
          .insert({
            table_no: resTableNo,
            status: false,
            quantity: item.order,
            discount: null,
            amount: item.price,
            dish_id: item.id,
            client_id: clientId,
          })
          .returning("*");
      }
      return resOrder;
    } catch (error) {
      console.log(`error: `, error);
      return false;
    }
  }

  async insertSpecialReq(ordersId: number, specialListId: number) {
    await this.knex("special_req").insert({
      orders_id: ordersId,
      special_list_id: specialListId,
    });
  }

  async getSpecialReq() {
    const result = (
      await this.knex.raw(`SELECT req,client_id,table_no FROM orders
      JOIN special_req ON orders.id = special_req.orders_id
      JOIN special_list ON special_req.special_list_id = special_list.id
    `)
    ).rows;
    return result;
  }

  async getTargetSpecialReq(client_id: string) {
    const result = (
      await this.knex.raw(
        `SELECT req FROM orders
    JOIN special_req ON orders.id = special_req.orders_id
    JOIN special_list ON special_req.special_list_id = special_list.id
    WHERE orders.client_id =?`,
        [client_id + ""]
      )
    ).rows;
    return result;
  }

  // async getOrderByKitchen() {
  // const result = await this.knex<newOrder>("orders")
  //   .select("*","dish.name","table_no.id as table")
  //   .leftJoin("table_no", "table_no.id", "orders.table_no")
  //   .leftJoin("dish", "dish.id", "orders.dish_id")
  //   // .leftJoin("client_id","client_id.id", "orders.clientId")
  //   .where("status", false)

  async getOrderByKitchen() {
    const result = (
      await this.knex.raw(
        `SELECT table_no.table_no, orders.client_id, dish.name, dish.id, orders.updated_at as time, SUM(orders.quantity) as total_quantity
        FROM orders
        LEFT JOIN table_no ON table_no.id = orders.table_no
        LEFT JOIN dish ON dish.id = orders.dish_id
        WHERE orders.status = false
        GROUP BY table_no.table_no, orders.client_id, dish.name, dish.id, orders.updated_at`
      )
    ).rows;

    return result;
  }

  async getFinishOrderByKitchen() {
    const result = (
      await this.knex.raw(
        `SELECT table_no.table_no, orders.client_id, dish.name, dish.id, SUM(orders.quantity) as total_quantity
      FROM orders
      LEFT JOIN table_no ON table_no.id = orders.table_no
      LEFT JOIN dish ON dish.id = orders.dish_id
      WHERE orders.status = true
      GROUP BY table_no.table_no, orders.client_id, dish.name, dish.id`
      )
    ).rows;
    return result;
  }

  async finishOrder(clientId: string) {
    //do some logic to match database;s structure
    try {
      const stat = await this.knex("orders")
        .update({
          status: true,
        })
        .where("client_id", clientId)
        .returning("status");
      return stat;
    } catch (error) {
      console.log(`error: `, error);
      return false;
    }
  }

  async getOrderByClient(clientId: string) {
    const result = (
      await this.knex.raw(
        `SELECT table_no.table_no, orders.client_id, dish.name, dish.id, orders.amount, orders.amount*SUM(orders.quantity) as total_amount, SUM(orders.quantity) as total_quantity
      FROM orders
      LEFT JOIN table_no ON table_no.id = orders.table_no
      LEFT JOIN dish ON dish.id = orders.dish_id
      WHERE orders.client_id = ?
      GROUP BY table_no.table_no, orders.client_id, dish.name, dish.id, orders.amount`,
        [clientId + ""]
      )
    ).rows;

    // const result = await this.knex("orders")
    //   .sum("orders.quantity as total_quantity")
    //   .select("*","table_no.table_no", "dish.name")
    //   .leftJoin("table_no", "table_no.id","orders.table_no")
    //   .leftJoin("dish","dish.id","orders.dish_id")
    //   .where("client_id", clientId)
    //   .groupBy("table_no.table_no","dish.name","orders.id")
    //   .returning("*")

    // const result = (await this.knex.raw(/*sql*/
    //   `SELECT table_no.table_no, dish.name, dish.id, SUM(orders.quantity) as total_quantity
    //   FROM orders
    //   LEFT JOIN table_no ON table_no.id = orders.table_no
    //   LEFT JOIN dish ON dish.id = orders.dish_id
    //   WHERE client_id = $1
    //   GROUP BY table_no.table_no, dish.name, dish.id`,[clientId]
    // )).rows
    return result;
  }

  async deleteOrder(clientId: number) {
    await this.knex("orders").where("client_id", clientId).del();
  }

  async deleteOrderByEachDish(clientId: number, dishId: number) {
    await this.knex("orders").where("client_id", clientId).where("dish_id", dishId).del();
  }

  async insertSpecialListGetId(specialReq: string) {
    const special_listId = await this.knex("special_list").insert({ req: specialReq }).returning("id");
    return special_listId;
  }
}
