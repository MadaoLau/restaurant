import { Knex } from "knex";
import { hashPassword } from "../utils/hash";
import { User } from "../utils/models";


export class UserService {
  constructor(private knex: Knex) {}
//check login use
  async getUserByUsername(username: string) {
    const user = await this.knex<User>("users")
      .where({ username: username })
      .first();
    return user;
  }

  async getEmailByInputEmail(email:string){
    const row = await this.knex('users').where("email", email).first();
    return row
  }

  async register(user: any) {
    // let rows: any[] = 
    await this.knex("users")
      .insert({
        username: user.username,
        password_hash: await hashPassword(user.password),
        email: user.email,
        gender: user.gender,
        contact: user.contact
      })
      .returning('id')
    // let id = rows[0].id as number
    // let token: string = this.createToken({ id, username: user.username })
    // return rows
  }

  async getStaffData() {
    const user = await this.knex<User>("users").select("id", "username", "email", "gender", "status", "contact", "shift", "is_admin");
    return user;
  }

  async getStaffDataById(staffId:number) {
    const user = await this.knex<User>("users").where("id",staffId).select("id", "username", "email", "gender", "status", "contact", "shift", "is_admin");
    return user;
  }

  async changeStaffData(user: any, staffId:number){
    const result = await this.knex("users").update({
      "username":user.username,
      "email":user.email,
      "gender":user.gender,
      "status":user.status,
      "contact":user.contact,
      "shift":user.shift,
      "is_admin":user.is_admin
    }).where("id", staffId);
    return result;
  }

  async deleteStaffData(staffId:number){
    await this.knex<User>("users").where("id",staffId).del();
  }

}