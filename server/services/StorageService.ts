import { Store } from "express-session";
import { Knex } from "knex";

export class StorageService {
    constructor(private knex: Knex) {}
  //check login use
    async getStore() {
      const store = await this.knex<Store>("store_item");
      return store;
    }
  }