import { Knex } from "knex";

export class FeedbackService {
  constructor(private knex: Knex) {}

  async getAllFeedback() {
    const result = await this.knex("feedback").select("*").orderBy('id','desc');
    return result;
  }

  async getTotalAnalysis(){
    const totalAnalysisScore = 
      await this.knex("feedback")
      .sum({sum_food_quality:"food_quality"})
      .sum({sum_service:"service"})
      .sum({sum_cleanness:"cleanness"})
      .avg({avg_food_quality:"food_quality"})
      .avg({avg_service:"service"})
      .avg({avg_cleanness:"cleanness"})
      // console.log("a:",totalAnalysisScore)
      return totalAnalysisScore
  }

  async sortingFeedback(sort:string){
    let res;
    if(sort == '0' || sort == '1'){
      res = await this.knex("feedback").select("*").orderBy('id', 'desc');
    }else if (sort == '2'){
      res = await this.knex("feedback").select("*").orderBy('id');
    }else if (sort == '3'){
      res = await this.knex("feedback").select("*").orderBy('food_quality', 'desc');
    }else if (sort == '4'){
      res = await this.knex("feedback").select("*").orderBy('food_quality');
    }else if (sort == '5'){
      res = await this.knex("feedback").select("*").orderBy('service', 'desc');
    }else if (sort == '6'){
      res = await this.knex("feedback").select("*").orderBy('service');
    }else if (sort == '7'){
      res = await this.knex("feedback").select("*").orderBy('cleanness', 'desc');
    }else if (sort == '8'){
      res = await this.knex("feedback").select("*").orderBy('cleanness');
    }
    console.log(res)
    return res
  }

  async selectRatingFeedback(select:string){
    let res;
    if(select == '1'){
      res = await this.knex("feedback").select("food_quality").orderBy('food_quality', 'desc');
    }else if (select == '2'){
      res = await this.knex("feedback").select("food_quality").orderBy('food_quality');
    }else if (select == '3'){
      res = await this.knex("feedback").select("service").orderBy('service', 'desc');
    }else if (select == '4'){
      res = await this.knex("feedback").select("service").orderBy('service');
    }else if (select == '5'){
      res = await this.knex("feedback").select("cleanness").orderBy('cleanness', 'desc');
    }else if (select == '6'){
      res = await this.knex("feedback").select("cleanness").orderBy('cleanness');
    }else if (select == '7'){
      res = await this.knex("feedback").select("suggestion");
    }
    return res
  }

  // async sortingFeedback(sort:string){
  //     if(sort == '0' || sort == '1'){
  //       const res = await this.knex("feedback").select("*").orderBy('id','desc').returning('*');
  //       let result = res[0];
  //       return result;
  //     }else if(sort == '2'){
  //       const res = await this.knex("feedback").select("*").orderBy('id').returning('*');
  //       let result = res[0];
  //       return result;
  //     }else if(sort == '3'){
  //       const res = await this.knex("feedback").select("*").orderBy('food_quality','desc').returning('*');
  //       let result = res[0];
  //       return result;
  //     }else if(sort == '4'){
  //       const res = await this.knex("feedback").select("*").orderBy('food_quality').returning('*');
  //       let result = res[0];
  //       return result;
  //     }
  // }

  async postFeedback(feedbackData:any) {
    try {
      const result = await this.knex("feedback").insert({
        food_quality: feedbackData.food_quality,
        service: feedbackData.service,
        cleanness: feedbackData.cleanness,
        suggestion: feedbackData.suggestion,
        client_id: feedbackData.client_id
      });
    return result;

    } catch (error) {
      console.log(`error: `, error);
      return false;
    }
  }

  async deleteFeedbackById(id:number){
    await this.knex("feedback").where("id",id).del();
  }

  // async getOrderByKitchen(){
  //     const result = (await this.knex.raw(
  //       `SELECT table_no.table_no, orders.client_id, dish.name, dish.id, SUM(orders.quantity) as total_quantity
  //       FROM orders
  //       LEFT JOIN table_no ON table_no.id = orders.table_no
  //       LEFT JOIN dish ON dish.id = orders.dish_id
  //       WHERE orders.status = false
  //       GROUP BY table_no.table_no, orders.client_id, dish.name, dish.id`
  //     )).rows

  //     return result
  // }

  // async getFinishOrderByKitchen(){
  //   const result = (await this.knex.raw(
  //     `SELECT table_no.table_no, orders.client_id, dish.name, dish.id, SUM(orders.quantity) as total_quantity
  //     FROM orders
  //     LEFT JOIN table_no ON table_no.id = orders.table_no
  //     LEFT JOIN dish ON dish.id = orders.dish_id
  //     WHERE orders.status = true
  //     GROUP BY table_no.table_no, orders.client_id, dish.name, dish.id`
  //   )).rows

  //   return result
  // }

  // async getOrderByClient(clientId:string){

  //   const result = (await this.knex.raw(
  //     `SELECT table_no.table_no, orders.client_id, dish.name, dish.id, orders.amount, orders.amount*SUM(orders.quantity) as total_amount, SUM(orders.quantity) as total_quantity
  //     FROM orders
  //     LEFT JOIN table_no ON table_no.id = orders.table_no
  //     LEFT JOIN dish ON dish.id = orders.dish_id
  //     WHERE orders.client_id = ?
  //     GROUP BY table_no.table_no, orders.client_id, dish.name, dish.id, orders.amount`,[clientId+""]
  //   )).rows
  //   return result
  // }
}

