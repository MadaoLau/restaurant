import { Knex } from "knex";
import { DishItem } from "../utils/models";

export class ManageService {
  constructor(private knex: Knex) {}

  dishTable() {
    return this.knex("dish");
  }
  async getAllDish() {
    let allDishes = await this.knex<DishItem>("dish")
      .select("id", "name", "price", "img", "category")
      .orderBy("id", "ASC");
    return allDishes;
  }

  async addDish(name: string, price: string, img: string, category: string) {
    await this.dishTable().insert({
      name: name,
      price: price,
      img: img,
      category: category,
    });
  }

  async getLatestDish(){
    let latestDish = await this.dishTable().first().orderBy("created_at", "DESC").limit(1)
    console.log(latestDish);
  }

  // async addDish(newDish: NewDish) {
  //   let newDishId = await this.dishTable().insert(newDish).returning("id");
  //   return newDishId[0].id;
  // }

  async updateName(id: number, name: string) {
    await this.dishTable().update({ name }).where({ id: id });
  }

  async updatePrice(id: number, price: number) {
    await this.dishTable().update({ price }).where({ id: id });
  }

  async updateCategory(id: number, category: string) {
    await this.dishTable().update({ category }).where({ id: id });
  }

  async updateImg(id: number, img: string) {
    await this.dishTable().update({ img }).where({ id: id });
  }

  async deleteDish(id: number) {
    await this.dishTable().delete().where({ id: id });
  }
}
