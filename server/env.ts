import dotenv from "dotenv";
dotenv.config();

const env = {
  DB_NAME: process.env.DB_NAME || "restpos",
  DB_USER: process.env.DB_USER || "restpos",
  DB_PASSWORD: process.env.DB_PASSWORD || "restpos",
  SERVER_PORT: process.env.SERVER_PORT || 8000,
  NODE_ENV: process.env.NODE_ENV || "development",
  SESSION_SECRET: process.env.SESSION_SECRET,
  JWT_SECRET: process.env.JWT_SECRET || "restpos1234"
};

export default env;
