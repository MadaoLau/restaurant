import env from "./env";
import express from "express";
import { logger } from "./utils/logger";
import cors from "cors";
import config from "./knexfile";
import Knex from "knex";
import multer from "multer";
import path from "path";
import http from "http";
import { Server as SocketIO } from "socket.io";

export const knex: any = Knex(config[env.NODE_ENV || "development"]);

const app = express();
//multer
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve('./uploads'));
  },
  filename: function (req, file, cb) {
    console.log('multer handling file', file);
    cb(null, `${file.originalname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
  }
})
export const upload = multer({storage})

app.use(express.static("uploads"))

// var server = http.createServer(app);
const server = new http.Server(app);
export const io = new SocketIO(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  },
});

//cors library help to connect 2 api (between front and back end)
app.use(cors());
//接駁json 睇req.body
app.use(express.json());
//接駁urlencoded req.body
app.use(express.urlencoded({ extended: true }));

// app.use(express.static("uploads"));
import { routes } from "./routers/routes";

const API_VERSION = "/api";
app.use(API_VERSION, routes);
// 完整route流程:  /api -> routes -> orderRoutes -> OrderController -> OrderService
// demo path:     localhost:8000/api/order/
app.use(express.static(path.join(__dirname, "public")));
//開server
server.listen(env.SERVER_PORT, () => {
  // deploy後就會係度show deploy info
  logger.info(`Listening at: http://localhost:${env.SERVER_PORT}`);
});
