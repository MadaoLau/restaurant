import { Knex } from "knex";
import { hashPassword } from "../utils/hash";

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex.raw(/*sql*/ `TRUNCATE users RESTART IDENTITY CASCADE`);
  await knex.raw(/*sql*/ `TRUNCATE store_item RESTART IDENTITY CASCADE`);
  await knex.raw(/*sql*/ `TRUNCATE table_no RESTART IDENTITY CASCADE`);
  await knex.raw(/*sql*/ `TRUNCATE feedback RESTART IDENTITY CASCADE`);
  await knex.raw(/*sql*/ `TRUNCATE orders RESTART IDENTITY CASCADE`);
  await knex.raw(/*sql*/ `TRUNCATE special_list RESTART IDENTITY CASCADE`);
  await knex.raw(/*sql*/ `TRUNCATE special_req RESTART IDENTITY CASCADE`);

  //Default Table No
  await knex
    .insert([
      { table_no: 1 },
      { table_no: 2 },
      { table_no: 3 },
      { table_no: 4 },
      { table_no: 5 },
      { table_no: 6 },
      { table_no: 7 },
      { table_no: 8 },
      { table_no: 9 },
      { table_no: 10 },
      { table_no: 11 },
      { table_no: 12 },
      { table_no: 13 },
      { table_no: 14 },
      { table_no: 15 },
      { table_no: 16 },
      { table_no: 17 },
      { table_no: 18 },
      { table_no: 19 },
      { table_no: 20 },
    ])
    .into("table_no");

  //Default one Boss & one Staff
  await knex
    .insert([
      {
        username: "Admin",
        email: "admin@gmail.com",
        password_hash: await hashPassword("admin"),
        gender: "M",
        is_admin: true,
        status: "On",
        contact: "51234678",
        shift: "08:00-23:00",
      },
      {
        username: "Bob",
        email: "bob@gmail.com",
        password_hash: await hashPassword("bob"),
        gender: "M",
        is_admin: true,
        status: "Off",
        contact: "55556666",
        shift: "08:00-23:00",
      },
      {
        username: "Terence",
        password_hash: await hashPassword("1234"),
        email: "terence@gmail.com",
        gender: "M",
        is_admin: false,
        status: "On",
        contact: "58907634",
        shift: "14:00-23:00",
      },
      {
        username: "Alice",
        password_hash: await hashPassword("4321"),
        email: "alice@gmail.com",
        gender: "F",
        is_admin: false,
        status: "On",
        contact: "52678943",
        shift: "08:30-17:30",
      },
      {
        username: "Daigo",
        password_hash: await hashPassword("7890"),
        email: "daigo@gmail.com",
        gender: "M",
        is_admin: false,
        status: "On",
        contact: "51112222",
        shift: "11:00-20:00",
      },
    ])
    .into("users");
}
