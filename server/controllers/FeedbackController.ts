import { FeedbackService } from "../services/FeedbackService";
import { Request, Response } from "express";

export class FeedbackController {
  constructor(private feedbackService: FeedbackService) {}

  getAllFeedback = async (req: Request, res: Response) => {
    try {
      const allFeedback = await this.feedbackService.getAllFeedback();
      if (!allFeedback) {
        res.status(400).json({ message: "Feedback not found" });
      } else {
        res.json(allFeedback);
      }
    } catch (error) {
      console.log(`Catch error`, error.message);
      res.status(500).json({ message: "Feedback Internal server error" });
    }
  };

  getTotalAnalysis = async (req:Request, res:Response)=>{
    try {
      const score = await this.feedbackService.getTotalAnalysis();
      res.json(score)
    } catch (error) {
      res.status(500).json({ message: "AVG Internal server error" });
    }
  }

  sortingFeedback = async (req: Request, res: Response) => {
    try {
      let sort = req.body.sorting!
      console.log(sort)
      const sortingFeedback = await this.feedbackService.sortingFeedback(sort);
      if (!sortingFeedback) {
        res.status(400).json({ message: "Feedback not found" });
      } else {
      res.json(sortingFeedback);
      }
    } catch (error) {
      console.log(`Catch error`, error.message);
      res.status(500).json({ message: "Feedback Internal server error" });
    }
  };

  selectRatingFeedback = async (req: Request, res: Response) => {
    try {
      let select = req.body.select!
      console.log(select)
      const selectRatingFeedback = await this.feedbackService.selectRatingFeedback(select);
      if (!selectRatingFeedback) {
        res.status(400).json({ message: "Feedback not found" });
      } else {
      res.json(selectRatingFeedback);
      }
    } catch (error) {
      console.log(`Catch error`, error.message);
      res.status(500).json({ message: "selectRatingFeedback Internal server error" });
    }
  };

  postFeedback = async (req: Request, res: Response) => {
    try {
      console.log(req.body);
      // const
      const { food_quality, service, cleanness, suggestion, client_id } = req.body;
      let feedbackData = { food_quality, service, cleanness, suggestion, client_id }
      const postfb = await this.feedbackService.postFeedback(feedbackData);

      if (!postfb) {
        throw new Error("DB error.Missing TableOrder");
      }
      res.json({postfb});
    } catch (error) {
      console.log(`Catch error`, error.message);
      res.status(500).json({ message: "post Internal server error" });
    }
  };

  deleteFeedbackById = async (req: Request, res:Response) => {
    try {
      let clientId = +req.params.id;
      if(!clientId){
          res.status(500).json({message: "Param Id Error"})
          return
      }
      this.feedbackService.deleteFeedbackById(clientId);
      res.json({message:"Delete Order Success"})
    } catch (error) {
        res.status(500).json({message:error + ", Delete Staff Data Server Error"})
    }
  }

  // getOrderByKitchen = async (req: Request, res:Response) => {
  //   try {
  //     const kitchenOrder = await this.feedbackService.getOrderByKitchen();
  //     if(!kitchenOrder){
  //       res.status(400).json({ message: "Order not found" });
  //     }else{
  //       this.io.emit("getOrderKitchen",{kitchenOrder})
  //       res.json(kitchenOrder);
  //     }
  //   } catch (error) {
  //     res.status(500).json({ message: error + "get order kitchen server error" })
  //   }
  // };

  // finishOrder = async (req:Request, res:Response)=>{
  //   try {
  //     const finishedOrder = await this.feedbackService.finishOrder(req.body.clientId);
  //     res.json(finishedOrder)
  //   } catch (error) {
  //     res.status(500).json({ message: "Finish order kitchen server error", error:error })

  //   }
  // }

  // getFinishOrderByKitchen = async (req: Request, res:Response) => {
  //   try {
  //     const kitchenOrder = await this.feedbackService.getFinishOrderByKitchen();
  //     if(!kitchenOrder){
  //       res.status(400).json({ message: "Order not found" });
  //     }else{
  //       this.io.emit("getFinishOrderByKitchen",{kitchenOrder})
  //       res.json(kitchenOrder);
  //     }
  //   } catch (error) {
  //     res.status(500).json({ message: error + "get finish order kitchen server error" })
  //   }
  // };

  // getOrderByClient = async (req: Request, res:Response) => {
  //   try {
  //     let cId = (req.params.id).toString()
  //     const clientOrder = await this.feedbackService.getOrderByClient(cId);
  //     if(!clientOrder){
  //       res.status(400).json({ message: "Order not found" });
  //     }else{
  //       this.io.emit("getClientOrder",{clientOrder})
  //       console.log(clientOrder)
  //       res.json(clientOrder);
  //     }
  //   } catch (error) {
  //     res.status(500).json({ message: "get client order server error" + error})
  //   }
  // };


  // deleteOrderByEachDish = async (req: Request, res:Response) => {
  //   try {
  //     let clientId = +req.body.clientId;
  //     let dishId = req.body.dishId;
  //     if(!clientId){
  //         res.status(500).json({message: "Param Id Error"})
  //         return
  //     }
  //     this.feedbackService.deleteOrderByEachDish(clientId,dishId);
  //     res.json({message:"Delete Order Success"})
  //   } catch (error) {
  //       res.status(500).json({message:error + ", Delete Staff Data Server Error"})
  //   }
  // }
}
