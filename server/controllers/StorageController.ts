import { StorageService } from "../services/StorageService";
import { Request, Response } from "express";
export class StorageController {
    constructor(private storageService: StorageService) { }

    storage = async (req: Request, res:Response) => {
        try {
            const store = await this.storageService.getStore()
            res.json(store)
        } catch (error) {
            res.status(500).json({ message: error + "Server Error" })
        }
    }
    
}