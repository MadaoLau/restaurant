import { OrderService } from "../services/OrderService";
import { Request, Response } from "express";

export class OrderController {
  constructor(private orderService: OrderService, private io: any) {}

  getAllDishes = async (req: Request, res: Response) => {
    try {
      const allDishes = await this.orderService.getAllDishes();
      // console.log(allDishes);
      if (!allDishes) {
        res.status(400).json({ message: "Dish not found" });
      } else {
        res.json(allDishes);
      }
    } catch (error) {
      console.log(`Catch error`, error.message);
      res.status(500).json({ message: "Internal server error" });
    }
  };

  // addNewOrder = async (req: Request, res: Response) => {
  //   try {
  //     const newOrder = req.body.newOrder;
  //     console.log(`newOrder sent from frontend: `, newOrder);
  //     // const tableOrder = await this.orderService.addNewOrder(newOrder, tableNo);
  //     // console.log("controller after addDish, get from db:", tableOrder);
  //     // res.json({ message: "success", newOrder: tableOrder });
  //   } catch (error) {
  //     console.log(`Catch error`, error.message);
  //     res.status(500).json({ message: "Internal server error" });
  //   }
  // };

  sendOrder = async (req: Request, res: Response) => {
    try {
      // console.log(req.body);
      // if(!req.body.specialReq){
      //   req.body.specialReq = "None"
      // }
      const { newOrder, tableNo, clientId, specialReq } = req.body;
      // const { specialReq } = req.body;

      console.log("target req:", specialReq);

      if (!clientId) {
        throw new Error("DB error.Missing clientId");
      }

      if (!tableNo) {
        throw new Error("DB error.Missing tableNo");
      }
      // if (!specialReq) {
      //   console.log("nothing req");
      // } else {
   
        let special_listId = await this.orderService.insertSpecialListGetId(specialReq);
        console.log("special_listId", special_listId);
        console.log("2special_listId", special_listId[0].id);  
      
        if (!special_listId) {
          throw new Error("DB error.Missing special_listId");
        }
  



      const tableOrder = await this.orderService.sendOrder(newOrder, tableNo, clientId);

      if (!tableOrder) {
        throw new Error("DB error.Missing TableOrder");
      }

      await this.orderService.insertSpecialReq(tableOrder[0].id, special_listId[0].id);
      // const userFoodReq = await this.orderService.getSpecialReq(clientId)
      // console.log('userFoodReq:', userFoodReq[0]);

      // console.log("tableOrder:", tableOrder);
      // console.log("2tableOrder:", tableOrder[0].id);

      res.json({ tableOrder });
      // res.json({ hi: "inserted" });
      // }
    } catch (error) {
      console.log(`Catch error`, error.message);
      res.status(500).json({ message: "Internal server error" });
    }
  };

  getOrderByKitchen = async (req: Request, res: Response) => {
    try {
      const kitchenOrder = await this.orderService.getOrderByKitchen();
      
      if (kitchenOrder.length === 0) {
        this.io.emit("getOrderKitchen", { kitchenOrder:[], clientReq:[] });
        res.status(400).json({ message: "Order not found" });
      }
      else {

        console.log("kitchen?", kitchenOrder);

        // const newKitchenOrder = kitchenOrder;
        // const targetClientID = kitchenOrder[0].client_id + "";
        // console.log("targetClientID:", targetClientID);

        const clientReq = await this.orderService.getSpecialReq();
        // if(!clientReq){
        //   continue
        // }
        console.log("clientReq", clientReq);
        // this.io.emit("getOrderKitchen", { kitchenOrder });
        //remark
        this.io.emit("getOrderKitchen", { kitchenOrder,clientReq });

        res.json(kitchenOrder);
      }
    } catch (error) {
      res.status(500).json({ message: error + "get order kitchen server error" });
    }
  };

  finishOrder = async (req: Request, res: Response) => {
    try {
      const finishedOrder = await this.orderService.finishOrder(req.body.clientId);
      res.json(finishedOrder);
    } catch (error) {
      res.status(500).json({ message: "Finish order kitchen server error", error: error });
    }
  };

  getFinishOrderByKitchen = async (req: Request, res: Response) => {
    try {
      const kitchenOrder = await this.orderService.getFinishOrderByKitchen();
      if (!kitchenOrder) {
        res.status(400).json({ message: "Order not found" });
      } else {
        this.io.emit("getFinishOrderByKitchen", { kitchenOrder });
        res.json(kitchenOrder);
      }
    } catch (error) {
      res.status(500).json({ message: error + "get finish order kitchen server error" });
    }
  };

  getOrderByClient = async (req: Request, res: Response) => {
    try {
      let cId = req.params.id.toString();
      const clientOrder = await this.orderService.getOrderByClient(cId);
      const clientReq = await this.orderService.getSpecialReq();
      if (!clientOrder) {
        res.status(400).json({ message: "Order not found" });
      } else {
        // this.io.emit("getClientOrder", { clientOrder });
        //remark
        this.io.emit("getClientOrder", { clientOrder, clientReq });

        console.log("clientId Req", clientReq);
        res.json({ clientOrder, clientReq });

        // res.json({ clientOrder });
      }
    } catch (error) {
      res.status(500).json({ message: "get client order server error" + error });
    }
  };

  deleteOrder = async (req: Request, res: Response) => {
    try {
      let clientId = +req.params.id;
      if (!clientId) {
        res.status(500).json({ message: "Param Id Error" });
        return;
      }
      this.orderService.deleteOrder(clientId);
      res.json({ message: "Delete Order Success" });
    } catch (error) {
      res.status(500).json({ message: error + ", Delete Staff Data Server Error" });
    }
  };

  deleteOrderByEachDish = async (req: Request, res: Response) => {
    try {
      let clientId = +req.body.clientId;
      let dishId = req.body.dishId;
      if (!clientId) {
        res.status(500).json({ message: "Param Id Error" });
        return;
      }
      this.orderService.deleteOrderByEachDish(clientId, dishId);
      res.json({ message: "Delete Order Success" });
    } catch (error) {
      res.status(500).json({ message: error + ", Delete Staff Data Server Error" });
    }
  };
}
