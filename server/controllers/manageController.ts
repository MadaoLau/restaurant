import { ManageService } from "../services/manageService";
import type { Request, Response } from "express";

export class ManageController {
  constructor(private manageService: ManageService) {}

  getAllDish = async (req: Request, res: Response) => {
    try {
      let allDishes = await this.manageService.getAllDish();
      // console.log("controller get from db:", allDishes);
      res.json({ allDishes });
    } catch (error) {
      res.status(500).json({ msg: "cannot get all dish", error });
    }
  };

  addDish = async (req: Request, res: Response) => {
    try {
      let reqData: any = req.body;
      let name: string = reqData.name;
      // let price = +reqData.price;
      let price = reqData.price;
      let img = req.file?.filename as string;
      let category: string = reqData.category;
      // console.log("reqData sent from frontend", reqData);
      // console.log("reqData name:", name);
      console.log("reqData price:", typeof (price));
      console.log("reqData img:", img);
      // console.log("reqData category:", category);
      console.log("newDish req.file:", req.file);
      // console.log("newDish req.file.des:", req.file?.destination);
      // res.json({ msg: "insert with picture " });
      await this.manageService.addDish(name, price, img, category);
      let allDishes = await this.manageService.getAllDish();
      // console.log("controller after addDish, get from db:");
      res.json({ allDishes });
    } catch (error) {
      res.status(400).json({ msg: "cannot insert Dish:", error });
    }
  };

  update = async (req: Request, res: Response) => {
    try {
      let data: any = req.body;
      let id = data.id;
      let newName = data.name;
      // let newPrice = parseInt(data.price);
      let newPrice = data.price;

      if (newPrice == null) {
        newPrice = 0;
      }
      console.log("priceeeeeeeeee", typeof newPrice);
      let newCategory = data.category;
      console.log("data sent from frontend", data);
      if (data.name !== undefined) {
        await this.manageService.updateName(id, newName);
      } else if (data.price !== undefined) {
        await this.manageService.updatePrice(id, newPrice);
      } else if (data.category !== undefined) {
        await this.manageService.updateCategory(id, newCategory);
      }
      res.json({ msg: "update success" });
    } catch (error) {
      res.status(400).json({ msg: "cannot update:", error });
    }
  };

  updateImg = async (req: Request, res: Response) => {
    try {
      let data: any = req.body;
      let id = +data.id;
      let img = req.file?.filename as string;
      console.log("newImg sent from frontend", typeof id);
      console.log("newImg filename sent from frontend", img);
      await this.manageService.updateImg(id, img);
      let latestDish = await this.manageService.getLatestDish();
      console.log("latest:", latestDish);

      res.json({ latestDish });

      // res.json({ msg: "update Img success" });
    } catch (error) {
      res.status(400).json({ msg: "cannot update Img:", error });
    }
  };

  deleteDish = async (req: Request, res: Response) => {
    try {
      let id = req.body.id;
      console.log("id sent from frontend:", id);
      await this.manageService.deleteDish(id);
      res.json({ msg: "delete success" });
    } catch (error) {
      res.status(400).json({ msg: "cannot delete:", error });
    }
  };
}
