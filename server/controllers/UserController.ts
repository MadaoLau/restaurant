import { UserService } from "../services/UserService";
import { Request, Response } from "express";
import { checkPassword } from "../utils/hash";
import { User } from "../utils/models";
import jwtSimple from "jwt-simple";
import { jwtConfig } from "../utils/jwt";

// import { HttpError } from "../utils/http-error";
// import fetch from "node-fetch";

export class UserController {
  constructor(private userService: UserService) {}
  //login system(frontend type username+pw, backend take out username+pw_hash in DB, matching)
  login = async (req: Request, res: Response) => {
    try {
      if (!req.body.username || !req.body.password) {
        res.status(401).json({ msg: "Wrong Username/Password" });
        return;
      }
      const { username, password } = req.body;
      // console.log({username,password})
      const user: User | undefined = await this.userService.getUserByUsername(
        username
      );
      if (!user) {
        res.status(401).json({ message: "Wrong username" });
        return;
      }

      if (!(await checkPassword(password, user.password_hash))) {
        res.status(401).json({ message: "Wrong password" });
        return;
      }
      //token + jwt session
      const payload = { username: user.username, is_admin: user.is_admin };
      const token = jwtSimple.encode(payload, jwtConfig.jwtSecret!);
      res.json({ message:'Login success', token: token });
    } catch (error) {
      res.status(500).json({ message: error + ", Login Server Error" });
    }
  };

  register = async (req: Request, res: Response) => {
    try {
      let { username, password, email, gender, contact } = req.body;
      contact = +contact;
      let nameFindFromDb = await this.userService.getUserByUsername(
        username
      );
      let emailFindFromDb = await this.userService.getEmailByInputEmail(
        email
      );
      console.log("register username find from Db result", nameFindFromDb);
      let user: any = { username, password, email, gender, contact };
      console.log("user data before insert into db:", user);
      if (nameFindFromDb?.username === username) {
        console.log("found:", nameFindFromDb);
        res.json({ message: "username have been used" });
      } else if(emailFindFromDb?.email===email){
        console.log('email found:',emailFindFromDb);
        res.json({message:"email have been used"})
      }else {
        await this.userService.register(user);
        res.json({ message: "Create User Success" });
      }
    } catch (error) {
      res.status(500).json({ message: error + ", Register Server Error" });
    }
    // return { token }
  };

  getStaffData = async (req: Request, res: Response) => {
    try {
      let data = await this.userService.getStaffData();
      console.log(data);
      res.json(data);
    } catch (error) {
      res
        .status(500)
        .json({ message: error + ", Get Staff Data Server Error" });
    }
  };

  changeStaffData = async (req: Request, res: Response) => {
    try {
      let staffId = +req.params.id;
      if (!staffId) {
        res.status(500).json({ message: "Param Id Error" });
        return;
      }
      let row: any = await this.userService.getStaffDataById(staffId);
      console.log("origin", row[0]);
      let origin = row[0];
      let { username, email, gender, status, contact, shift, is_admin } =
        req.body;
      if (!username) {
        username = origin.username;
      }
      if (!email) {
        email = origin.email;
      }
      if (!gender) {
        gender = origin.gender;
      }
      if (!status) {
        status = origin.status;
      }
      if (!contact) {
        contact = origin.contact;
      }
      if (!shift) {
        shift = origin.shift;
      }
      if (!is_admin) {
        is_admin = origin.is_admin;
      }
      let user: any = {
        username,
        email,
        gender,
        status,
        contact,
        shift,
        is_admin,
      };
      await this.userService.changeStaffData(user, staffId);
      res.json({ message: "Update User Success" });
    } catch (error) {
      res
        .status(500)
        .json({ message: error + ", Change Staff Data Server Error" });
    }
  };

  deleteStaffData = async (req: Request, res: Response) => {
    try {
      let staffId = +req.params.id;
      if (!staffId) {
        res.status(500).json({ message: "Param Id Error" });
        return;
      }
      this.userService.deleteStaffData(staffId);
      res.json({ message: "Delete Staff Success" });
    } catch (error) {
      res
        .status(500)
        .json({ message: error + ", Delete Staff Data Server Error" });
    }
  };
}
