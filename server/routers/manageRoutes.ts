import express from "express";
import { ManageService } from "../services/manageService";
import { ManageController } from "../controllers/manageController";
import { knex } from "../server";
import {upload} from '../server'

export const manageService = new ManageService(knex);
const manageController = new ManageController(manageService);

export const manageRoutes = express.Router();
manageRoutes.get("/manageMenu", manageController.getAllDish);
// manageRoutes.post("/manageMenu", manageController.addDish);
manageRoutes.post("/manageMenu",upload.single('img'), manageController.addDish);
manageRoutes.put("/manageMenu/updateImg",upload.single('img'), manageController.updateImg);
manageRoutes.put("/manageMenu", manageController.update);
manageRoutes.delete("/manageMenu", manageController.deleteDish);
