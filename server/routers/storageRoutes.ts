import express from "express";
import { knex } from "../server";
import { StorageService } from "../services/StorageService";
import { StorageController } from "../controllers/StorageController";
import { isLoggedIn } from "../utils/guard";
export const storageService = new StorageService(knex);
const storageController = new StorageController(storageService);

export const storageRoutes = express.Router();
storageRoutes.get("/", isLoggedIn, storageController.storage); //localhost:8080/api/storage