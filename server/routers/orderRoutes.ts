import express from "express";
import { OrderService } from "../services/OrderService";
import { OrderController } from "../controllers/OrderController";
import { knex, io } from "../server";

const orderService = new OrderService(knex);
const orderController = new OrderController(orderService, io);

export const orderRoutes = express.Router();
orderRoutes.get("/", orderController.getAllDishes);
// orderRoutes.post("/", orderController.addNewOrder);
orderRoutes.post("/sendOrder", orderController.sendOrder);
orderRoutes.get("/getOrder", orderController.getOrderByKitchen);
orderRoutes.get("/getFinishOrder", orderController.getFinishOrderByKitchen);
orderRoutes.get("/getClientOrder/:id", orderController.getOrderByClient);
orderRoutes.put("/finishOrder", orderController.finishOrder);
orderRoutes.delete("/deleteOrder/:id", orderController.deleteOrder);
orderRoutes.delete("/deleteOrderByEachDish/:id", orderController.deleteOrderByEachDish);