import express from "express";
import { UserService } from "../services/UserService";
import { UserController } from "../controllers/UserController";
import { knex } from "../server";
export const userService = new UserService(knex);
const userController = new UserController(userService);

export const userRoutes = express.Router();
userRoutes.post("/login", userController.login); //localhost:8080/api/users/login [POST]
userRoutes.post("/register", userController.register);//localhost:8080/api/users/register [POST]
userRoutes.get("/staff", userController.getStaffData);//localhost:8080/api/users/staff [GET]
userRoutes.put("/staff/:id", userController.changeStaffData)//localhost:8080/api/users/staff [PUT]
userRoutes.delete("/staff/:id", userController.deleteStaffData)//localhost:8080/api/users/staff [PUT]
// userRoutes.post("/loginFacebook", userController.loginFacebook); //localhost:8080/api/users/loginFacebook [POST]