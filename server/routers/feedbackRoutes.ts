import express from "express";
import { FeedbackController } from "../controllers/FeedbackController";
import { FeedbackService } from "../services/FeedbackService";
import { knex } from "../server";


const feedbackService = new FeedbackService(knex);
const feedbackController = new FeedbackController(feedbackService);

export const feedbackRoutes = express.Router();
feedbackRoutes.get("/", feedbackController.getAllFeedback);
feedbackRoutes.get("/score", feedbackController.getTotalAnalysis);
feedbackRoutes.post("/", feedbackController.postFeedback);
feedbackRoutes.post("/sorting", feedbackController.sortingFeedback);
feedbackRoutes.post("/select", feedbackController.selectRatingFeedback);
feedbackRoutes.delete("/", feedbackController.deleteFeedbackById);