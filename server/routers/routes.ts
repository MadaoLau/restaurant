import express from "express";
import { feedbackRoutes } from "./feedbackRoutes";
import { manageRoutes } from "./manageRoutes";
import { orderRoutes } from "./orderRoutes";
import { storageRoutes } from "./storageRoutes";
import { userRoutes } from "./userRoutes";

export const routes = express.Router();

// localhost:8000/api/order/getOrder
routes.use("/order", orderRoutes); // localhost:8000/api/order
routes.use("/user", userRoutes); // localhost:8000/api/user
routes.use("/feedback", feedbackRoutes); //localhost:8000/api/feedback
routes.use("/storage", storageRoutes);
routes.use("/", manageRoutes); // localhost:8000/api/manageMenu