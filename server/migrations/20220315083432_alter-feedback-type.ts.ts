import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("feedback")){
        await knex.schema.alterTable("feedback",(table)=>{
            table.integer('food_quality').alter();
            table.integer('service').alter();
            table.integer('cleanness').alter();
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    if(await knex.schema.hasTable("feedback")){
        await knex.schema.alterTable("feedback",(table)=>{
            table.string('food_quality').alter();
            table.string('service').alter();
            table.string('cleanness').alter();
        })
    }
}

