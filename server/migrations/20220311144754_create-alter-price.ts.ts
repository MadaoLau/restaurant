import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if(await knex.schema.hasColumn("dish","price")){
        await knex.schema.alterTable("dish",(table)=>{
            table.string("price", 10).notNullable().alter();
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    if(await knex.schema.hasColumn("dish","price")){
        await knex.schema.alterTable("dish",(table)=>{
            table.integer("price").notNullable().alter();
        })
    }
}

