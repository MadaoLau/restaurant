import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable("special_list")) {
    await knex.schema.alterTable("special_list", (table) => {
      table.string("req").nullable().alter();
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  if (await knex.schema.hasTable("special_list")) {
    await knex.schema.alterTable("special_list", (table) => {
      table.string("req").notNullable().alter();
    });
  }
}
