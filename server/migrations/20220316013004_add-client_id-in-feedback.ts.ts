import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  if (!(await knex.schema.hasColumn("feedback", "client_id"))) {
    await knex.schema.alterTable("feedback", (table) => {
      table.string("client_id").nullable();
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  if (await knex.schema.hasColumn("feedback", "client_id")) {
    await knex.schema.alterTable("feedback", (table) => {
      table.dropColumn("client_id");
    });
  }
}
