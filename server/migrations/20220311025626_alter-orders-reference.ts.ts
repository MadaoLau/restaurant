import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (!(await knex.schema.hasColumn("orders","client_id"))) {
        await knex.schema.alterTable("orders", (table) => {
            table.integer("client_id").nullable();
        });
    }

    if (await knex.schema.hasColumn("orders","table_no")) {
        await knex.schema.alterTable("orders", (table) => {
            table.integer("table_no").nullable().references("table_no.id").alter();
        });
    }
    // if (await knex.schema.hasColumn("orders","table_no")) {
    //     await knex.schema.alterTable("orders", (table) => {
    //         table.integer("table_no").nullable().references("table_no.id").alter();
    //     });
    // }
    
}


export async function down(knex: Knex): Promise<void> {
    if (await knex.schema.hasColumn("orders","client_id")) {
        await knex.schema.alterTable("orders", (table) => {
            table.dropColumn("client_id");
        });
    }

    if (await knex.schema.hasColumn("orders","table_no")) {
    await knex.schema.alterTable("orders", (table) => {
        table.dropForeign("table_no","orders_table_no_foreign");
    });
}
}

