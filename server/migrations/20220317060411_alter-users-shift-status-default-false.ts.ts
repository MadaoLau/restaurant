import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable("users")) {
        await knex.schema.alterTable("users", (table) => {
          table.string("status").nullable().defaultTo("Off").alter();
          table.string("shift", 30).nullable().defaultTo("Off").alter();
        });
      }
}


export async function down(knex: Knex): Promise<void> {
    if (await knex.schema.hasTable("users")) {
        await knex.schema.alterTable("users", (table) => {
          table.string("status").nullable().alter();
          table.string("shift", 30).nullable().alter();
        });
      }
}

