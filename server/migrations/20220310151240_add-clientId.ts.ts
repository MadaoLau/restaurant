import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    if (!(await knex.schema.hasTable("table_no"))) {
        await knex.schema.createTable("table_no", (table) => {
            table.increments("id");
            table.string("table_no", 10).notNullable();
            table.timestamps(false, true);
        });
    }

}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("table_no");
}
