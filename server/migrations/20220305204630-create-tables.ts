import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  if (!(await knex.schema.hasTable("users"))) {
    await knex.schema.createTable("users", (table) => {
      table.increments("id");
      table.string("username", 64).notNullable().unique();
      table.string("email", 255).nullable().unique();
      table.string("password_hash", 255).notNullable();
      table.string("gender").nullable();
      table.boolean("is_admin").notNullable().defaultTo("false");
      table.string("status").nullable();
      table.integer("contact").nullable();
      table.string("shift", 30).nullable();
      table.timestamps(false, true);
    });
  }

  if (!(await knex.schema.hasTable("dish"))) {
    await knex.schema.createTable("dish", (table) => {
      table.increments("id");
      table.string("name", 50).notNullable();
      table.string("price", 10).notNullable();
      table.string("img", 255).nullable();
      table.string("category", 20).notNullable();
      table.timestamps(false, true);
    });
  }

  if (!(await knex.schema.hasTable("store_item"))) {
    await knex.schema.createTable("store_item", (table) => {
      table.increments("id");
      table.string("name", 50).notNullable();
      table.timestamps(false, true);
    });
  }

  if (!(await knex.schema.hasTable("special_list"))) {
    await knex.schema.createTable("special_list", (table) => {
      table.increments("id");
      table.string("req").notNullable();
      table.timestamps(false, true);
    });
  }

  if (!(await knex.schema.hasTable("feedback"))) {
    await knex.schema.createTable("feedback", (table) => {
      table.increments("id");
      table.string("food_quality").notNullable();
      table.string("service").notNullable();
      table.string("cleanness").notNullable();
      table.string("suggestion").notNullable();
      table.timestamps(false, true);
    });
  }

  if (!(await knex.schema.hasTable("orders_detail"))) {
    await knex.schema.createTable("orders_detail", (table) => {
      table.increments("id");
      table.timestamp("orders_date").notNullable();
      table.timestamp("arrived_date").notNullable();
      table.timestamp("expire_date").notNullable();
      table.string("quantity", 20).notNullable();
      table.integer("price").notNullable();
      table.integer("store_item_id").notNullable().references("store_item.id");
      table.timestamps(false, true);
    });
  }

  if (!(await knex.schema.hasTable("ingredient"))) {
    await knex.schema.createTable("ingredient", (table) => {
      table.increments("id");
      table.integer("dish_id").notNullable().references("dish.id");
      table.integer("store_item_id").notNullable().references("store_item.id");
      table.timestamps(false, true);
    });
  }

  if (!(await knex.schema.hasTable("orders"))) {
    await knex.schema.createTable("orders", (table) => {
      table.increments("id");
      table.integer("table_no").notNullable();
      table.boolean("status").notNullable().defaultTo(false);
      table.integer("quantity").notNullable();
      table.string("discount").nullable();
      table.integer("amount").notNullable();
      table.integer("dish_id").notNullable().references("dish.id");
      table.timestamps(false, true);
    });
  }

  if (!(await knex.schema.hasTable("special_req"))) {
    await knex.schema.createTable("special_req", (table) => {
      table.increments("id");
      table.integer("orders_id").notNullable();
      table.integer("special_list_id").notNullable();
      table.timestamps(false, true);
    });
  }
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("special_req");
  await knex.schema.dropTableIfExists("orders");
  await knex.schema.dropTableIfExists("ingredient");
  await knex.schema.dropTableIfExists("orders_detail");
  await knex.schema.dropTableIfExists("feedback");
  await knex.schema.dropTableIfExists("special_list");
  await knex.schema.dropTableIfExists("store_item");
  await knex.schema.dropTableIfExists("dish");
  await knex.schema.dropTableIfExists("users");
}
