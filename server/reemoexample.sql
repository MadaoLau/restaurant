const result = await this.knex < newOrder >("orders").select("*", "dish.name", "table_no.id as table").leftJoin("table_no", "table_no.id", "orders.table_no").leftJoin("dish", "dish.id", "orders.dish_id") / /.leftJoin("client_id", "client_id.id", "orders.clientId").where("status", false)
SELECT
        dish.name,
        SUM(orders.quantity)
FROM
        orders
        LEFT JOIN table_no ON table_no.id = orders.table_no
        LEFT JOIN dish ON dish.id = orders.dish_id
WHERE
        orders.status = false
GROUP BY
        table_no
SELECT
        table_no.table_no,
        dish.name,
        dish.id,
        SUM(orders.quantity) as total_quantity
FROM
        orders
        LEFT JOIN table_no ON table_no.id = orders.table_no
        LEFT JOIN dish ON dish.id = orders.dish_id
WHERE
        orders.status = false
GROUP BY
        table_no.table_no,
        dish.name,
        dish.id
SELECT
        *
FROM
        orders jsonb_agg(dish.name) as name,
        jsonb_agg(orders.quantity) as quant
SELECT
        req
FROM
        orders
        JOIN special_req ON orders.id = special_req.orders_id
        JOIN special_list ON special_req.special_list_id = special_list.id
WHERE
        orders.client_id =
SELECT
        table_no.table_no,
        orders.client_id,
        dish.name,
        dish.id,
        SUM(orders.quantity) as total_quantity
FROM
        orders
        LEFT JOIN table_no ON table_no.id = orders.table_no
        LEFT JOIN dish ON dish.id = orders.dish_id
WHERE
        orders.status = false
GROUP BY
        table_no.table_no,
        orders.client_id,
        dish.name,
        dish.id

SELECT req,client_id,table_no FROM orders
    JOIN special_req ON orders.id = special_req.orders_id
    JOIN special_list ON special_req.special_list_id = special_list.id



    async getTargetSpecialReq() {
    const result = (
      await this.knex.raw(`SELECT req,client_id,table_no FROM orders
    JOIN special_req ON orders.id = special_req.orders_id
    JOIN special_list ON special_req.special_list_id = special_list.id
    `)
    ).rows;
    return result;
  }