import { useState } from "react";
import styles from "../styles/OrderPage.module.css";
import MenuList from "../components/MenuList";
import { useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { useQuery } from "./OrderConfirm";
import { OrderInfoBar } from "../components/OrderInfoBar";
import { Alert, Box, Button } from "@mui/material";
import ReceiptLongIcon from "@mui/icons-material/ReceiptLong";
import { IRootState } from "../redux/store";

// interface DishNumProps {
//   dishNum: number;
// }

const OrderPage = () => {
  const history = useHistory();

  const tableNo = useQuery().get("table");
  const orderNo = useQuery().get("orderNo");
  const clientId = useQuery().get("clientId");

  const count = useSelector((state: IRootState) =>
    state.order.list.reduce((sum, item) => sum + item.order, 0)
  );

  const [errMsg, setErrMage] = useState(false);

  const isOrdered = () => {
    if (count == 0) {
      setErrMage(true);
    } else {
      history.push(
        "/OrderConfirm?table=" +
          tableNo +
          "&clientId=" +
          clientId +
          "&orderNo=" +
          orderNo
      );
    }
  };
  return (
    <div>
      <div className={styles.header1}>
        <h2 style={{ margin: 0, padding: "5px" }}>
          {/* 入座號碼{clientId}  */}
          {tableNo}號枱
          {/* 單號{orderNo} */}
        </h2>
        <div>
          {clientId && (
            <Button
            // variant="outlined" sx={{ borderColor: "white" }}
            >
              <Link
                to={
                  "/clientOrder?table=" +
                  tableNo +
                  "&clientId=" +
                  clientId +
                  "&orderNo=" +
                  orderNo
                }
                className={styles.receiptButton}
              >
                查看歷史單據
              </Link>
              <ReceiptLongIcon style={{ color: "white" }} />
            </Button>
          )}
        </div>
      </div>
      <OrderInfoBar />

      <MenuList />

      {errMsg ? (
        <Box>
          <Alert severity="warning">還未點，請點選後再確認</Alert>餐
        </Box>
      ) : (
        ""
      )}
      <button
        className={styles.confirmButton}
        onClick={isOrdered}
        // history.push(
        //   "/OrderConfirm?table=" +
        //     tableNo +
        //     "&clientId=" +
        //     clientId +
        //     "&orderNo=" +
        //     orderNo
        // );
        // dispatch(
        //   push(
        //     "/OrderConfirm?table=" +
        //       tableNo +
        //       "&clientId=" +
        //       clientId +
        //       "&orderNo=" +
        //       orderNo
        //   )
        // );
        // }
      >
        確認落單
      </button>
    </div>
  );
};

export default OrderPage;
