import { GenMenuList } from "../components/manaMenuList";
import styles from "../styles/OrderPage.module.css";
import { createTheme, ThemeProvider } from "@mui/material";
import LogoutButton from "../components/LogoutButton";
// import manaStyles from "../styles/ManaMenuList.module.css"

const theme = createTheme({
  palette: {
    secondary: {
      main: "#388e3c",
    },
  },
});

const ManageMenuPage = () => {
  return (
    <ThemeProvider theme={theme}>
      <div>
        <div
          // className={manaStyles.topLine}
          style={{
            display: "flex",
            justifyContent: "space-between",
            backgroundColor: "#66c139",
          }}
        >
          <h1 className={styles.header1}>餐單管理</h1>
          <h1 className={styles.header1}>
            <LogoutButton />
          </h1>
        </div>
        <GenMenuList />
        {/* <GenDishDetail /> */}
      </div>
    </ThemeProvider>
  );
};

export default ManageMenuPage;
