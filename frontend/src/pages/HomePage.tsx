import React from "react";
import { Link } from "react-router-dom";
import TableForQRcode from "../components/TableForQRcode";
// import styles from "../styles/HomePage.module.css";
import "../styles/HomePage.css";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import bg1 from "../img/cha.jpeg";

const HomePage = () => {
  return (
    <div>
      <div
        style={{
          backgroundImage: `url(${bg1})`,
          backgroundSize: "cover",
          minWidth: "100%",
          minHeight: "100%",
          zIndex: -5,
          position: "fixed",
          top: 0,
          opacity: 0.6,
        }}
      ></div>
      <h1 className="homepageTitle">「想點」訂餐系統</h1>
      <div className="homePageContainer">
        <div className="homePageSmallContainer left">
          <TableForQRcode />
        </div>

        <div className="homePageSmallContainer right">
          <Stack spacing={2} direction="row">
            <Link to="/login" className="link">
              <Button type="submit" variant="contained" sx={{ margin: "0.1rem" }}>員工登入</Button>
            </Link>
          </Stack>
        </div>
      </div>
    </div>
  );
};

export default HomePage;
