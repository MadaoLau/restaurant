import React, { useEffect, useState } from "react";
import OrderByTable from "../components/OrderByTable";
import { fetchFinishOrderToKitchen, fetchGetAllDishesToKitchen } from "../api/order";
import { Tabs, Tab, Box, Typography, Grid } from "@mui/material";
import { TabPanelProps } from "../components/MenuList";
import OrderFinish from "../components/OrderFinish";
import styles from "../styles/KitchenPage.module.css";
import LogoutButton from '../components/LogoutButton'

export function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div role="tabpanel" hidden={value !== index} id={`vertical-tabpanel-${index}`} aria-labelledby={`vertical-tab-${index}`} {...other}>
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

export function a11yProps(index: number) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

const KitchenPage = ({ socket }: any) => {
  const [value, setValue] = useState<number>(0);
  const [result, setResult] = useState<any[]>([]);
  const [finish, setFinish] = useState<any[]>([]);
  const [tableArr, setTableArr] = useState<any>();
  const [req, setReq] = useState<any>();

  //testing for date logic
  // let d = new Date(),
  // dformat = [d.getMonth() + 1, d.getDate(), d.getFullYear()].join("/");
  // let todayYear = d.getFullYear()
  // let todayMonth = d.getMonth()
  // let todayDate = d.getDate()
  // console.log(todayDate)
  
  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  useEffect(() => {
    (async () => {
      await fetchGetAllDishesToKitchen();
    })();
  }, []);

  useEffect(() => {
    console.log(value);
    if (value === 0) {
      return;
    } else if (value === 1) {
      // ( async () =>{
      (async () => {
        let kitchenOrder = await fetchFinishOrderToKitchen();
        console.log(kitchenOrder);
        let tableNo = Array.from(new Set(kitchenOrder.map((n: any) => n.table))) as string[];

        setTableArr(tableNo);
        console.log(tableArr)
        let dataArr: any = [];
        for (let i of tableNo) {
          dataArr.push(kitchenOrder.filter((j: any) => j.table === i));
        }
        console.log("what dataArr", dataArr);

        setFinish(dataArr);
      })();
    }
  }, [value]);

  useEffect(() => {
    socket.on("getOrderKitchen", (data: any) => {
      let kitchenOrder = data.kitchenOrder;

      console.log(kitchenOrder);

      //testing
      let initialClientReq = data.clientReq;

      console.log("reqqqqq", initialClientReq);
      // console.log("front clientReq:", initialClientReq);
      // let kitchenOrderWithReq = kitchenOrder.push(clientReq);
      // // console.log(kitchenOrderWithReq);
      // console.log("what received:", kitchenOrder);
      // let clientReq = initialClientReq.reduce((word: null, word2: any) => word + word2.req, "");
      // console.log("conbined?", clientReq);

      let tableNo = Array.from(new Set(kitchenOrder.map((n: any) => n.table))) as string[];
      console.log("tableNo:", tableNo);

      setTableArr(tableNo);

      let dataArr: any = [];
      for (let i of tableNo) {
        dataArr.push(kitchenOrder.filter((j: any) => j.table === i));
      }
      console.log("dataArr:", dataArr);

      setReq(initialClientReq);
      setResult(dataArr);
    });
  }, [socket]);

  // useEffect( ()=>{
  //   if(value == 1){
  //     // dispatch(fetchFinishOrderToKitchen())
  //   } else if(value == 0){
  //     return
  //   }
  // },[value])

  return (
    <div>
      <h1 className={styles.header1}>後廚接單</h1>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }} className={styles.tabs}>
        <Tabs orientation="horizontal" variant="scrollable" value={value} scrollButtons="auto" onChange={handleChange} aria-label="scrollable auto tabs example" sx={{ borderColor: "divider" }}>
          <Tab label="現有單張" {...a11yProps(0)} className={styles.tabsPanel} />
          <Tab label="已完成" className={styles.tabsPanel} />
          <Tab label="返回後台" href="/manage" className={styles.tabsPanel} />
        </Tabs>
      </Box>

      <TabPanel value={value} index={0}>
        <Grid container direction="row" justifyContent="flex-start" spacing={1}>
          {/* <Grid sx={{ bgcolor: "primary", border: 1 }}>{req}</Grid> */}
          {result && result.map((v: any, i: any) => <OrderByTable remark={req} data={v} key={i + `${v}`} />)}
        </Grid>
      </TabPanel>

      <TabPanel value={value} index={1}>
        <Grid container direction="row" justifyContent="flex-start" spacing={1}>
          {finish && finish.map((v: any, i: any) => <OrderFinish data={v} key={i} />)}
        </Grid>
      </TabPanel>

      <TabPanel value={value} index={2}>
        返回後台中...
      </TabPanel>
    </div>
  );
};

export default KitchenPage;
