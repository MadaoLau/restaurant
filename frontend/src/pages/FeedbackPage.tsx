import {
  Box,
  Button,
  Grid,
  Paper,
  styled,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import DataTable, {
  TableColumn,
  createTheme,
  ExpanderComponentProps,
} from "react-data-table-component";
import Checkbox from "@material-ui/core/Checkbox";
import ArrowDownward from "@material-ui/icons/ArrowDownward";
import { fetchGetScore, fetchSortingFeedback } from "../api/feedback";
import { fetchGetOrderToClient } from "../api/order";
import DateOnly from "../components/DateOnly";
import LogoutButton from "../components/LogoutButton";
import {
  Chart as ChartJS,
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
} from "chart.js";
import { Radar } from "react-chartjs-2";

const sortIcon = <ArrowDownward />;

ChartJS.register(RadialLinearScale, PointElement, LineElement, Filler);

createTheme(
  "solarized",
  {
    text: {
      primary: "#FFFFFF",
      secondary: "#2aa198",
    },
    background: {
      default: "#002b36",
    },
    context: {
      background: "#cb4b16",
      text: "#FFFFFF",
    },
    divider: {
      default: "#073642",
    },
    action: {
      button: "rgba(0,0,0,.54)",
      hover: "rgba(0,0,0,.08)",
      disabled: "rgba(0,0,0,.12)",
    },
  },
  "dark"
);

interface DataRow {
  client_id: string;
  food_quality: number;
  service: number;
  cleanness: number;
  suggestion: string;
  updated_at: string;
}

export const options: any = {
  responsive: true,
  scales: {
    r: {
      pointLabels: {
        font: { size: 20, weight: "bold" },
        color: "blue",
      },
    },
  },
};

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

export default function FeedbackPage() {
  const [sorting, setSorting] = useState<string>("0");
  const [result, setResult] = useState<any[]>([]);
  const [showResult, setShowResult] = useState<boolean>(false);
  const [showMenu, setShowMenu] = useState<boolean>(false);
  const [showAnalysis, setShowAnalysis] = useState<boolean>(false);
  // const [showMenu, setShowMenu] = useState<boolean>(false);
  const [avgScore, setAvgScore] = useState<any>([]);
  const [dataTable, setDataTable] = useState<any>([]);
  const [chartScoreData, setChartScoreData] = useState<any>({
    labels: ["食物質素", "服務質素", "潔淨程度"],
    datasets: [
      {
        label: "評價平均埴",
        data: ["5", "5", "5"],
        backgroundColor: "rgba(255, 29, 43, 0.5)",
        borderColor: "rgba(255, 29, 43, 1)",
        borderWidth: 3,
      },
      {
        data: ["5", "5", "5"],
        backgroundColor: "rgba(255, 29, 43, 0)",
        borderColor: "rgba(255, 29, 43, 0)",
      },
      {
        data: ["0", "0", "0"],
        backgroundColor: "rgba(255, 29, 43, 0)",
        borderColor: "rgba(255, 29, 43, 0)",
      },
    ],
  });

  //     const scoreFoodQualitySort = (a: any, b: any) => {
  //     let aVal = a.food_quality.props.value;
  //     let bVal = b.food_quality.props.value;
  //     return aVal - bVal;
  //   };

  const columns: TableColumn<DataRow>[] = [
    {
      name: "單號",
      selector: (row) => row.client_id,
      sortable: true,
      center: true,
    },
    {
      name: "食物質素",
      selector: (row) => row.food_quality,
      sortable: true,
      center: true,
    },
    {
      name: "服務質素",
      selector: (row) => row.service,
      sortable: true,
      center: true,
    },
    {
      name: "潔淨程度",
      selector: (row) => row.cleanness,
      sortable: true,
      center: true,
    },
    {
      name: "建議",
      selector: (row) => row.suggestion,
      sortable: true,
      center: true,
    },
    {
      name: "日期",
      selector: (row) => row.updated_at,
      sortable: true,
      center: true,
    },
  ];

  useEffect(() => {
    (async () => {
      let result = await fetchGetScore();
      let avgArrNum = [
        parseFloat(result[0].avg_food_quality).toFixed(1),
        parseFloat(result[0].avg_service).toFixed(1),
        parseFloat(result[0].avg_cleanness).toFixed(1),
      ];
      let avgArr = [avgArrNum[0], avgArrNum[1], avgArrNum[2]];
      setChartScoreData({
        labels: ["食物質素", "服務質素", "潔淨程度"],
        datasets: [
          {
            label: "評價平均埴",
            data: avgArr,
            backgroundColor: "rgba(255, 29, 43, 0.5)",
            borderColor: "rgba(255, 29, 43, 1)",
            borderWidth: 3,
          },
          {
            data: ["5", "5", "5"],
            backgroundColor: "rgba(255, 29, 43, 0)",
            borderColor: "rgba(255, 29, 43, 0)",
          },
          {
            data: ["0", "0", "0"],
            backgroundColor: "rgba(255, 29, 43, 0)",
            borderColor: "rgba(255, 29, 43, 0)",
          },
        ],
      });
      setAvgScore(avgArrNum);
    })();
  }, []);

  //   useEffect(() => {
  //     (async()=>{
  //         let feedbackData = await fetchGetAllFeedback();
  //         for (let i = 0; i< feedbackData.length; i++) {
  //                     let orderResult = await fetchGetOrderToClient(feedbackData[i].client_id);
  //                     for (let j=0;j<orderResult.length;j++) {
  //                         console.log(orderResult[j])
  //                         feedbackData[i][`name${j}`]=orderResult[j].name
  //                         feedbackData[i][`total_quantity${j}`]=orderResult[j].total_quantity
  //                     }
  //                     console.log(feedbackData)
  //                     // if (i.client_id == arrs[0].client_id) {
  //                     //   i.menu = arrs;
  //                     // }
  //                   }
  //                   console.log(feedbackData)
  //                   setDataTable(feedbackData);
  //     })()

  //   }, []);

  useEffect(() => {
    (async () => {
      let res = await fetchSortingFeedback(sorting);
      let feedbackData = await res.json();
      for (let i of feedbackData) {
          let fetchOrderResult = await fetchGetOrderToClient(i.client_id);
          console.log(fetchOrderResult);
          let orderResult = fetchOrderResult.clientOrder;
          console.log(orderResult);
          let arrs = [];
          for (let j of orderResult) {
            arrs.push({
              client_id: j.client_id,
              name: j.name,
              total_quantity: j.total_quantity,
            });
          }
            i.menu = arrs;  
      }
      setDataTable(feedbackData);
      setResult(feedbackData);
    })();
  }, []);

  return (
    <Box>
                  <div className="AdminPageTitle">
              <DateOnly/>
              <div className="adminPageTitleText">食客評價</div>
              <LogoutButton />
            </div>
      <DataTable
        columns={columns}
        data={dataTable}
        pagination
        sortIcon={sortIcon}
        theme="solarized"
      />

    <Box sx={{display:'flex', justifyContent:'center'}}>
      <Button
      sx={{margin:'0.8em', fontSize:'1.5em', fontWeight:'bold'}}
      variant="contained"
        onClick={() => {
          setShowMenu(true);
          setShowAnalysis(false);
        }}
      >
        查看餐單
      </Button>

      <Button
      sx={{margin:'0.8em', fontSize:'1.5em', fontWeight:'bold'}}
      variant="contained"
        onClick={() => {
          setShowAnalysis(true);
          setShowMenu(false);
        }}
      >
        數據分析
      </Button>

      <Button
      sx={{margin:'0.8em', fontSize:'1.5em', fontWeight:'bold'}}
      variant="contained"
        onClick={() => {
          setShowMenu(true);
          setShowAnalysis(true);
        }}
      >
        顯示全部
      </Button>

      <Button
      sx={{margin:'0.8em', fontSize:'1.5em', fontWeight:'bold'}}
      variant="contained"
        onClick={() => {
          setShowMenu(false);
          setShowAnalysis(false);
        }}
      >
        隱藏全部
      </Button>
      </Box>
      {showAnalysis && 
            <Box>
            <Box>
              <Box sx={{marginTop:'0.8em', fontSize:'1.5em', fontWeight:'bold'}}>現在平均值:</Box>
              <Box sx={{display:'flex', justifyContent:'center'}}>
              <Box sx={{margin:'0.8em', fontSize:'1.5em', fontWeight:'bold'}}>食物質素:{avgScore[0]}</Box>
              <Box sx={{margin:'0.8em', fontSize:'1.5em', fontWeight:'bold'}}>服務質素:{avgScore[1]}</Box>
              <Box sx={{margin:'0.8em', fontSize:'1.5em', fontWeight:'bold'}}>潔淨程度:{avgScore[2]}</Box>
              </Box>
            </Box>
            <div style={{ height: "30%" }}>
              <div
                style={{
                  width: "80%",
                  maxHeight: "50%",
                  display: "inline-block",
                  marginBottom: "0",
                }}
              >
                <Radar data={chartScoreData} options={options} />
              </div>
            </div>
            </Box>
      }


      <Grid container direction="row" justifyContent="flex-start" spacing={1}>
        {showMenu &&
          result.map((v: any, i: number) => (
            <Grid item xs={12} md={3} key={"a" + i}>
              <Item>
                  <Box>
                    {/* <Typography
                      sx={{
                        fontSize: "1.5em",
                        color: "primary.main",
                        fontWeight: "bold",
                      }}
                      component="legend"
                    >
                      餐單:
                    </Typography> */}
                                    <Typography
                  sx={{
                    fontSize: "1.5em",
                    color: "primary.main",
                    fontWeight: "bold",
                  }}
                >
                  單號:{v.client_id}
                </Typography>
                    {v.menu.map((x: any, i: any) => (
                      <Box key={i}>
                        <Box
                          sx={{
                            fontSize: "1.2em",
                            fontWeight: "bold",
                          }}
                        >
                          {x.name}:{x.total_quantity}
                        </Box>
                      </Box>
                    ))}
                  </Box>
              </Item>
            </Grid>
          ))}
      </Grid>
    </Box>
  );
}

//

// }
// }
{/* <Box
sx={{
  "& > legend": { mt: 2 },
}}
>
<Typography
  sx={{
    fontSize: "1.5em",
    color: "primary.main",
    fontWeight: "bold",
  }}
  component="legend"
>
  食物質素
</Typography>
<Rating
  name="food_quality"
  value={parseInt(v.food_quality)}
  readOnly
/>
</Box>
<Box
sx={{
  "& > legend": { mt: 2 },
}}
>
<Typography
  sx={{
    fontSize: "1.5em",
    color: "primary.main",
    fontWeight: "bold",
  }}
  component="legend"
>
  服務質素
</Typography>
<Rating name="service" value={parseInt(v.service)} readOnly />
</Box>
<Box
sx={{
  "& > legend": { mt: 2 },
}}
>
<Typography
  sx={{
    fontSize: "1.5em",
    color: "primary.main",
    fontWeight: "bold",
  }}
  component="legend"
>
  潔淨程度
</Typography>
<Rating value={parseInt(v.cleanness)} readOnly />
</Box>
<Box marginTop="0.5em">
<Typography
  sx={{
    fontSize: "1.5em",
    color: "primary.main",
    fontWeight: "bold",
  }}
  component="legend"
>
  其他建議
</Typography>
<Typography
  sx={{
    fontSize: "1.75em",
    color: "primary.main",
    fontWeight: "bold",
  }}
  component="legend"
>
  {v.suggestion}
</Typography>
</Box> */}