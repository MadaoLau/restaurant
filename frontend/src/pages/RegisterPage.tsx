import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { registerThunk } from "../redux/auth/thunk";
import { IRootState } from "../redux/store";
import { useHistory } from "react-router-dom";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { Alert, CircularProgress } from "@mui/material";
import { phone } from "phone";
import { msgClear } from "../redux/auth/actions";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";

interface IRegisterForm {
  username: string;
  password: string;
  confirm_password: string;
  email: string;
  gender: string;
  contact: string;
}

function ValidateEmail(input: string) {
  var validRegex =
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

  if (input.match(validRegex)) {
    return true;
  } else {
    return false;
  }
}

const Field = (props: {
  iRegisterState: IRegisterForm;
  field: keyof IRegisterForm;
  placeholder: string;
  label: string;
  inputType: "text" | "password";
  warningMsg: Record<keyof IRegisterForm, string>;
  setIRegisterFormState: (state: IRegisterForm) => void;
  autoComplete: string;
}) => {
  const {
    iRegisterState,
    field,
    placeholder,
    label,
    inputType,
    setIRegisterFormState,
    autoComplete,
  } = props;
  const value = iRegisterState[field];
  const validateMsg = props.warningMsg[field];

  return (
    <>
      <Grid item xs={12}>
        <TextField
          type={inputType}
          placeholder={placeholder}
          // {...register(field, {})}
          // required
          fullWidth
          // id={value}
          label={label}
          // name={name}
          value={value}
          autoComplete={autoComplete}
          color={!value ? "success" : validateMsg ? "warning" : "primary"}
          onChange={(e) =>
            setIRegisterFormState({
              ...iRegisterState,
              [field]: e.currentTarget.value || "",
            })
          }
        />
        <Grid color={!value ? "dark" : "red"}>{validateMsg}</Grid>
      </Grid>
    </>
  );
};

export default function RegisterPage() {
  const [iRegisterState, setIRegisterFormState] = useState<IRegisterForm>({
    username: "",
    password: "",
    confirm_password: "",
    email: "",
    gender: "",
    contact: "",
  });
  const [msgTricker, setMsgTricker] = useState(false);
  const [alertMsg, setAlertMsg] = useState("");
  const [open, setOpen] = useState(false);
  const handleClose = () => {
    setOpen(false);
  };

  const { register, handleSubmit } = useForm<IRegisterForm>();
  let msg = useSelector((state: IRootState) => state.auth.msg);
  const dispatch = useDispatch();
  const history = useHistory();
  // const [username, setUsername] = useState("");

  const warningMsg = {
    username: !iRegisterState.username
      ? ""
      : iRegisterState.username.length < 2 ||
        iRegisterState.username.length > 12
      ? "用戶名稱長度必須為2或12個字元以內"
      : "",
    password: !iRegisterState.password
      ? ""
      : iRegisterState.password.length < 6
      ? "密碼長度不少於6個字位"
      : "",
    confirm_password:
      iRegisterState.confirm_password !== iRegisterState.password
        ? "密碼不相同"
        : "",
    email: "",
    gender: "",
    contact: "",
  };

  function msgBox(msgWord: string) {
    return <Alert severity="warning">{msgWord}</Alert>;
  }

  const onSubmit = () => {
    dispatch(msgClear());
    setMsgTricker(false);
    setAlertMsg("");

    let username = iRegisterState.username;
    let password = iRegisterState.password;
    let email = iRegisterState.email;
    let gender = iRegisterState.gender;
    let contact = iRegisterState.contact;
    const invalidMsg = Object.values(warningMsg).find((msg) => msg.length > 0);
    if (invalidMsg) {
      setMsgTricker(true);
      setAlertMsg("用戶名稱或密碼有誤，請重新填寫");
      return;
    } else if (username === "") {
      setMsgTricker(true);
      setAlertMsg("請輸入用戶名稱");
      return;
    } else if (password === "") {
      setMsgTricker(true);
      setAlertMsg("請輸入密碼");
      return;
    } else if (email === "") {
      setMsgTricker(true);
      setAlertMsg("請輸入電郵");
      return;
    } else if (!ValidateEmail(email)) {
      setMsgTricker(true);
      setAlertMsg("請輸入有效電郵");
      return;
    } else if (gender === "") {
      setMsgTricker(true);
      setAlertMsg("請輸入性別");
      return;
    } else if (contact === "") {
      setMsgTricker(true);
      setAlertMsg("請輸入聯絡電話");
      return;
    } else if (!phone(contact, { country: "HKG" }).isValid) {
      setMsgTricker(true);
      setAlertMsg("電話號碼無效，請重新輸入");
      return;
    } else {
      dispatch(registerThunk(username, password, email, gender, contact));
    }
  };

  useEffect(() => {
    // console.log(msg);
    // if(msg===''){
    //   msg=''
    // }else
    if (msg === "username have been used") {
      setMsgTricker(true);
      setAlertMsg("用戶名稱已被使用");
      // return
    } else if (msg === "email have been used") {
      setMsgTricker(true);
      setAlertMsg("電郵已被使用");
      // return
    } else if (msg === "註冊成功") {
      msg = "";
      setMsgTricker(false);
      setOpen(true);
      setTimeout(() => {
        history.push("/");
      }, 2000);
    }
  }, [msg]);

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Box
        sx={{
          // marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
          {/* <LockOutlinedIcon /> */}
        </Avatar>
        <Typography component="h2" variant="h5">
          員工註冊
        </Typography>
        <Box
          onSubmit={handleSubmit(onSubmit)}
          component="form"
          noValidate
          sx={{ mt: 3 }}
        >
          <Grid container spacing={2}>
            <Field
              {...register("username")}
              {...{
                iRegisterState,
                setIRegisterFormState,
                field: "username",
                placeholder: "輸入用戶名稱",
                label: "用戶名稱",
                inputType: "text",
                warningMsg,
                autoComplete: "姓名",
              }}
            ></Field>
            <Field
              {...register("password")}
              {...{
                iRegisterState,
                setIRegisterFormState,
                field: "password",
                placeholder: "輸入密碼",
                label: "密碼",
                inputType: "password",
                warningMsg,
                autoComplete: "密碼",
              }}
            ></Field>
            <Field
              {...register("confirm_password")}
              {...{
                iRegisterState,
                setIRegisterFormState,
                field: "confirm_password",
                placeholder: "確認密碼",
                label: "確認密碼",
                inputType: "password",
                warningMsg,
                autoComplete: "確認密碼",
              }}
            ></Field>
            <Field
              {...register("email")}
              {...{
                iRegisterState,
                setIRegisterFormState,
                field: "email",
                placeholder: "輸入電郵",
                label: "電郵",
                inputType: "text",
                warningMsg,
                autoComplete: "電郵",
              }}
            ></Field>
            <Field
              {...register("gender")}
              {...{
                iRegisterState,
                setIRegisterFormState,
                field: "gender",
                placeholder: "輸入性別",
                label: "性別",
                inputType: "text",
                warningMsg,
                autoComplete: "性別",
              }}
            ></Field>
            <Field
              {...register("contact")}
              {...{
                iRegisterState,
                setIRegisterFormState,
                field: "contact",
                placeholder: "輸入聯絡電話",
                label: "電話",
                inputType: "text",
                warningMsg,
                autoComplete: "電話",
              }}
            ></Field>
          </Grid>

          <br />
          <Button type="submit" variant="outlined">
            確認註冊
          </Button>

          <Box sx={{ display: "flex", justifyContent: "center" }}>
            {msg && msgTricker ? msgBox(msg) : msgTricker && msgBox(alertMsg)}
          </Box>
          <div>
            <Dialog
              open={open}
              onClose={handleClose}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title"></DialogTitle>
              <DialogContent sx={{ textAlign: "center" }}>
                <DialogContentText
                  id="alert-dialog-description"
                  sx={{ fontWeight: "bold", fontSize: "2rem" }}
                >
                  <div>歡迎加入『想點』</div>
                </DialogContentText>
                <DialogContentText>
                  <div>已完成員工註冊</div>
                  <div>...2秒後將進入登入介面</div>
                  <CircularProgress />
                </DialogContentText>
              </DialogContent>
            </Dialog>
          </div>
        </Box>
      </Box>
    </Container>
  );
}
