import React from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { IRootState } from "../redux/store";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import "../styles/AdminManagePage.css";
import PeopleAltIcon from "@mui/icons-material/PeopleAlt";
import MenuBookIcon from "@mui/icons-material/MenuBook";
import StarIcon from "@mui/icons-material/Star";
import WarehouseIcon from "@mui/icons-material/Warehouse";
import KitchenIcon from "@mui/icons-material/Kitchen";
import ReceiptLongIcon from "@mui/icons-material/ReceiptLong";
import LogoutButton from "../components/LogoutButton";
import QrCodeScannerIcon from "@mui/icons-material/QrCodeScanner";

export default function AdminManagePage() {
  const username = useSelector(
    (state: IRootState) => state.auth.user?.username
  );
  const admin = useSelector((state: IRootState) => state.auth.user?.is_admin);
  console.log(admin, username);
  return (
    <div>
      <div className="AdminPageTitle">
        <div className="adminPageTitleText">後台管理</div>
        <LogoutButton />
      </div>

      <br />
      <div className="adminPageContainer">
        <div>使用者：{username}</div>
        <div>身份：{!admin ? "Staff" : "Admin"}</div>
      </div>
      <Stack>
        <div>
          <Link to="/qrCode" className="adminPageLink">
            <Button
              sx={{ width: 150, height: 150, fontSize: 14, m: 1, boxShadow: 2 }}
              variant="outlined"
            >
              <div>
                <QrCodeScannerIcon sx={{ fontSize: "60px" }} />
                <div
                  className="adminManagePageTags"
                  style={{ fontWeight: "bold" }}
                >
                  QRCode
                </div>
              </div>
            </Button>
          </Link>

          <Link to="/orderManage" className="adminPageLink">
            <Button
              sx={{ width: 150, height: 150, fontSize: 14, m: 1, boxShadow: 2 }}
              variant="outlined"
            >
              <div>
                <ReceiptLongIcon sx={{ fontSize: "60px" }} />
                <div className="adminManagePageTags">訂單管理</div>
              </div>
            </Button>
          </Link>

          <Link to="/kitchenPage" className="adminPageLink">
            <Button
              sx={{ width: 150, height: 150, fontSize: 14, m: 1, boxShadow: 2 }}
              variant="outlined"
            >
              <div>
                <KitchenIcon sx={{ fontSize: "60px" }} />
                <div className="adminManagePageTags">後廚接單</div>
              </div>
            </Button>
          </Link>

          <Link to="/feedback" className="adminPageLink">
            <Button
              sx={{ width: 150, height: 150, fontSize: 14, m: 1, boxShadow: 2 }}
              variant="outlined"
            >
              <div>
                <StarIcon sx={{ fontSize: "60px" }} />
                <div className="adminManagePageTags">食客評價</div>
              </div>
            </Button>
          </Link>

          {admin && (
            <Link to="/staff" className="adminPageLink">
              <Button
                sx={{
                  width: 150,
                  height: 150,
                  fontSize: 14,
                  m: 1,
                  boxShadow: 2,
                }}
                variant="outlined"
              >
                <div>
                  <PeopleAltIcon sx={{ fontSize: "60px" }} />
                  <div className="adminManagePageTags">員工管理</div>
                </div>
              </Button>
            </Link>
          )}

          {admin && (
            <Link to="/manageMenu" className="adminPageLink">
              <Button
                sx={{
                  width: 150,
                  height: 150,
                  fontSize: 14,
                  m: 1,
                  boxShadow: 2,
                }}
                variant="outlined"
              >
                <div>
                  <MenuBookIcon sx={{ fontSize: "60px" }} />
                  <div className="adminManagePageTags">餐牌管理</div>
                </div>
              </Button>
            </Link>
          )}
        </div>
      </Stack>
      {/* admin only */}
    </div>
  );
}

/* <Stack spacing={2} direction="row">
  <Button variant="text">Text</Button>
  <Button variant="contained">Contained</Button>
  <Button variant="outlined">Outlined</Button>
</Stack>; */
