import { useEffect, useState } from "react";
import { Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { loginThunk } from "../redux/auth/thunk";
import { IRootState } from "../redux/store";
import "../styles/LoginPage.css";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import { msgClear } from "../redux/auth/actions";
import Dialog from "@mui/material/Dialog";
// import DialogActions from '@mui/material/DialogActions';
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import { Alert, Box, CircularProgress } from "@mui/material";
import bg1 from "../img/cha.jpeg";
import homePageTitle from "../img/reemoLogo.png";
import Image from "react-bootstrap/Image";
import Container from "react-bootstrap/Container";

/* imports omitted for simplicity purpose*/
interface ILoginForm {
  username: string;
  password: string;
}

function msgBox(msgWord: string) {
  return (
    <Alert sx={{ m: 2 }} severity="warning">
      {msgWord}
    </Alert>
  );
}

export default function LoginPage() {
  const dispatch = useDispatch();
  const { register, handleSubmit } = useForm<ILoginForm>();
  const msg = useSelector((state: IRootState) => state.auth.msg);
  const [alert, setAlert] = useState(false);
  const history = useHistory();
  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const onSubmit = (data: ILoginForm) => {
    if (data.username && data.password) {
      dispatch(loginThunk(data.username, data.password));
    }
  };

  useEffect(() => {
    // console.log(msg);
    //can be del?
    if (msg === "註冊成功") {
      dispatch(msgClear());
    } else if (msg === "用戶名稱不正確") {
      setAlert(true);
    } else if (msg === "密碼錯誤") {
      setAlert(true);
    } else if (msg === "Login Success") {
      setAlert(false);
      setOpen(true);
      setTimeout(() => {
        history.push("/manage");
      }, 1000);
    }
    //eslint-disable-next-line react-hooks/exhaustive-deps
  }, [msg]);

  // const result = useSelector((state: IRootState) => state.auth.loginResult)
  // const dispatch = useDispatch()

  return (
    <div>
      <div className="bg" style={{ backgroundImage: "url(" + bg1 + ")" }}></div>

      <div>
        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title"></DialogTitle>
          <DialogContent sx={{ textAlign: "center", p: "40px" }}>
            <DialogContentText
              id="alert-dialog-description"
              sx={{ fontWeight: "bold", fontSize: "2rem" }}
            >
              <Box>登入成功</Box>
            </DialogContentText>
            <DialogContentText>
              <br />
              <CircularProgress />
            </DialogContentText>
          </DialogContent>
          {/* <DialogActions> */}
          {/* <Button onClick={handleClose}>Disagree</Button> */}
          {/* <Button onClick={handleClose} autoFocus> */}
          {/* Agree */}
          {/* </Button> */}
          {/* </DialogActions> */}
        </Dialog>
      </div>
      {/* <img
        style={{
          backgroundImage: "url(" + homePageTitle + ")",
          maxWidth: "100%",
          height: "auto",
          zIndex: 2,
          position: "fixed",
          top: 0,
        }}
      ></img> */}
      {/* <Container> */}
      <div
        style={{
          // width: "50%",
          height: "auto",
          display: "flex",
          justifyContent: "center",
          textAlign: "center",
          // alignItems: "center",
          marginBottom: "-90px",
        }}
      >
        <div>
          <img
            src={homePageTitle}
            width={250}
            height={160}
            // fluid={true}
          />
        </div>
      </div>
      {/* </Container> */}

      {/* <Image src={homePageTitle} width="500px" height="150px" /> */}
      {/* <br />
      <br /> */}

      {/* <h1 className="loginPageTitle">「想點」訂餐系統</h1> */}
      <div className="loginContainer">
        <br />

        <Form onSubmit={handleSubmit(onSubmit)}>
          <Form.Group className="loginItems">
            <Form.Label>帳戶：</Form.Label>
            <Form.Control
              type="text"
              placeholder="請輸入帳號"
              {...register("username", {})}
            />
          </Form.Group>
          <Form.Group className="loginItems">
            <Form.Label>密碼：</Form.Label>
            <Form.Control
              type="password"
              placeholder="請輸入密碼"
              {...register("password", {})}
            />
          </Form.Group>
          <Stack
            spacing={2}
            direction="row"
            sx={{ display: "flex", justifyContent: "center", m: 1 }}
          >
            <Button
              sx={{ width: 290, fontSize: 18 }}
              variant="contained"
              className="loginItems"
              type="submit"
            >
              登入
            </Button>
          </Stack>

          {msg && alert ? msgBox(msg) : ""}
        </Form>
        <Stack
          spacing={2}
          direction="row"
          sx={{ display: "flex", justifyContent: "center" }}
        >
          <Link to="/register" className="loginPageLink">
            <Button
              sx={{ width: 290, fontSize: 18, border: "1.5px solid" }}
              variant="outlined"
              className="loginItems"
            >
              員工注冊
            </Button>
          </Link>

          {/* <Link to="/" className="loginPageLink">
            <Button
              sx={{ width: 120, fontSize: 14 }}
              variant="outlined"
              className="loginItems"
            >
              返回首頁
            </Button>
          </Link> */}
        </Stack>
      </div>
    </div>
  );
}
