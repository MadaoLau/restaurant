import { useEffect, useState } from "react";
import { Box, Button, TextField } from "@mui/material";
import { Link } from "react-router-dom";
import {
  fetchDeleteOrder,
  fetchGetAllDishesToKitchen,
  fetchGetOrderToClient,
} from "../api/order";
import OrderManageHelper from "../components/OrderManageHelper";
import styles from "../styles/OrderManage.module.css";
import LogoutButton from "../components/LogoutButton";

export const OrderManage = ({ socket }: any) => {
  // const dispatch = useDispatch();
  // const orderList = useSelector((state: IRootState) => state.order.orderList);
  const [tableNo, setTableNo] = useState<any>();
  const [clientId, setClientId] = useState<any>();
  const [result, setResult] = useState<any[]>([]);
  const [totalAmount, setTotalAmount] = useState<number>(0);

  const removeOrder = async (clientId: string) => {
    const res = await fetchDeleteOrder(clientId);
    if (res.ok) {
      await fetchGetOrderToClient(clientId!);
      console.log("deleted");
      await fetchGetAllDishesToKitchen();
    } else {
      console.log("failed");
    }
  };

  const handleChangeTable = (event: any) => {
    setTableNo(event.target.value);
  };

  const handleChangeClient = (event: any) => {
    setClientId(event.target.value as string);
  };

  useEffect(() => {
    socket.on("getClientOrder", (data: any) => {
      let clientOrder = data.clientOrder;
      console.log("adminMange received:", data);

      let price = clientOrder.reduce((sum: number, item: any) => sum + +item.total_amount, 0);
      setTotalAmount(price);
      setResult(clientOrder);
    });
  }, [socket]);

  useEffect(() => {
    (async () => {
      // dispatch(getOrderThunk(orderList,clientId))    //Terence trying to fix +/- in orderManage
      let clientOrder = await fetchGetOrderToClient(clientId!);
      console.log(clientOrder.map((v: any) => v.name));
      let price = clientOrder.reduce((sum: number, item: any) => sum + +item.total_amount, 0);
      setTotalAmount(price);
      // orderList.push(...clientOrder)
      setResult(clientOrder);
    })();
  }, [clientId]);

  let d = new Date(),
    dformat = [d.getMonth() + 1, d.getDate(), d.getFullYear()].join("/") + " " + [d.getHours(), d.getMinutes(), d.getSeconds()].join(":");

  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          fontWeight: "bold",
          backgroundColor: "#1b76d2",
          color: "white",
          pl: "15px",
          pr: "15px",
          pt: "5px",
          pb: "5px",
          mb: "10px",
        }}
      >
        <Box sx={{ fontSize: "1.5em" }}>{tableNo}號枱</Box>
        <Box sx={{ fontSize: "1.3em", alignSelf: "center" }}>
          {/* 入座號碼:{clientId} */}
          訂單管理
        </Box>
        <Box>
          <LogoutButton />
        </Box>
      </Box>

      <TextField id="outlined-name" label="tableNo" value={tableNo} onChange={handleChangeTable} sx={{ width: "45%", mr: "10px" }} />
      <TextField id="outlined-name" label="clientId" value={clientId} onChange={handleChangeClient} sx={{ width: "45%", mb: "10px" }} />
      <Box sx={{ backgroundColor: "#1b76d2", color: "white" }}>訂餐時間： {dformat}</Box>
      <Box className={styles.header3}>
        <Box>訂單狀態：未付款 </Box>

        <Link to="/manage" style={{ color: "#1b76d2", fontWeight: "bold" }}>
          返回後台
        </Link>
      </Box>

      {/* <Grid
        sx={{
          //  borderBottom: 1, borderTop: 1,
          display: "flex",
          py: 1,
        }}
      >
        <Grid xs={4} style={{ fontWeight: "bold" }}>
          名稱
        </Grid>
        <Grid xs={2} sx={{}}>
          數量
        </Grid>
        <Grid xs={3} sx={{}}>
          價錢
        </Grid>
        <Grid xs={3} sx={{}}>
          刪除
        </Grid>
      </Grid> */}
      {result && result.map((v: any, i: any) => <OrderManageHelper data={v} key={i} />)}
      <Box
        sx={{
          display: "flex",
          justifyContent: "flex-end",
          fontSize: "1em",
          fontWeight: "bold",
        }}
      >
        合共: {"$" + totalAmount}
      </Box>

      <Button
        onClick={() => removeOrder(clientId)}
        sx={{
          backgroundColor: "#E9E9E9",
          color: "black",
          borderRadius: 0,
          width: "100%",
          height: "40px",
          fontSize: "20px",
          padding: "5px",
          fontWeight: "bold",
          cursor: "pointer",
          position: "fixed",
          left: 0,
          bottom: 0,
        }}
      >
        全部取消
      </Button>
    </Box>
  );
};

export default OrderManage;

// backgroundColor: "#66c139",
// color: white,
// border: none,
// width: 50%,
// height: 40px;
// font-size: 20px;
// padding: 5px;
// font-weight: bold;
// cursor: pointer;
// position: absolute;
// right: 0;
// bottom: 0;
