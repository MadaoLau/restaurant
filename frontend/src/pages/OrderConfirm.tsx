import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import OrderList from "../components/OrderList";
import { DishList } from "../redux/order/state";
import { IRootState } from "../redux/store";
import styles from "../styles/OrderConfirm.module.css";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert, { AlertProps } from "@mui/material/Alert";
import { fetchSendOrder } from "../api/order";
import { useHistory, useLocation } from "react-router-dom";
import { getAllDishesThunk } from "../redux/order/thunk";
import { Box, Grid, TextField, Typography } from "@mui/material";

const header1 = {
  fontSize: "20px",
  fontWeight: "bold",
  m: 0,
  p: "5px",
};

export function useQuery() {
  const { search } = useLocation();
  // console.log(`OrderConfirm search: `, search);
  return React.useMemo(() => new URLSearchParams(search), [search]);
}

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const OrderConfirm = () => {
  const dispatch = useDispatch();
  const list = useSelector((state: IRootState) => state.order.list);
  // console.log(`Check order list`, list[0].price);
  const history = useHistory();
  const result = list.filter((item) => item.order > 0);
  const [specialReq, setSpecialReq] = useState<string>("");

  const totalPrice = result.map((item) => item.order * item.price).reduce((partialSum, a) => partialSum + a, 0);
  console.log(`Total Price: `, totalPrice);

  const [open, setOpen] = React.useState(false);

  // const handleClick = () => {
  //   setOpen(true);
  // };

  const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const onChangeReq = (req: string) => {
    setSpecialReq(req);
  };

  const handleAddOrder = async (newOrder: DishList, specialReq: string) => {
    console.log("Received from confirm order: ", newOrder, specialReq);

    await fetchSendOrder(newOrder, tableNo!.toString(), clientId!, specialReq);

    setOpen(true);

    if (!orderNo) {
      return;
    } else {
      orderNo = (parseInt(orderNo) + 1).toString();
      console.log(`OrderConfirm orderNo`, orderNo);
    }

    setTimeout(() => {
      history.push(`/order?table=${tableNo}&clientId=${clientId}&orderNo=${orderNo}`);
      dispatch(getAllDishesThunk([]));
    }, 2000);

    // dispatch(addOrderThunk(newOrder));
    // console.log(`submit form data`, data);
  };

  const tableNo = useQuery().get("table");
  let orderNo = useQuery().get("orderNo");
  const clientId = useQuery().get("clientId");

  return (
    <Box>
      <Grid className={styles.header1} sx={{ display: "flex", justifyContent: "space-around" }}>
        <Typography sx={header1}>
          {/* 入座號碼{clientId}  */}
          {tableNo}號枱{/* 單號{orderNo} */}
        </Typography>
        <Typography sx={header1}>確認落單</Typography>
      </Grid>
      <OrderList />

      {/* <Typography style={{ float: "right", marginRight: "25px", fontWeight: "bold" }}>
        合共：${totalPrice}
      </Typography> */}

      <Box
        sx={{
          width: 500,
          maxWidth: "100%",
          display: "inline-flex",
          justifyContent: "center",
        }}
      >
        <TextField
          value={specialReq}
          fullWidth
          label="食物指引"
          id="specialReq"
          placeholder="eg. 少甜"
          onChange={(e) => {
            onChangeReq?.(e.currentTarget.value);
          }}
        ></TextField>
      </Box>

      <button onClick={() => history.push("/order?table=" + tableNo + "&clientId=" + clientId + "&orderNo=" + orderNo)} className={styles.confirmButtonLeft}>
        返回修改
      </button>

      {/* <Stack spacing={2} sx={{ width: "100%" }}> */}
      <button onClick={() => handleAddOrder(result, specialReq)} className={styles.confirmButtonRight}>
        落單
      </button>
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="info" sx={{ width: "100%" }}>
          落單成功！
        </Alert>
      </Snackbar>
      {/* <Alert severity="error">This is an error message!</Alert>
        <Alert severity="warning">This is a warning message!</Alert>
        <Alert severity="info">This is an information message!</Alert>
        <Alert severity="success">This is a success message!</Alert> */}
      {/* </Stack> */}
    </Box>
  );
};

export default OrderConfirm;
