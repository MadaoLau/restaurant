import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useQuery } from "./OrderConfirm";
import { fetchGetOrderToClient } from "../api/order";
import {
  Box,
  Button,
  Grid,
  Card,
  Typography,
  Dialog,
  DialogActions,
  TextField,
} from "@mui/material";
import Feedback from "../components/Feedback";
import ReceiptLongIcon from "@mui/icons-material/ReceiptLong";
import { TransitionProps } from "@mui/material/transitions";
import Slide from "@mui/material/Slide";
import styles from "../styles/ClientOrderPage.module.css";
import { Link } from "react-router-dom";

const wordStyle = {
  textAlign: "center",
  fontWeight: "bold",
};

const total = {
  fontWeight: "bold",
  borderBottomStyle: "double",
};

const cardStyle = {
  bgcolor: "background.paper",
  display: "flex",
  borderRadius: "10px",
  // mx: 2,
  my: 1,
  py: 3,
  mx: 1,
};

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const ClientOrderPage = ({ socket }: any) => {
  // const dispatch = useDispatch();
  const history = useHistory()
  const [result, setResult] = useState<any[]>([]);
  const [totalAmount, setTotalAmount] = useState<number>(0);
  const [showFeedback, setShowFeedback] = useState(false);
  const [specialReq, setSpecialReq] = useState<Object[]>([]);

  //localhost:8000/api/order/getOrder
  const tableNo = useQuery().get("table");
  const orderNo = useQuery().get("orderNo");
  let clientId = useQuery().get("clientId");

  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    socket.on("getClientOrder", (data: any) => {
      console.log("data from socket:", data);

      let clientOrder = data.clientOrder;
      // console.log("allClientReq socket:", data.clientReq);
      // console.log("2allClientReq socket:", data.clientReq[0].req);

      console.log(clientOrder);
      let price = clientOrder.reduce((sum: number, item: any) => sum + +item.total_amount, 0);
      setTotalAmount(price);
      setResult(clientOrder);
    });
  }, [socket]);

  useEffect(() => {
    (async () => {
      console.log(clientId);
      let allClientOrder = await fetchGetOrderToClient(clientId!);
      console.log("allClientReq list res from server:", allClientOrder);
      console.log("2allClientReq list res from server:", allClientOrder.clientOrder[0].client_id);
      let clientReqs = allClientOrder.clientReq;
      let clientOrder = allClientOrder.clientOrder;

      let targetClientId = allClientOrder.clientOrder[0].client_id;
      // console.log(
      //   "allClientReq word res from server:",
      //   allClientOrder.clientReq[0].req
      // );
      let clientReq = clientReqs.filter((r: any) => r.client_id === targetClientId);

      console.log("conbined?", clientReq);

      // let reqResult:any;
      // for (let i = 0; i < clientReq.length; i++) {
      //   reqResult.push(clientReq[i].req);
      // }

      let newReq:any = clientReq.reduce((word: null, word2: any) => word + word2.req, "");
      console.log('newOk:', newReq);
      

      console.log(clientOrder.map((v: any) => v.name));
      let price = clientOrder.reduce((sum: number, item: any) => sum + +item.total_amount, 0);
      setTotalAmount(price);
      setResult(clientOrder);
      setSpecialReq(newReq);
    })();
  }, []);

  const postFeedback = () => {
    setOpen(true);
  };

  let d = new Date(),
    dformat = [d.getMonth() + 1, d.getDate(), d.getFullYear()].join("/") + " " + [d.getHours(), d.getMinutes(), d.getSeconds()].join(":");

  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          fontSize: "1.5rem",
          pt: "5px",
          pb: "5px",
          pl: "10px",
          pr: "10px",
          fontWeight: "bold",
          color: "white",
          backgroundColor: "#66c139",
        }}
      >
        <Box>{tableNo}號枱</Box>
        <Box sx={{ display: "flex" }}>
          <Box sx={{ fontSize: "1.1rem", alignSelf: "center" }}>
            查看歷史單據
          </Box>
          <ReceiptLongIcon style={{ color: "white" }} />
        </Box>
        {/* <Box>
          <Button
            className={styles.receiptButton}
            sx={{ fontSize: "1.1rem", alignSelf: "center" }}
            href={ `/order?table=${tableNo}&clientId=${clientId}&orderNo=${orderNo}` }
          >
            <Link
              to={`/order?table=${tableNo}&clientId=${clientId}&orderNo=${orderNo}`}
              className={styles.receiptButton}
            >
              繼續點餐
            </Link>
            <ReceiptLongIcon style={{ color: "white" }} />
          </Button>
        </Box> */}
        {/* <Box>入座號碼:{clientId}</Box> */}
      </Box>
      <Box sx={{ background: "#E9E9E9", p: "5px" }}>訂餐時間： {dformat}</Box>

      <Grid sx={{ borderBottom: 1, borderTop: 1, display: "flex", py: 1.5 }}>
        <Grid item xs={2} style={{ fontWeight: "bold" }}>
          項例
        </Grid>
        <Grid item xs={4} sx={wordStyle}>
          名稱
        </Grid>
        <Grid item xs={3} sx={wordStyle}>
          數量
        </Grid>
        <Grid item xs={3} sx={wordStyle}>
          價錢
        </Grid>
      </Grid>
      {result &&
        result.map((v: any, i: number) => (
          <Card key={v.name} sx={cardStyle}>
            <Grid item xs={2} key={v.id}>
              {i + 1}.
            </Grid>
            <Grid item xs={4}>
              {" "}
              {v.name}:
            </Grid>
            <Grid item xs={3}>
              {v.total_quantity}
            </Grid>
            <Grid item xs={3}>
              {"$" + v.total_amount}
            </Grid>
          </Card>
        ))}
      <Grid sx={{ display: "flex", py: 1.5 }}>
        <Grid item xs={2} style={{ fontWeight: "bold" }}></Grid>
        <Grid item xs={3} sx={wordStyle}></Grid>
        <Grid item xs={3} sx={wordStyle}></Grid>
        <Grid item xs={4}>
          <Typography sx={total}>總計: {"$" + totalAmount}</Typography>
        </Grid>
      </Grid>

      {specialReq && (
        <Box sx={{ my: 2 }}>
          <Grid
            sx={{
              width: 500,
              maxWidth: "100%",
              display: "inline-flex",
              justifyContent: "center",
            }}
          >
            <TextField key={specialReq + `one`} value={specialReq} disabled fullWidth label="食物指引" id="specialReq"></TextField>
          </Grid>
        </Box>
      )}

      <Typography color="primary">為確保改善服務質素, 歡迎填交你的寶貴意見</Typography>
      <Button variant="outlined" onClick={() => postFeedback()}>
        填寫問卷
      </Button>
      <br />
      <br />
      <br />
      <br />
      <br />
      <div>
        <Dialog open={open} TransitionComponent={Transition} keepMounted onClose={handleClose} aria-describedby="alert-dialog-slide-description">
          <Grid sx={{ display: "flex", justifyContent: "center" }}>
            <Feedback />
          </Grid>

          <DialogActions sx={{ display: "flex", justifyContent: "center" }}>
            <Button onClick={handleClose}>返回</Button>
            {/* <Button onClick={handleClose}>確認</Button> */}
          </DialogActions>
        </Dialog>
      </div>
      <button
        className={styles.confirmButton}
        onClick={() => {
          history.push(
            `/order?table=${tableNo}&clientId=${clientId}&orderNo=${orderNo}`
          );
        }}
      >
        返回點餐
      </button>
    </Box>
  );
};

export default ClientOrderPage;
