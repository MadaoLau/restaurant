import LogoutButton from "../components/LogoutButton";
import TableForQRcode from "../components/TableForQRcode";
import "../styles/GenQRCode.css";

const GenQRCode = () => {
  return (
    <div>
      <div className="QRCodePageTitle">
        <div className="QRCodePageTitleText">生成QRCode</div>
        <LogoutButton />
      </div>
      <br />
      <div className="homePageContainer">
        <div className="homePageSmallContainer left">
          <TableForQRcode />
        </div>
      </div>
    </div>
  );
};

export default GenQRCode;
