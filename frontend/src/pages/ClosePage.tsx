import React from "react";
// import './styles.css';
import Lottie from "react-lottie";
import animationData from "../lotties/74797-thank-you-with-confetti.json";
import { Typography } from "@mui/material";
// import Lottie from 'react-lottie-player'
// Alternatively:
// import Lottie from 'react-lottie-player/dist/LottiePlayerLight'

// import lottieJson from './my-lottie.json'

// export default function Example() {
//   return (
//     <Lottie
//       loop
//       animationData={lottieJson}
//       play
//       style={{ width: 150, height: 150 }}
//     />
//   )
// }

export default function ClosePage() {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <Typography variant="h3" component="h2">
      <Lottie options={defaultOptions} height={400} width={400} />
      感謝您的意見，
      <br />
      歡迎下次光臨！
    </Typography>
  );
}
