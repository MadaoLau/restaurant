import React from "react";
import LogoutButton from "../components/LogoutButton";
import StaffList from "../components/StaffList";
import "../styles/StaffManagePage.css";

const StaffManagePage = () => {
  // const staffList = () =>{
  //   return allStaffList.map
  // }
  return (
    <>
      <div className="staffManagePageTitle">
        <h1 className="header1">員工管理</h1>
        <h1 className="header1">
          <LogoutButton />
        </h1>
      </div>
      <br />
      <StaffList />
    </>
  );
};

export default StaffManagePage;
