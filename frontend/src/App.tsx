import "./App.css";
import { useEffect, useState } from "react";
import { Provider } from "react-redux";
import store, { history } from "./redux/store";
import { ConnectedRouter } from "connected-react-router";
// import { Route, BrowserRouter } from "react-router";
import io from "socket.io-client";
import PrivateRoute from "./components/PrivateRoute";
import { BrowserRouter, Route } from "react-router-dom";

//lazy loading for
// const NoMatch = lazy (()=>import("./pages/NoMatch"))
// const HomePage = lazy (()=>import("./pages/HomePage"))
// const OrderPage = lazy (()=>import("./pages/OrderPage"))
// const AdminManagePage = lazy (()=>import("./pages/AdminManagePage"))
// const LoginPage = lazy (()=>import("./pages/LoginPage"))
// const RegisterPage = lazy (()=>import("./pages/RegisterPage"))
// const OrderConfirm = lazy (()=>import("./pages/OrderConfirm"))
// const ManageMenuPage  = lazy (()=>import("./pages/ManageMenuPage"))
// const OrderManage = lazy (()=>import("./pages/OrderManage"))
// const StorageManagePage = lazy (()=>import("./pages/StorageManagePage"))
// const StaffManagePage = lazy (()=>import("./pages/StaffManagePage"))
// const KitchenPage = lazy (()=>import("./pages/KitchenPage"))
// const ClientOrderPage = lazy (()=>import("./pages/ClientOrderPage"))
// const FeedbackPage = lazy (()=>import("./pages/FeedbackPage"))
// const ClosePage  = lazy (()=>import("./pages/ClosePage"))

//normally loading
// import NoMatch from "./pages/NoMatch";
import HomePage from "./pages/HomePage";
import OrderPage from "./pages/OrderPage";
import AdminManagePage from "./pages/AdminManagePage";
import LoginPage from "./pages/LoginPage";
import RegisterPage from "./pages/RegisterPage";
import OrderConfirm from "./pages/OrderConfirm";
import ManageMenuPage from "./pages/ManageMenuPage";
import OrderManage from "./pages/OrderManage";
// import StorageManagePage from "./pages/GenQRCode";
import StaffManagePage from "./pages/StaffManagePage";
import KitchenPage from "./pages/KitchenPage";
import ClientOrderPage from "./pages/ClientOrderPage";
import FeedbackPage from "./pages/FeedbackPage";
import ClosePage from "./pages/ClosePage";
import GenQRCode from "./pages/GenQRCode";

const { REACT_APP_API_SERVER } = process.env;

function App() {
  const [socket, setSocket] = useState<any>(null);

  useEffect((): any => {
    const newSocket = io(`${REACT_APP_API_SERVER}`);
    setSocket(newSocket);
    return () => newSocket.close();
  }, [setSocket]);

  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <div className="App">

          <BrowserRouter>
            <Route path="/" exact={true} component={HomePage} />
            <Route path="/order" component={OrderPage} />
            {/* <Route path="/login" exact={true} component={LoginPage} /> */}
            <Route path="/login" exact={true} component={LoginPage} />
            <Route path="/showCase" exact={true} component={HomePage}/>
            <Route path="/register" exact={true} component={RegisterPage} />
            <Route path="/staff" exact={true} component={StaffManagePage} />
            <Route path="/qrCode" exact={true} component={GenQRCode} />
            <Route path="/close" exact={true} component={ClosePage} />
            <PrivateRoute
              path="/manage"
              exact={true}
              component={AdminManagePage}
            />
            <Route path="/feedback" exact={true} component={FeedbackPage} />
            <Route path="/manageMenu" component={ManageMenuPage} />
            <Route path="/orderConfirm" component={OrderConfirm} />
            <Route path="/orderManage">
              {socket ? <OrderManage socket={socket} /> : <></>}
            </Route>{" "}
            <Route path="/kitchenPage">
              {socket ? <KitchenPage socket={socket} /> : <></>}
            </Route>{" "}
            <Route path="/clientOrder">
              {socket ? <ClientOrderPage socket={socket} /> : <></>}
            </Route>{" "}
            {/* <Route path="*" component={NoMatch} /> */}
          </BrowserRouter>
        </div>
      </ConnectedRouter>
    </Provider>
  );
}

export default App;
