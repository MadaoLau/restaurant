// import { APIResultType } from "./state"

//About JWT Token


export function loadTokenAction(token: string) {
    return {
        type: "@@Auth/LOAD_TOKEN" as const,
        token
    }
}

export function msgClear() {
    return {
        type: "@@Auth/REGISTER_CLEAR" as const
    }
}

export function registerSuccess() {
    return {
        type: "@@Auth/REGISTER" as const
    }
}

export function registerFailed() {
    return {
        type: "@@Auth/REGISTER_FAILED" as const
    }
}

export function registerNameDuplicated() {
    return {
        type: "@@Auth/REGISTER_NAME_DUPLICATED_FAILED" as const
    }
}

export function registerEmailDuplicated() {
    return {
        type: "@@Auth/REGISTER_EMAIL_DUPLICATED_FAILED" as const
    }
}

export function loginSuccess() {
    return {
        type: "@@Auth/LOGIN" as const
    }
}

export function loginNameWrong() {
    return {
        type: "@@Auth/LOGIN_NAME_WRONG" as const
    }
}

export function loginPwWrong() {
    return {
        type: "@@Auth/LOGIN_PW_WRONG" as const
    }
}

export function loginFailed() {
    return {
        type: "@@Auth/LOGIN_FAILED" as const
    }
}

export function logoutAction() {
    return { type: '@@Auth/LOGOUT' as const }
}

export function getStaffDataAction(data:any){
    return{
        type:'@@Auth/STAFF_DATA' as const,
        data
    }
}

export function selectedStaff(staffId: number) {
    return {
        type: "@@Auth/SELECTED_STAFF_ID" as const,
        staffId
    }
}

// export function setLoginResultAction(result: APIResultType) {
//     return { type: '@@Auth/setLoginResult' as const, result }
// }
// export function setRegisterResultAction(result: APIResultType) {
//     return { type: '@@Auth/setRegisterResult' as const, result }
// }

export type IAuthAction =
    | ReturnType<typeof loginSuccess>
    | ReturnType<typeof loadTokenAction>
    | ReturnType<typeof logoutAction>
    | ReturnType<typeof loginFailed>
    | ReturnType<typeof registerSuccess>
    | ReturnType<typeof registerFailed>
    | ReturnType<typeof getStaffDataAction>
    | ReturnType<typeof selectedStaff>
    | ReturnType<typeof registerNameDuplicated>
    | ReturnType<typeof registerEmailDuplicated>
    | ReturnType<typeof msgClear>
    | ReturnType<typeof loginNameWrong>
    | ReturnType<typeof loginPwWrong>
