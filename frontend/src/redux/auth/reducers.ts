import { IAuthState } from "./state";
import { IAuthAction } from "./actions";
import jwt_decode from "jwt-decode";
import { JWTPayload } from "./state";

//james ver + cms

const initialState: IAuthState = {
  isAuthenticated: localStorage.getItem("token") != null,
  msg: "",
  user: loadExistToken(),
  list: null,
  staffId: null,
};

function loadExistToken() {
  const token = localStorage.getItem("token");
  if (token) {
    const payload: JWTPayload = jwt_decode(token);
    return { username: payload.username, is_admin: payload.is_admin };
  } else {
    return { username: "", is_admin: false };
  }
}

export const authReducers = (
  state: IAuthState = initialState,
  action: IAuthAction
): IAuthState => {
  switch (action.type) {
    case "@@Auth/REGISTER":
      return {
        ...state,
        msg: "註冊成功",
      };
    case "@@Auth/REGISTER_CLEAR":
      return {
        ...state,
        msg: "",
      };
    case "@@Auth/REGISTER_NAME_DUPLICATED_FAILED":
      return {
        ...state,
        msg: "用戶名稱已被使用",
      };
    case "@@Auth/REGISTER_EMAIL_DUPLICATED_FAILED":
      return {
        ...state,
        msg: "電郵已被使用",
      };

    case "@@Auth/REGISTER_FAILED":
      return {
        ...state,
        msg: "註冊失敗，請檢查及重新填寫資料",
      };
    case "@@Auth/LOGIN":
      return {
        ...state,
        isAuthenticated: true,
        msg: "Login Success",
      };
      case "@@Auth/LOGIN_NAME_WRONG":
      return {
        ...state,
        msg: "用戶名稱不正確",
      };
      case "@@Auth/LOGIN_PW_WRONG":
      return {
        ...state,
        msg: "密碼錯誤",
      };
    case "@@Auth/LOGIN_FAILED":
      return {
        ...state,
        msg: "Login Failed, Please check your username and password",
      };
    case "@@Auth/LOAD_TOKEN":
      const payload: JWTPayload = jwt_decode(action.token);
      const { username, is_admin } = payload;
      return {
        ...state,
        user: { username: username, is_admin: is_admin },
      };
    case "@@Auth/LOGOUT":
      return {
        ...state,
        isAuthenticated: false,
        msg: "",
      };
    case "@@Auth/STAFF_DATA":
      return {
        ...state,
        msg: "Staff Data Success",
        list: [...action.data],
      };
    case "@@Auth/SELECTED_STAFF_ID":
      return {
        ...state,
        msg: "Select Edit Staff Success",
        staffId: action.staffId,
      };
    default:
      return state;
  }
};
