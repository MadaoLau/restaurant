//In Tecky CMS
export interface IAuthState{
    isAuthenticated: boolean
    msg: string
    user: JWTPayload
    list: StaffDataList | null
    staffId: number | null
}

export interface IStaffData{
    id:number
    username:string
    email:string
    gender:string
    contact:string
    status:string | null
    shift:string | null
    is_admin:boolean
}

export type StaffDataList = IStaffData[]
export interface JWTPayload {
    username: string
    is_admin: boolean
}
