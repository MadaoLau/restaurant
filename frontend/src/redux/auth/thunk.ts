import { Dispatch } from "redux";
import {
  fetchDeleteStaff,
  fetchGetStaff,
  fetchLogin,
  fetchRegister,
  fetchUpdateStaff,
} from "../../api/auth";
import {
  getStaffDataAction,
  IAuthAction,
  loadTokenAction,
  loginFailed,
  loginSuccess,
  logoutAction,
  msgClear,
  registerNameDuplicated,
  registerEmailDuplicated,
  registerFailed,
  registerSuccess,
  loginNameWrong,
  loginPwWrong,
} from "./actions";
import { CallHistoryMethodAction } from "connected-react-router";

// import { useHistory } from "react-router-dom";

export function registerThunk(
  username: string,
  password: string,
  email: string,
  gender: string,
  contact: string
) {
  return async (dispatch: Dispatch<IAuthAction>) => {
    // console.log("thunk received:", username, password, email, gender, contact);
    const res = await fetchRegister(username, password, email, gender, contact);
    const result = await res.json();
    console.log("register Result:", result.message);
    if (result.message === "Create User Success") {
      dispatch(registerSuccess());
    }else if(result.message === 'username have been used') {
      dispatch(registerNameDuplicated())
    }else if(result.message === 'email have been used') {
      dispatch(registerEmailDuplicated())
    }
    else {
      dispatch(msgClear())
      dispatch(registerFailed());
    }
  };
}

export function loginThunk(username: string, password: string) {
  return async (dispatch: Dispatch<IAuthAction>) => {
    const res = await fetchLogin(username, password);
    const result = await res.json();
    console.log(result);
    if (result.message==='Wrong username') {
      dispatch(loginNameWrong())
    }else if(result.message==='Wrong password'){
      dispatch(loginPwWrong())
    } else if(result.message==='Login success'){
      localStorage.setItem("token", result.token);
      dispatch(loginSuccess());
      dispatch(loadTokenAction(result.token));
    } else {
      dispatch(loginFailed());
    }
  };
}

export function logoutThunk() {
  return async (dispatch: Dispatch<IAuthAction | CallHistoryMethodAction>) => {
    dispatch(logoutAction());
    localStorage.removeItem("token");
  };
}

export function getStaffThunk() {
  return async (dispatch: Dispatch<IAuthAction>) => {
    const res = await fetchGetStaff();
    const result = await res.json();
    dispatch(getStaffDataAction(result));
  };
}

export function deleteStaffThunk(staffId: number) {
  return async (dispatch: Dispatch<IAuthAction>) => {
    const res = await fetchDeleteStaff(staffId);
    if (res.ok) {
      const getRes = await (await fetchGetStaff()).json();
      dispatch(getStaffDataAction(getRes));
    }
  };
}

export function updateStaffThunk(
  staffId: number,
  username: string,
  email: string,
  gender: string,
  status: string,
  contact: string,
  shift: string,
  is_admin: string
) {
  return async (dispatch: Dispatch<IAuthAction>) => {
    const res = await fetchUpdateStaff(
      staffId,
      username,
      email,
      gender,
      status,
      contact,
      shift,
      is_admin
    );
    if (res.ok) {
      const getRes = await (await fetchGetStaff()).json();
      dispatch(getStaffDataAction(getRes));
    }
  };
}

//beeno's ver

// export function loginWithPasswordThunk(user: {
//   username: string
//   password: string
// }) {
//   return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
//     let json = await post('/api/user/login', user)
//     if (json.error) {
//       dispatch(setLoginResultAction({ type: 'fail', message: json.error }))
//     } else {
//       localStorage.setItem('token', json.token)
//       dispatch(
//         setLoginResultAction({
//           type: 'success',
//           token: json.token,
//         }),
//       )
//     }
//   }
// }
// export function registerThunk(user: { username: string; password: string }) {
//   return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
//     let json = await post('/user/register', user)
//     if (json.error) {
//       dispatch(setRegisterResultAction({ type: 'fail', message: json.error }))
//     } else {
//       localStorage.setItem('token', json.token)
//       dispatch(
//         setRegisterResultAction({
//           type: 'success',
//           token: json.token,
//         }),
//       )
//     }
//   }
// }

// export function postItem(item: { title: string }) {
//   return async (dispatch: IRootThunkDispatch, getState: () => IRootState) => {
//     let token = getState().auth.user?.token
//     let json = await postWithToken(token, '/item', item)
//     if (json.error) {
//       // dispatch a failed action
//       return
//     } else {
//       // dispatch a success action
//       return
//     }
//   }
// }

////////////////////////////////////////////////////////////////////////////////
//james+cms ver

// export function login(username:string,password:string){
//   return async (dispatch: Dispatch<IAuthAction>)=>{
//       const res = await fetch(`${REACT_APP_API_SERVER}/login`,{
//           method:"POST",
//           headers:{
//               "Content-Type":"application/json"
//           },
//           body: JSON.stringify({username,password})
//       });

//       const result = await res.json();

//       if(res.status != 200){
//           dispatch(failed("LOGIN_FAILED",result.msg));
//       }else{
//           localStorage.setItem('token',result.token);
//           dispatch(loginSuccess());
//           dispatch(push("/"));
//       }
//   }
// }

//CMS logout
// export function logout(){
//   return async (dispatch:Dispatch<IAuthAction>)=>{
//       // dispatch(logoutSuccess());
//       localStorage.removeItem('token');
//       // dispatch(push('/'));
//   }
// }
