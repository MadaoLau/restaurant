export function selectItemAction(id: number) {
  return {
    type: "@@Order/SELECT_ITEM" as const,
    itemId: id,
  };
}

export function addItemAction(foodID: number) {
  return {
    type: "@@Order/ADD_ITEM" as const,
    foodID,
  };
}

export function removeItemAction(foodID: number) {
  return {
    type: "@@Order/REMOVE_ITEM" as const,
    foodID,
  };
}

export function fetchDataSuccess(data: any) {
  return {
    type: "@@Order/FETCH_DATA_SUCCESS" as const,
    data,
  };
}

//Terence trying to fix +/- in orderManage
///////////////////////////////////////
export function addOrderItemAction(dishID: number) {
  return {
    type: "@@Order/ADD_ORDER_ITEM" as const,
    dishID,
  };
}

export function removeOrderItemAction(dishID: number) {
  return {
    type: "@@Order/REMOVE_ORDER_ITEM" as const,
    dishID,
  };
}

export function fetchOrderDataSuccess(data: any) {
  return {
    type: "@@Order/FETCH_Order_DATA_SUCCESS" as const,
    data,
  };
}
/////////////////////////////////////////////////


export function addOrderAction(data: any) {
  return {
    type: "@@Order/ADD_ORDER" as const,
    data,
  };
}

export function addClientIdAction(clientId: number) {
  return {
    type: "@@Order/ADD_CLIENT_ID" as const,
    clientId,
  };
}

export function removeClientIdAction(clientId: number) {
  return {
    type: "@@Order/REMOVE_CLIENT_ID" as const,
    clientId,
  };
}

export function showKitchenOrderAction(clientId: number) {
  return {
    type: "@@Order/SHOW_KITCHEN_ORDER" as const,
    clientId,
  };
}

export function showClientOrderAction(data:[], clientId: number) {
  return {
    type: "@@Order/SHOW_CLIENT_ORDER" as const,
    data,
    clientId,
  };
}


export type IOrderAction =
  | ReturnType<typeof selectItemAction>
  | ReturnType<typeof addItemAction>
  | ReturnType<typeof removeItemAction>
  | ReturnType<typeof fetchDataSuccess>
  | ReturnType<typeof addOrderAction>
  | ReturnType<typeof addClientIdAction>
  | ReturnType<typeof removeClientIdAction>
  | ReturnType<typeof showKitchenOrderAction>
  | ReturnType<typeof showClientOrderAction>
  | ReturnType<typeof addOrderItemAction>
  | ReturnType<typeof removeOrderItemAction>
  | ReturnType<typeof fetchOrderDataSuccess>
