import { CallHistoryMethodAction } from "connected-react-router";
import { Dispatch } from "redux";
import { fetchGetAllDishes } from "../../api/order";
import {
  fetchDataSuccess,
  IOrderAction,
} from "./action";

// const { REACT_APP_API_SERVER } = process.env;

export function getAllDishesThunk(allDishlist: any) {
  return async (dispatch: Dispatch<IOrderAction | CallHistoryMethodAction>) => {
    const is_ordering = allDishlist.filter((dish: any) => dish.order >= 1);
    console.log(`is_ordering: `, is_ordering);
    if (is_ordering.length >= 1) {
      return;
    }

    const res = await fetchGetAllDishes();
    let result = await res.json();
    result = result.map((dish: any) => {
      return { ...dish, order: 0 };
    });
    console.log(`result: `, result);

    dispatch(fetchDataSuccess(result));
  };
}

//Terence trying to fix +/- in orderManage
// export function getOrderThunk(orderList: any, clientId:string) {
//   return async (dispatch: Dispatch<IOrderAction | CallHistoryMethodAction>) => {
//     const is_ordering = orderList.filter((v: any) => v.order >= 1);
//     if (is_ordering.length >= 1) {
//       return;
//     }
//     let result = await fetchGetOrderToClient(clientId);
//     result = result.map((v: any) => {
//       return { ...v, order: 0 };
//     });
//     console.log(`result: `, result);
//     dispatch(fetchOrderDataSuccess(result));
//   };
// }

// export function addOrderThunk(newOrder: DishList) {
//   return async (dispatch: Dispatch<IOrderAction | CallHistoryMethodAction>) => {
//     // console.log("Received from confirm order: ", newOrder);

//     // await fetchAddOrder(newOrder);
//     const res = await fetchAddOrder(newOrder);
//     if (res.ok) {
//       const data = await res.json();
//       dispatch(fetchDataSuccess(data.items));
//     }
//   };
// }
// export function getAllTodoItemsThunk() {
//   return async (dispatch: Dispatch<ITodoAction>) => {
//     console.log("getAllTodoItemsThunk");

//     const res = await fetchGetAllTodoItems();
//     const result: any = await res.json();
//     const items: TodoItemState[] = result.items;
//     if (res.ok) {
//       const data = await res.json();
//       dispatch(fetchDataSuccess(data.items));
//     }
//   };
// }
