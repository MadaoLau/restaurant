export interface DishDetail {
  id: number;
  name: string;
  price: number;
  img: string;
  order: number;
  category: string;
}

export type FrontendNewOrder = {
  table_no?: string;
  status?: number;
  quantity: string;
  discount?: string | null;
  amount: number;
  dish_id: number;
};

export type DishList = DishDetail[];
export type FrontendNewOrderList = FrontendNewOrder[];

export interface IOrderState {
  list: DishList;
  selectedItemId: number | null;
  newOrder: FrontendNewOrderList;
  clientOrder: any[];
  clientId: number;
  kitchenStatus: boolean;
  orderList: any[]; //Terence trying to fix +/- in orderManage
}
