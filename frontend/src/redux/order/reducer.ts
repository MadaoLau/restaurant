import { loadClientId } from "../../components/TableForQRcode";
import { IOrderAction } from "./action";
import { DishDetail, IOrderState } from "./state";

const initialState: IOrderState = {
  list: [
    {
      category: "",
      id: 0,
      img: "",
      name: "",
      order: 0,
      price: 0,
    },
  ],
  selectedItemId: 0,
  newOrder: [],
  clientOrder: [],
  clientId: parseInt(loadClientId()),
  kitchenStatus: false,
  orderList: [],
};

export const orderReducer = (
  state: IOrderState = initialState,
  action: IOrderAction
): IOrderState => {
  switch (action.type) {
    case "@@Order/SELECT_ITEM": {
      let newItems = state.list.map((item): DishDetail => {
        if (item.id === action.itemId) {
          return {
            ...item,
          };
        }
        return item;
      });
      return { ...state, list: newItems };
    }
    case "@@Order/ADD_ITEM": {
      let newList = [...state.list];
      let editItem = newList.filter((item) => item.id === action.foodID)[0];
      editItem.order += 1;
      return { ...state, list: newList };
    }
    case "@@Order/REMOVE_ITEM": {
      let newList = [...state.list];
      let editItem = newList.filter((item) => item.id === action.foodID)[0];
      editItem.order -= 1;
      return { ...state, list: newList };
      // let newItems = state.list.map((item): DishDetail => {
      //   if (item.id === action.foodID) {
      //     return {
      //       ...item,
      //       order: item.order - 1,
      //     };
      //   }
      //   return item;
      // });
      // return { ...state, list: newItems };
    }
    case "@@Order/FETCH_DATA_SUCCESS": {
      return { ...state, list: [...action.data] };
    }

    //Terence trying to fix +/- in orderManage
    ///////////////////////////////////////
    // case "@@Order/FETCH_Order_DATA_SUCCESS": {
    //   return { ...state, orderList: [...action.data] };
    // }
    // case "@@Order/ADD_ORDER_ITEM": {
    //   let newOrderList = [...state.orderList];
    //   let editItem = newOrderList.filter((item) => item.id === action.dishID)[0];
    //   console.log(editItem)
    //   editItem.total_quantity = 1+editItem.total_quantity
    //   // console.log(editItem.total_quantity)
    //   return { ...state, orderList: newOrderList };
    // }
    // case "@@Order/REMOVE_ORDER_ITEM": {
    //   let newOrderList = [...state.orderList];
    //   let editItem = newOrderList.filter((item) => item.id === action.dishID)[0];
    //   editItem.total_quantity -= 1;
    //   return { ...state, orderList: newOrderList };
    // }
    ////////////////////////////////////////////

    case "@@Order/ADD_ORDER":
      return {
        ...state,
        newOrder: [...action.data],
      };
    case "@@Order/ADD_CLIENT_ID":
      let clientId = action.clientId + 1;
      localStorage.setItem("clientId", clientId.toString());
      return {
        ...state,
        clientId: clientId,
      };
    case "@@Order/REMOVE_CLIENT_ID":
      localStorage.removeItem("clientId");
      return {
        ...state,
        clientId: 0,
      };
    case "@@Order/SHOW_KITCHEN_ORDER":
      return {
        ...state,
      };
    case "@@Order/SHOW_CLIENT_ORDER":
      return {
        ...state,
        clientOrder: [...action.data],
      };
    default:
      return state;
  }
};
