import { MenuListState, MenuState } from "./state";

const initialState: MenuListState = {
  allDishes: [],
  selectedDishId: null,
};

//一開始既state

// const StorageKey = "menu-list";

// //first state
// const loadState = (): MenuListState => {
//   let text = localStorage.getItem("menu-list");
//   console.log('text', text);
//   if (!text) {
//     return initialState;
//   }
//   return JSON.parse(text);
// };

// //initial first > save
// const saveState = (state: MenuListState) => {
//   let text = JSON.stringify(state);
//   localStorage.setItem(StorageKey, text);
// };

// export const menuListReducer = (
//   state: MenuListState = initialState,
//   action: MenuListActionType
// ): MenuListState => {
//   let newState = menuListReducer_logic(state, action);
//   if (newState !== state) {
//     saveState(newState);
//   }
//   return newState;
// };

// export const menuListReducer_logic = (
//   state: MenuListState = initialState,
//   action: MenuListActionType
// ): MenuListState => {
//   switch (action.type) {
//     case "@@MenuList/selectItem":
//       return { ...state, selectedDishId: action.dishId };

//     // case "@@MenuList/addDish":
//     //   let maxId = Math.max(0, ...state.dishes.map((item) => item.id));
//     //   let newId = maxId + 1;
//     //   let newItem: MenuState = {
//     //     id: newId,
//     //     ...action.newItem,
//     //     // count: 0,
//     //     img: "pic",
//     //   };
//     //   return { dishes: [...state.dishes, newItem], selectedDishId: newId };

//     case "@@MenuList/updateName":
//       return {
//         ...state,
//         dishes: updateItem(state.dishes, action.dishId, (item) => ({
//           name: action.name,
//         })),
//       };
//     case "@@MenuList/updateDishPrice":
//       return {
//         ...state,
//         dishes: updateItem(state.dishes, action.dishId, (item) => ({
//           price: action.price,
//         })),
//       };
//     case "@@MenuList/updateDishCategory":
//       return {
//         ...state,
//         dishes: updateItem(state.dishes, action.dishId, (item) => ({
//           category: action.category,
//         })),
//       };

//     // case "@@DishList/addQuantity": {
//     //   let newList = [...state.dishes];
//     //   let editItem = newList.filter((item) => item.id === action.foodID)[0];
//     //   editItem.count += 1;
//     //   return { ...state, dishes: newList };
//     // }
//     // case "@@DishList/subtractQuantity": {
//     //   let newList = [...state.dishes];
//     //   let editItem = newList.filter((item) => item.id === action.foodID)[0];
//     //   editItem.count -= 1;
//     //   return { ...state, dishes: newList };
//     // }

//     default:
//       return state;
//   }
// };

// function updateItem(
//   dishes: MenuState[],
//   id: number,
//   updater: (dish: MenuState) => Partial<MenuState>
// ): MenuState[] {
//   let newDishes = dishes.map((dish): MenuState => {
//     if (dish.id === id) {
//       let patch = updater(dish);
//       return {
//         ...dish,
//         ...patch,
//       };
//     }

//     return dish;
//   });

//   return newDishes;
// }

function updateDish(
  dishes: MenuState[],
  id: number,
  //-3a- updater係一個fn, 目的食一個想改既item TodoState內容，因為應該只會想改一個target TodoState，所以叫Partial（部分），跟住return落patch variable裝住
  updater: (dish: MenuState) => Partial<MenuState>
): MenuState[] {
  let newDishes = dishes.map((dish): MenuState => {
    if (dish.id === id) {
      //-3b- patch係updater食完舊item
      let patch = updater(dish);
      return {
        //將舊item 同新patch item合埋
        ...dish,
        ...patch,
      };
    }
    //舊同新item 合埋，繼續map（因為map係要行array.length咁多次）
    return dish;
  });
  //經過map，行左N次後既成為完全體newItem
  return newDishes;
}

export const menuListReducer_logic = (
  state: MenuListState = initialState,
  action: any
): any => {
  switch (action.type) {
    case "@@MenuList/gotAllDishesAction":
      const allDishesWithNew: MenuState[] = [...action.data];
      console.log("gotAllDishesAction ...action.data:", allDishesWithNew);
      return {
        ...state,
        allDishes: allDishesWithNew,
      };

    case "@@MenuList/selectDishItem":
      let id = action.id;
      return {
        ...state,
        selectedDishId: id,
      };
    case "@@MenuList/addDish":
      return {
        ...state,
        selectedDishId: null,
      };
    // case "@@MenuList/deleteDish":

    //   return {
    //     ...state,
    //     selectedDishId: id,
    //   };

    // case "@@MenuList/selectDishItem":
    //   let id = action.id
    //   return { ...state, selectedDishId: id  };
    case "@@MenuList/updateName":
      return {
        ...state,
        dishes: updateDish(state.allDishes, action.dishId, (dish) => ({
          name: action.name,
        })),
      };
    case "@@MenuList/updateDishPrice":
      return {
        ...state,
        dishes: updateDish(state.allDishes, action.dishId, (dish) => ({
          price: action.price,
        })),
      };
    case "@@MenuList/updateDishCategory":
      return {
        ...state,
        dishes: updateDish(state.allDishes, action.dishId, (dish) => ({
          category: action.category,
        })),
      };
    default:
      return state;
  }
};
