import { CallHistoryMethodAction } from "connected-react-router";
import { Dispatch } from "react";
import {
  fetchAddDish,
  fetchDeleteDish,
  fetchGetAllDishes,
  fetchUpdate,
  fetchUpdateImg,
} from "../../api/manage";
import {
  addDishAction,
  gotAllDishesAction,
  MenuListActionType,
} from "./action";
import { MenuState } from "./state";
// import { NewItemDTO } from "./state";

export function getAllDishesThunk() {
  return async (
    dispatch: Dispatch<MenuListActionType | CallHistoryMethodAction>
  ) => {
    const res = await fetchGetAllDishes();
    const result: any = await res.json();
    console.log("frontend data get from server:", result);
    const allDishes: MenuState[] = result.allDishes;
    console.log("2frontend data get from server:", allDishes);

    if (res.ok) {
      dispatch(gotAllDishesAction(allDishes));
      dispatch(addDishAction());
    } else {
      console.log("error getAllDishesThunk");
    }
  };
}

// export function addNewDishesThunk(newDish: NewItemDTO) {
//   return async (
//     dispatch: Dispatch<MenuListActionType | CallHistoryMethodAction>
//   ) => {
//     console.log('thunk newDish:', newDish.img[0].name);
//     console.log('thunk newDish:', newDish.name);

//     const res = await fetchAddDish(newDish);
//     const result = await res.json();
//     console.log("dishID received from controller:", result);
//     if (res.ok) {
//       const allDishRes = await fetchGetAllDishes();
//       const getDbRes: any = await allDishRes.json();
//       // console.log("allDishes received from controller:", getDbRes);
//       const allDishes: MenuState[] = getDbRes.allDishes;
//       console.log("2allDishes received from controller:", allDishes);
//       dispatch(gotAllDishesAction(allDishes));
//       dispatch(addDishAction());
//     } else {
//       console.log("error addNewDishesThunk");
//     }
//   };
// }

// export function addNewDishesThunk(newDish: FormData) {
export function addNewDishesThunk(newDish: FormData) {
  return async (
    dispatch: Dispatch<MenuListActionType | CallHistoryMethodAction>
  ) => {
    console.log('thunk newDish:', newDish);
    // console.log('thunk newDish name:', newDish.name);

    const res = await fetchAddDish(newDish);
    const result = await res.json();
    console.log("dishID received from controller:", result);
    if (res.ok) {
      const allDishRes = await fetchGetAllDishes();
      const getDbRes: any = await allDishRes.json();
      // console.log("allDishes received from controller:", getDbRes);
      const allDishes: MenuState[] = getDbRes.allDishes;
      console.log("2allDishes received from controller:", allDishes);
      dispatch(gotAllDishesAction(allDishes));
      dispatch(addDishAction());
    } else {
      console.log("error addNewDishesThunk");
    }
  };
}

export function updateNameThunk(id: number, name: string) {
  return async (
    dispatch: Dispatch<MenuListActionType | CallHistoryMethodAction>
  ) => {
    // console.log("sending data: ", targetDish);

    const res = await fetchUpdate({ id: id, name: name });
    const result = await res.json();
    console.log("received from controller:", result);
    if (res.ok) {
      const allDishRes = await fetchGetAllDishes();
      const getDbRes: any = await allDishRes.json();
      // console.log("allDishes received from controller:", getDbRes);
      const allDishes: MenuState[] = getDbRes.allDishes;
      console.log("2allDishes received from controller:", allDishes);
      dispatch(gotAllDishesAction(allDishes));
    } else {
      console.log("error fetchUpdate");
    }
  };
}

export function updatePriceThunk(id: number, price: string) {
  return async (
    dispatch: Dispatch<MenuListActionType | CallHistoryMethodAction>
  ) => {
    const res = await fetchUpdate({ id: id, price: price });
    const result = await res.json();
    console.log("received from controller:", result);
    if (res.ok) {
      const allDishRes = await fetchGetAllDishes();
      const getDbRes: any = await allDishRes.json();
      // console.log("allDishes received from controller:", getDbRes);
      const allDishes: MenuState[] = getDbRes.allDishes;
      console.log("2allDishes received from controller:", allDishes);
      dispatch(gotAllDishesAction(allDishes));
      // dispatch()
    } else {
      console.log("error updatePriceThunk");
    }
  };
}

export function updateCategoryThunk(id: number, category: string) {
  return async (
    dispatch: Dispatch<MenuListActionType | CallHistoryMethodAction>
  ) => {
    const res = await fetchUpdate({ id: id, category: category });
    const result = await res.json();
    console.log("received from controller:", result);
    if (res.ok) {
      const allDishRes = await fetchGetAllDishes();
      const getDbRes: any = await allDishRes.json();
      // console.log("allDishes received from controller:", getDbRes);
      const allDishes: MenuState[] = getDbRes.allDishes;
      console.log("2allDishes received from controller:", allDishes);
      dispatch(gotAllDishesAction(allDishes));
    } else {
      console.log("error updateCategoryThunk");
    }
  };
}

export function updateImgThunk(id: number, img: File) {
  return async (
    dispatch: Dispatch<MenuListActionType | CallHistoryMethodAction>
  ) => {
    console.log('result----:', id, img);
    // const id = id.toString()
    const data = new FormData()
    data.append('id', id.toString())
    data.append('img', img)
    console.log('FormData',data);
    const res = await fetchUpdateImg(data);
    const result = await res.json();
    console.log("received newImage data from controller:", result);
    if (res.ok) {
      const allDishRes = await fetchGetAllDishes();
      const getDbRes: any = await allDishRes.json();
      // console.log("allDishes received from controller:", getDbRes);
      const allDishes: MenuState[] = getDbRes.allDishes;
      console.log("2allDishes received from controller:", allDishes);
      dispatch(gotAllDishesAction(allDishes));
    } else {
      console.log("error updateCategoryThunk");
    }
  };
}

export function deleteDishThunk(id: number) {
  return async (
    dispatch: Dispatch<MenuListActionType | CallHistoryMethodAction>
  ) => {
    const res = await fetchDeleteDish(id);
    if (res.ok) {
      const allDishRes = await fetchGetAllDishes();
      const getDbRes: any = await allDishRes.json();
      const allDishes: MenuState[] = getDbRes.allDishes;
      console.log("2allDishes received from controller:", allDishes);
      dispatch(gotAllDishesAction(allDishes));
      // dispatch(addDishAction());
    }
  };
}
