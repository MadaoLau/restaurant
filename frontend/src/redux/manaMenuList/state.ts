export type MenuListState = {
  allDishes: MenuState[];
  selectedDishId: number | null | string;
};

// export type MenuList = MenuState[];

export type MenuState = {
  id: number;
  name: string;
  price: number;
  category: string;
  // count: number;
  img: string;
};

export type NewItemDTO = {
  name: string;
  price: string;
  // img: string;
  img: any;
  category: string;
};

export type NewImg = {
  id: number;
  img: FileList;
};
