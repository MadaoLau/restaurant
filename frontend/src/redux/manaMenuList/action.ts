import { MenuState } from "./state";

export function gotAllDishesAction(data: MenuState[]) {
  return {
    type: "@@MenuList/gotAllDishesAction",
    data,
  };
}

export function selectDishAction(id: number) {
  return {
    type: "@@MenuList/selectDishItem" as const,
    id,
  };
}

export function deleteDishAction(id: number) {
  return {
    type: "@@MenuList/deleteDish" as const,
    id,
  };
}

//加item
export function addDishAction() {
  return {
    type: "@@MenuList/addDish" as const,
    // newItem,
  };
}

export function updateNameAction(id: number, name: string) {
  return {
    type: "@@MenuList/updateName" as const,
    dishId: id,
    name,
  };
}

export function updatePriceAction(id: number, price: string) {
  return {
    type: "@@MenuList/updateDishPrice" as const,
    dishId: id,
    price,
  };
}

export function updateCategoryAction(id: number, category: string) {
  return {
    type: "@@MenuList/updateDishCategory" as const,
    dishId: id,
    category,
  };
}

//加
// export function addQuantityAction(foodID: number) {
//   return {
//     type: "@@DishList/addQuantity" as const,
//     foodID,
//   };
// }
//減
// export function subtractQuantityAction(foodID: number) {
//   return {
//     type: "@@DishList/subtractQuantity" as const,
//     foodID,
//   };
// }

// type FAILED_INTENT =
//   | "@@MenuList/gotAllDishesAction_FAILED"
//   | "@@MenuList/addDish_FAILED";

export type MenuListActionType =
  | ReturnType<typeof gotAllDishesAction>
  | ReturnType<typeof addDishAction>
  | ReturnType<typeof selectDishAction>
  | ReturnType<typeof updateNameAction>
  | ReturnType<typeof updatePriceAction>
  | ReturnType<typeof updateCategoryAction>
  | ReturnType<typeof deleteDishAction>;

// | ReturnType<typeof addQuantityAction>
// | ReturnType<typeof subtractQuantityAction>;
