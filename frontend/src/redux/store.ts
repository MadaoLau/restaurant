import {
  Action,
  combineReducers,
  createStore,
  compose,
  applyMiddleware,
} from "redux";
import {
  RouterState,
  connectRouter,
  routerMiddleware,
  CallHistoryMethodAction,
} from "connected-react-router";
import thunk, { ThunkDispatch } from "redux-thunk";
import logger from "redux-logger";
import { IOrderAction } from "./order/action";
import { orderReducer } from "./order/reducer";
import { IOrderState } from "./order/state";
import { createBrowserHistory } from "history";
import { IAuthState } from "./auth/state";
import { authReducers } from "./auth/reducers";
import { IAuthAction } from "./auth/actions";
import { MenuListState } from "./manaMenuList/state";
import { menuListReducer_logic } from "./manaMenuList/reducer";
import { MenuListActionType } from "./manaMenuList/action";
export const history = createBrowserHistory();

export type IRootThunkDispatch = ThunkDispatch<IRootState, null, IRootAction>;
declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  }
}

export interface IRootState {
  order: IOrderState;
  menuList: MenuListState;
  auth: IAuthState;
  router: RouterState
}

export type IRootAction =
  | IOrderAction
  | MenuListActionType
  | IAuthAction
  | CallHistoryMethodAction;

const rootReducers = combineReducers<IRootState>({
  order: orderReducer,
  menuList: menuListReducer_logic,
  auth: authReducers,
  router: connectRouter(history)
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore<IRootState, IRootAction, {}, {}>(
  rootReducers,
  composeEnhancers(
    applyMiddleware(routerMiddleware(history)),
    applyMiddleware(thunk),
    applyMiddleware(logger)
  )
);
