import { Grid } from "@mui/material";
import { useSelector } from "react-redux";
import { IRootState } from "../redux/store";
import "../styles/OrderInfoBar.css";

export function OrderInfoBar(): any {
  const totalPrice = useSelector((state: IRootState) =>
    state.order.list.reduce((sum, item) => sum + item.price * item.order, 0)
  );
  const count = useSelector((state: IRootState) =>
    state.order.list.reduce((sum, item) => sum + item.order, 0)
  );

  return totalPrice ? (
    <div>
      <Grid className="orderInfoBarDetail">
        <Grid>已選數量：{count}</Grid>
        <Grid>價格：${totalPrice} </Grid>
      </Grid>

      {/* <b>點餐資料</b> */}
      {/* <div className="orderInfoBarDetail">
        <div>已選數量：{count}</div>
        <div>價格：${totalPrice} </div>
      </div> */}
      {/* <div></div> */}
      {/* <button>tick</button> */}
    </div>
  ) : (
    []
  );
}
