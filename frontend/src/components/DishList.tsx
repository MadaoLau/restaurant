import React from "react";
import { useSelector } from "react-redux";
import { IRootState } from "../redux/store";

const DishList = () => {
  const list = useSelector((state: IRootState) => state.order.list);
  console.log(`DishList list`, list);

  return (
    <div>
      {list.map((item, index) => (
        <div key={index}>
          <div>{item.id}</div>
          <div>{item.img}</div>
          <div>{item.name}</div>
          <div>{item.price}</div>
        </div>
      ))}
    </div>
  );
};

export default DishList;
