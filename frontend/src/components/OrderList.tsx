import React from "react";
import { Box, Grid, Card, Typography } from "@mui/material";
import { useSelector } from "react-redux";
import { IRootState } from "../redux/store";

const cardStyle = {
  bgcolor: "background.paper",
  display: "flex",
  borderRadius: "10px",
  mx: 1,
  my: 1.5,
  py: 2,
};

const wordStyle = {
  textAlign: "center",
  fontWeight: "bold",
};

const total = {
  fontWeight: "bold",
  borderBottomStyle: "double",
};

const OrderList = () => {
  const list = useSelector((state: IRootState) => state.order.list);
  // console.log(`Check order list`, list[0].price);

  const result = list.filter((item) => item.order > 0);

  console.log(`orderList: `, { result });

  const totalPrice = result
    .map((item) => item.order * item.price)
    .reduce((partialSum, a) => partialSum + a, 0);
  // console.log(`Total Price: `, totalPrice);

  return (
    <Box>
      <Grid>
        <Grid
          sx={{
            // borderBottom: 1,
            // borderTop: 1,
            display: "flex",
            pt: 1.5,
            mx: 1,
          }}
        >
          <Grid xs={2} style={{ fontWeight: "bold" }}>
            項例
          </Grid>
          <Grid item xs={4} sx={wordStyle}>
            名稱
          </Grid>
          <Grid item xs={3} sx={wordStyle}>
            數量
          </Grid>
          <Grid item xs={3} sx={wordStyle}>
            價錢
          </Grid>
        </Grid>
        {result.map((item, index) => (
          <Card key={index} sx={cardStyle}>
            <Grid item xs={2}>{index + 1}.</Grid>
            <Grid item xs={4}>{item.name}</Grid>
            <Grid item xs={3}>{item.order}</Grid>
            <Grid item xs={3}>${item.price * item.order}</Grid>
          </Card>
        ))}
        <Grid sx={{ display: "flex", py: 1.5 }}>
          <Grid item xs={2} style={{ fontWeight: "bold" }}></Grid>
          <Grid item xs={3} sx={wordStyle}></Grid>
          <Grid item xs={3} sx={wordStyle}></Grid>
          <Grid item xs={4} >
            <Typography sx={total}>合共：${totalPrice}</Typography>
          </Grid>
          <br />
          <br />
          <br />
          <br />
          <br />
        </Grid>
      </Grid>
    </Box>
  );
};

export default OrderList;
