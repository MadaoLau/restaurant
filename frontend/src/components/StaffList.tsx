import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { selectedStaff } from "../redux/auth/actions";
import {
  deleteStaffThunk,
  getStaffThunk,
  updateStaffThunk,
} from "../redux/auth/thunk";
import { IRootState } from "../redux/store";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";

interface IStaffForm {
  username: string;
  email: string;
  gender: string;
  status: string;
  contact: string;
  shift: string;
  is_admin: string;
}

const rightLeftContainer = {
  display: { xs: "contents", sm: "flex" },
  justifyContent: "center",
};

const displayFlex = {
  display: "flex",
  justifyContent: "center",
};

const flexWrap = {
  display: "flex",
  justifyContent: "center",
  flexWrap: "wrap",
};

const allStaffArea = {
  display: "flex",
  justifyContent: "center",
};

const staffBox = {
  border: 1,
  borderRadius: "5px",
  m: 1,
  p: 1,
  height: "fit-content",
};

const displayStaffBox = {
  // flexWrap:'wrap',
  mniWidth: "max-content",
};

const StaffList = () => {
  const { register, handleSubmit, reset } = useForm<IStaffForm>();
  const msg = useSelector((state: IRootState) => state.auth.msg);

  const [staffListArr, setStaffListArr] = useState<any[]>([]);
  const dispatch = useDispatch();
  const allStaffList = useSelector((state: IRootState) => state.auth.list);
  // const msg = useSelector((state:IRootState)=>state.auth.msg);
  const [staffId, setStaffId] = useState<any>();
  const selectItem = (
    id: number,
    username: any,
    email: any,
    gender: any,
    status: any,
    contact: any,
    shift: any,
    is_admin: any
  ) => {
    dispatch(selectedStaff(id));
    setStaffId(id);
    setStaffUsername(username);
    setStaffEmail(email);
    setStaffGender(gender);
    setStaffStatus(status);
    setStaffContact(contact);
    setStaffShift(shift);
    setStaffIs_admin(is_admin);
  };

  const [staffUsername, setStaffUsername] = useState(null);
  const [staffEmail, setStaffEmail] = useState(null);
  const [staffGender, setStaffGender] = useState(null);
  const [staffStatus, setStaffStatus] = useState(null);
  const [staffContact, setStaffContact] = useState(null);
  const [staffShift, setStaffShift] = useState(null);
  const [staffIs_admin, setStaffIs_admin] = useState(null);

  const onChangeName = (value: any) => {
    setStaffUsername(value);
  };
  const onChangeEmail = (value: any) => {
    setStaffEmail(value);
  };
  const onChangeGender = (value: any) => {
    setStaffGender(value);
  };
  const onChangeStatus = (value: any) => {
    setStaffStatus(value);
  };
  const onChangeContact = (value: any) => {
    setStaffContact(value);
  };
  const onChangeShift = (value: any) => {
    setStaffShift(value);
  };
  const onChangeIs_admin = (value: any) => {
    setStaffIs_admin(value);
  };

  useEffect(() => {
    dispatch(getStaffThunk());
    console.log("allStaffList:", allStaffList);
  }, []); //<== Dependency Array (空的時候, 只在第一次網頁觸發)

  useEffect(() => {
    if (!allStaffList || allStaffList.length === 0) {
      setStaffListArr([]);
      return;
    }

    let staffListArr: any = [];
    staffListArr.push(...allStaffList);
    console.log(allStaffList.map((staff) => staff.username));
    setStaffListArr(staffListArr);
  }, [allStaffList]);

  const onSubmit = (data: IStaffForm) => {
    const { username, email, gender, status, contact, shift, is_admin } = data;
    console.log(data);
    console.log(staffId);
    dispatch(
      updateStaffThunk(
        staffId,
        username,
        email,
        gender,
        status,
        contact,
        shift,
        is_admin
      )
    );
    reset();
  };

  const removeItem = (id: number) => {
    dispatch(deleteStaffThunk(id));
  };

  return (
    <Box id="entireContainer">
      <Box sx={rightLeftContainer} id="secondContainer">
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
          id="leftBox"
        >
          <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
            <AccountCircleIcon></AccountCircleIcon>
          </Avatar>
          <Typography sx={{ mb: 2 }} component="h2" variant="h5">
            所有員工
          </Typography>
          <Box sx={flexWrap} id="allStaffArea">
            {staffListArr.map((staff) => (
              <Grid sx={staffBox} key={staff.id} id="eachStaffBox">
                <Grid sx={displayStaffBox}>
                  <Grid sx={{ mb: 1 }}> 員工:{staff.username}</Grid>
                  <Button
                    sx={{ mr: 0.5 }}
                    variant="outlined"
                    onClick={() =>
                      selectItem(
                        staff.id,
                        staff.username,
                        staff.email,
                        staff.gender,
                        staff.status,
                        staff.contact,
                        staff.shift,
                        staff.is_admin
                      )
                    }
                  >
                    修改資料
                  </Button>
                  <Button
                    sx={{ ml: 0.5 }}
                    variant="outlined"
                    onClick={() => removeItem(staff.id)}
                  >
                    刪除員工
                  </Button>
                </Grid>
                {/* <p>電郵:{staff.email}</p>
            <p>性別:{staff.gender}</p>
            <p>職位:{staff.is_admin ? "Admin" : "Staff"}</p>
            <p>狀況:{staff.status ? "Check-in" : "Check-Out"}</p>
            <p>電話:{staff.contact}</p>
            <p>更表:{staff.shift ? staff.shift : "TBC"}</p> */}
              </Grid>
            ))}
          </Box>
        </Box>
        {staffListArr
          .filter((staff) => staff.id == staffId)
          .map((staff) => (
            <Container component="main" maxWidth="xs">
              <CssBaseline />
              <Box
                sx={{
                  // marginTop: 8,
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
                  <LockOutlinedIcon />
                </Avatar>
                <Typography component="h2" variant="h5">
                  修改資料
                </Typography>
                <Box
                  onSubmit={handleSubmit(onSubmit)}
                  component="form"
                  noValidate
                  // onSubmit={handleSubmit}
                  sx={{ mt: 3 }}
                >
                  <Grid>
                    <Grid item sx={{ mb: 2 }}>
                      <TextField
                        type="text"
                        placeholder="請輸入姓名"
                        {...register("username", {})}
                        // required
                        fullWidth
                        id="username"
                        label="姓名"
                        name="username"
                        // autoComplete="username"
                        value={staffUsername}
                        onChange={(e) => {
                          onChangeName?.(e.currentTarget.value);
                          // console.log('currentValue',e.currentTarget.value);
                        }}
                      />
                    </Grid>
                    <Grid item sx={{ mb: 2 }}>
                      <TextField
                        type="text"
                        placeholder="請輸入電郵"
                        {...register("email", {})}
                        // required
                        fullWidth
                        name="email"
                        label="電郵"
                        id="email"
                        // autoComplete="email"
                        value={staffEmail}
                        onChange={(e) => {
                          onChangeEmail?.(e.currentTarget.value);
                        }}
                      />
                    </Grid>
                    <Grid item sx={{ mb: 2 }}>
                      <TextField
                        type="text"
                        placeholder="請輸入性別"
                        {...register("gender", {})}
                        // required
                        fullWidth
                        name="gender"
                        label="性別"
                        id="gender"
                        // autoComplete="gender"
                        value={staffGender}
                        onChange={(e) => {
                          onChangeGender?.(e.currentTarget.value);
                        }}
                      />
                    </Grid>
                    <Grid item sx={{ mb: 2 }}>
                      <TextField
                        type="text"
                        placeholder="請輸入上班狀況"
                        {...register("status", {})}
                        // required
                        fullWidth
                        name="status"
                        label="狀況"
                        id="status"
                        // autoComplete="status"
                        value={staffStatus}
                        onChange={(e) => {
                          onChangeStatus?.(e.currentTarget.value);
                        }}
                      />
                    </Grid>
                    <Grid item sx={{ mb: 2 }}>
                      <TextField
                        type="text"
                        placeholder="請輸入電話"
                        {...register("contact", {})}
                        // required
                        fullWidth
                        name="contact"
                        label="電話"
                        id="contact"
                        // autoComplete="contact"
                        value={staffContact}
                        onChange={(e) => {
                          onChangeContact?.(e.currentTarget.value);
                        }}
                      />
                    </Grid>
                    <Grid item sx={{ mb: 2 }}>
                      <TextField
                        type="text"
                        // placeholder="請輸入更表"
                        {...register("shift", {})}
                        // required
                        fullWidth
                        name="shift"
                        label="更表"
                        id="shift"
                        // autoComplete="shift"
                        value={staffShift}
                        onChange={(e) => {
                          onChangeShift?.(e.currentTarget.value);
                        }}
                      />
                    </Grid>
                    <Grid item sx={{ mb: 2 }}>
                      <TextField
                        type="text"
                        placeholder="請輸入職位"
                        {...register("is_admin", {})}
                        // required
                        fullWidth
                        name="is_admin"
                        label="職位"
                        id="is_admin"
                        // autoComplete="is_admin"
                        value={staffIs_admin}
                        onChange={(e) => {
                          onChangeIs_admin?.(e.currentTarget.value);
                        }}
                      />
                    </Grid>
                  </Grid>
                  <Button
                    type="submit"
                    // fullWidth
                    variant="contained"
                    // sx={{ mt: 3, mb: 2 }}
                  >
                    更改
                  </Button>
                </Box>
              </Box>
              {/* <Copyright sx={{ mt: 5 }} /> */}
            </Container>
          ))}
      </Box>
    </Box>
  );
};

export default StaffList;
