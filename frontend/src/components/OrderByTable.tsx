import { Box, Button, Grid, Paper, styled } from "@mui/material";
import { useState } from "react";
import { fetchFinishOrder } from "../api/order";

export const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

function OrderByTableHelper({ data }: any) {
  const [status, setStatus] = useState<any>(false);
  console.log(data);

  const finishByKitchen = () => {
    setStatus(true);
  };
  const cancelByKitchen = () => {
    setStatus(false);
  };
  const finishTable = async (clientId: string) => {
    await fetchFinishOrder(clientId);
    setStatus(false);
  };

  return (
    <Grid item md={3}>
      <Item sx={{ backgroundColor: "#FFC401", color: "black" }}>
        <Box sx={{ fontWeight: "bold", fontSize: "3em" }}>{data.name}號枱</Box>

        {data.value.map((v: any, i: number) => (
          <Box key={i + "v"} sx={{ fontWeight: "bold", fontSize: "1.5em" }}>
            <span>
              {" "}
              {v.name} : {v.total_quantity}{" "}
            </span>
          </Box>
        ))}

        {data.remark.map((v: any, i: number) => (
          <Box key={i + "v"} sx={{ fontWeight: "bold", fontSize: "1.5em" }}>
            <span> {v.req}</span>
          </Box>
        ))}

        {status ? (
          <Button
            sx={{
              fontWeight: "bold",
              fontSize: "1.5em",
              mr: 1,
            }}
            variant="contained"
            onClick={cancelByKitchen}
          >
            還原
          </Button>
        ) : (
          <Button sx={{ fontWeight: "bold", fontSize: "1.5em" }} variant="contained" onClick={finishByKitchen}>
            完成
          </Button>
        )}

        {status && (
          <Button sx={{ fontWeight: "bold", fontSize: "1.5em" }} variant="contained" onClick={() => finishTable(data.value[0].client_id)}>
            確認
          </Button>
        )}

        {/* </Grid> */}
      </Item>
    </Grid>
  );
}

function OrderByTable({ remark, data }: { remark: any; data: any }): any {
  // Group by table no. dish
  console.log("------------", data);

  let newData: any = new Map();

  data.forEach((v: any, i: number) => {
    if (newData.has(v.table_no)) {
      let temp = newData.get(v.table_no);
      temp.push({
        name: v.name,
        id: v.id,
        total_quantity: v.total_quantity,
        client_id: v.client_id,
      });
      newData.set(v.table_no, temp);
    } else {
      newData.set(v.table_no, [
        {
          name: v.name,
          id: v.id,
          total_quantity: v.total_quantity,
          client_id: v.client_id,
        },
      ]);
    }
  });

  // turn map to a normal array
  let arr: any = [...newData].map(([name, value]) => ({ name, value }));

  for (let i = 0; i < arr.length; i++) {
    const currentclientId: number = arr[i].value[0].client_id;

    const markfiltedArr: any = remark.filter((v: any) => v.client_id === currentclientId);
    arr[i]["remark"] = markfiltedArr;
  }

  console.log(arr);

  return arr.map((item: any) => (
    <OrderByTableHelper data={item} key={item.index} />
  ));
}

export default OrderByTable;
