import { Box } from "@mui/material";
import React from "react";

export default function DateOnly() {
  let d = new Date(),
    dayFormat = [d.getMonth() + 1, d.getDate(), d.getFullYear()].join("/");
  return <Box sx={{alignItems:'center', paddingTop:'0.5em',paddingLeft:'1em', fontWeight:'bold'}}>{dayFormat}</Box>;
}
