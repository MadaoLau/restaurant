import React, { useEffect, useState } from "react";
import QRCode from "qrcode.react";
import Form from "react-bootstrap/esm/Form";
import Button from "@mui/material/Button";
import { useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { addClientIdAction, removeClientIdAction } from "../redux/order/action";
import { IRootState } from "../redux/store";
import "../styles/HomePage.css";
import { Box } from "@mui/material";
interface ITableForm {
  tableNo: string;
}

export function loadClientId() {
  let localClientId = localStorage.getItem("clientId");
  if (localClientId) {
    return localClientId;
  } else {
    return "0";
  }
}

const TableForQRcode = () => {
  const dispatch = useDispatch();
  const [tableNo, setTableNo] = useState<number | null>(null);
  const clientId = useSelector((state: IRootState) => state.order.clientId);
  const { register, handleSubmit } = useForm<ITableForm>();
  const onSubmit = (data: ITableForm) => {
    setTableNo(+data.tableNo);
    dispatch(addClientIdAction(clientId));
  };

  useEffect(() => {
    let localClientId = localStorage.getItem("clientId");
    if (!localClientId) {
      localStorage.setItem("clientId", "0");
    }
  }, []);

  return (
    <div>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Form.Group>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <h4 style={{ fontSize: "1.1rem" }}>輸入枱號:</h4>
            <Form.Control
              className="tableInput"
              type="text"
              placeholder="枱號"
              {...register("tableNo", {})}
            />
            <Button type="submit" variant="contained" sx={{ margin: "0.1rem" }}>
              生成QRCode
            </Button>
          </Box>
        </Form.Group>
      </Form>
      {/* wannadim.xyz for deploy using // localhost:3000 for develop using*/}
      {tableNo && (
        <QRCode
          value={
            "https://wannadim.xyz/order?table=" +
            tableNo +
            "&clientId=" +
            clientId +
            "&orderNo=1"
          }
          renderAs="canvas"
        />
      )}

      {/* <div>
        {"https://wannadim.xyz/order?table=" +
          tableNo +
          "&clientId=" +
          clientId +
          "&orderNo=1"}
      </div> */}

      {/* <QRCode value={"http://localhost:3000"+"/order?table="+tableNo+"&clientId="+clientId+"&orderNo=1"} renderAs="canvas" />
            <div>{"http://localhost:3000"+"/order?table="+tableNo+"&clientId="+clientId+"&orderNo=1"}</div> */}
      <div>
        {tableNo && (
          <Box>
            <Box
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Box className="check-in-detail">枱號:{tableNo}</Box> /
              <Box className="check-in-detail" style={{ alignSelf: "center" }}>
                單號:{clientId}{" "}
                <Button
                  variant="outlined"
                  onClick={() => dispatch(removeClientIdAction(clientId))}
                  sx={{}}
                  size="small"
                >
                  重設單號
                </Button>
              </Box>
            </Box>

            <br />
            <br />
            <Button
              variant="contained"
              sx={{ width: "90%", fontSize: "1.1rem" }}
              href={
                "/order?table=" +
                tableNo +
                "&clientId=" +
                clientId +
                "&orderNo=1"
              }
            >
              客人點餐
            </Button>
          </Box>
        )}
      </div>
    </div>
  );
};

export default TableForQRcode;
