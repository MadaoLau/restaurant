import {
  Box,
  Button,
  Grid,
  Rating,
  TextField,
  Typography,
} from "@mui/material";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { fetchPostFeedback } from "../api/feedback";
import { useQuery } from "../pages/OrderConfirm";

interface IFeedbackForm {
  food_quality: any;
  service: any;
  cleanness: any;
  suggestion: any;
  client_id: any;
}
const header = {
  display: "flex",
  justifyContent: "center",
  fontSize: "2rem",
  backgroundColor: "#ffc400",
};

const boxEle = {
  "& > legend": { mt: 1 },
  display: "flex",
  justifyContent: "center",
};

const displayMid = {
  display: "flex",
  justifyContent: "center",
};

export default function Feedback() {
  const history = useHistory();
  const [valueOfFoodQuality, setValueOfFoodQuality] = useState<number | null>();
  const [valueOfService, setValueOfService] = useState<number | null>();
  const [valueOfCleanness, setValueOfCleanness] = useState<number | null>();
  const [valueOfSuggestion, setValueOfSuggestion] = useState<string | null>("");
  const [alertMsg, setAlertMsg] = useState<string | null>("");

  // const tableNo = useQuery().get("table");
  // const orderNo = useQuery().get("orderNo");
  const clientId = useQuery().get("clientId");


  const onSubmit = async (data: IFeedbackForm) => {
    console.log(data);
    if (
      data.food_quality === undefined ||
      data.service === undefined ||
      data.cleanness === undefined
    ) {
      console.log("failed");
      setAlertMsg("請確保三項評分已選, 再重新提交");
      return;
    }
    await fetchPostFeedback(
      data.food_quality,
      data.service,
      data.cleanness,
      data.suggestion,
      data.client_id
    );
    history.push("/close");
  };
  return (
    <Box sx={{}}>
      <Grid sx={header}>
        <Typography
          sx={{ fontSize: "1.5rem", fontWeight: "bold", color: "white" }}
        >
          問卷調查
        </Typography>
      </Grid>
      {/* Ranking */}
      <Box sx={{ px: 12, mb: 1 }}>
        <Box sx={boxEle}>
          <Typography
            sx={{
              fontSize: "1.5em",
              color: "primary.main",
              fontWeight: "bold",
            }}
            component="legend"
          >
            食物質素
          </Typography>
        </Box>

        {/* <Rating
                        {...register('food_quality')}
                    /> */}
        <Grid sx={displayMid}>
          <Rating
            name="food_quality"
            value={valueOfFoodQuality}
            onChange={(event: any) => {
              setValueOfFoodQuality(event.currentTarget.value);
            }}
          />
        </Grid>
        <Box sx={boxEle}>
          <Typography
            sx={{
              fontSize: "1.5em",
              color: "primary.main",
              fontWeight: "bold",
            }}
            component="legend"
          >
            服務質素
          </Typography>
        </Box>

        {/* <Rating
                        {...register('service')}
                    /> */}
        <Grid sx={displayMid}>
          <Rating
            name="service"
            value={valueOfService}
            onChange={(event: any) => {
              setValueOfService(event.currentTarget.value);
            }}
          />
        </Grid>
        <Box sx={boxEle}>
          <Typography
            sx={{
              fontSize: "1.5em",
              color: "primary.main",
              fontWeight: "bold",
            }}
            component="legend"
          >
            潔淨程度
          </Typography>
        </Box>
        {/* <Rating
                        {...register('cleanness')}
                    /> */}
        <Grid sx={displayMid}>
          <Rating
            value={valueOfCleanness}
            onChange={(event: any) => {
              setValueOfCleanness(event.currentTarget.value);
            }}
          />
        </Grid>
      </Box>
      {/* Comment*/}
      <Box>
        <Grid sx={displayMid}>
          <TextField
            id="outlined-multiline-static"
            multiline
            rows={5}
            value={valueOfSuggestion}
            onChange={(event: any) => {
              setValueOfSuggestion(event.currentTarget.value);
            }}
          />
        </Grid>
        <Grid sx={displayMid}>
          <Button
            sx={{
              my: 2,
              color: "primary.main",
              fontWeight: "bold",
            }}
            variant="outlined"
            onClick={() =>
              onSubmit({
                food_quality: valueOfFoodQuality,
                service: valueOfService,
                cleanness: valueOfCleanness,
                suggestion: valueOfSuggestion,
                client_id: clientId
              })
            }
          >
            提交
          </Button>
        </Grid>
      </Box>
      <Box sx={{ display: "flex", justifyContent: "center" }}>
        {alertMsg && <Box>{alertMsg}</Box>}
      </Box>
    </Box>
  );
}
