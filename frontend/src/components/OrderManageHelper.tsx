import { Box, Button } from "@mui/material";
import {
  fetchDeleteOrderEachDish,
  fetchGetAllDishesToKitchen,
  fetchGetOrderToClient,
} from "../api/order";

const OrderManageHelper = ({ data }: any) => {
  const removeOrderDish = async (clientId: string, id: number) => {
    const res = await fetchDeleteOrderEachDish(clientId, id);
    if (res.ok) {
      await fetchGetOrderToClient(clientId!);
      console.log("deleted");
      await fetchGetAllDishesToKitchen();
    } else {
      console.log("failed");
    }
  };

  return (
    <Box>
      {
        <Box
          key={data.id}
          sx={{
            display: "flex",
            justifyContent: "space-between",
            pl: "10px",
            pr: "10px",
          }}
        >
          <Box>
            {data.name} {data.total_quantity}
          </Box>

          <Box>{"$" + data.total_amount}</Box>
          <Button
                sx={{margin:'0.8em', fontSize:'1.5em', fontWeight:'bold'}}
                variant="contained"
            onClick={() => removeOrderDish(data.client_id, data.id)}
          >
            刪除
          </Button>
        </Box>
      }
    </Box>
  );
};

export default OrderManageHelper;
