// import { Button } from "react-bootstrap";
import { Logout } from "@mui/icons-material";
import { Box } from "@mui/material";
import Button from "@mui/material/Button";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { logoutThunk } from "../redux/auth/thunk";
import { IRootState } from "../redux/store";
import "../styles/LoginPage.css"


export default function LogoutButton() {
  const isAuthenticated = useSelector(
    (state: IRootState) => state.auth.isAuthenticated
  );
  const history = useHistory();
  const dispatch = useDispatch();

  const clickLogout = () => {
    dispatch(logoutThunk());
    history.push("/");
  };

  return (
    <div className="logout-bar">
      {isAuthenticated ? (
          <Button
            className="logout-button"
            onClick={clickLogout}
          >
            <Box className="logoutText">登出</Box>
            <Logout style={{ color: "white" }} />
        </Button>
      ) : (
        ""
      )}
    </div>
  );
}
