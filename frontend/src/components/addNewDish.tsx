import React from "react";
import { useForm } from "react-hook-form";
import { NewItemDTO } from "../redux/manaMenuList/state";
import {
  Button,
  Collapse,
  List,
  ListItemIcon,
  Paper,
  Grid,
  Box,
  TextField,
} from "@mui/material";
import { ExpandLess, ExpandMore } from "@mui/icons-material";
import { addNewDishesThunk } from "../redux/manaMenuList/thunk";
import { useDispatch } from "react-redux";


const textFiledMb = {
  mb:1,
}

export function AddNewDish() {
  const dispatch = useDispatch();
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(!open);
  };

  const { register, handleSubmit, reset } = useForm<NewItemDTO>({
    defaultValues: {
      name: "",
      price: "",
      img: "",
      category: "",
    },
  });

  const submit = (data: NewItemDTO) => {
    console.log("newDish data:", data);
    console.log("newDish formData", data.img);
    const newData = new FormData();
    let price = data.price;
    // if (price == null) {
    //   price = "0";
    // }
    newData.append("name", data.name);
    newData.append("price", price);
    newData.append("img", data.img[0]);
    newData.append("category", data.category);
    console.log("newData:", newData);
    console.log("form submit:", data);
    dispatch(addNewDishesThunk(newData));
    reset();
  };

  return (
    <Paper
      sx={{
        p: 2,
        margin: "auto",
        maxWidth: 500,
        backgroundColor: (theme) =>
          theme.palette.mode === "dark" ? "#1A2027" : "#fff",
        marginBottom: 1,
        pb: 0,
      }}
    >
      <Grid
        container
        spacing={2}
        id="bbbbbbbbb"
        sx={{ ml: 0, display: "block" }}
      >
        <Grid id="tttttttttt">
          <Button sx={{}} onClick={handleClick}>
            <ListItemIcon
              sx={{ fontSize: "1.2rem", color: "#3675CB", fontWeight: "bold" }}
            >
              新增菜式
            </ListItemIcon>
            {open ? <ExpandLess /> : <ExpandMore />}
          </Button>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <List
              component="div"
              disablePadding
              sx={{ display: "flex", justifyContent: "center" }}
            >
              <Box
                onSubmit={handleSubmit(submit)}
                component="form"
                noValidate
                sx={{ mt: 3 }}
              >
                <Grid
                  sx={{ mt: -3, mb: 1 }}
                >
                  <TextField
                    type="text"
                    {...register("name")}
                    placeholder="輸入菜式名稱"
                    id="new-menu--name"
                    fullWidth
                    size="small"
                    label="菜式名稱"
                    // autoComplete="菜式名稱"
                    onChange={(name) => name.currentTarget.value}
                  />
                </Grid>
                <Grid
                  sx={textFiledMb}
                  // item xs={12}
                >
                  <TextField
                    type="text"
                    {...register("price")}
                    placeholder="輸入價錢"
                    id="new-menu--price"
                    fullWidth
                    size="small"
                    label="價錢"
                    // autoComplete="價錢"
                    onChange={(price) => price.currentTarget.value}
                  />
                </Grid>
                <Grid
                  sx={textFiledMb}

                  // item xs={12} sx={{mb:4}}
                >
                  <TextField
                    type="text"
                    {...register("category")}
                    placeholder="輸入菜式種類"
                    id="new-menu--category"
                    fullWidth
                    size="small"
                    label="種類"
                    // autoComplete="種類"
                    onChange={(category) => category.currentTarget.value}
                  />
                </Grid>
                <Button
                  variant="outlined"
                  component="label"
                  sx={{ fontWeight: "bold", mr: 0.5, mb: 1, size: "small" }}
                >
                  上載圖片
                  <input
                    type="file"
                    hidden
                    {...register("img")}
                    id="new-menu--img"
                    name="img"
                  />
                </Button>

                <Button
                  type="submit"
                  sx={{
                    fontWeight: "bold",
                    ml: 0.5,
                    p: 0.5,
                    mb: 1,
                  }}
                  variant="outlined"
                >
                  Submit
                </Button>
              </Box>
            </List>
          </Collapse>
        </Grid>
      </Grid>
    </Paper>
  );
}
