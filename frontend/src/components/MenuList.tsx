import * as React from "react";
import Tabs, { tabsClasses } from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

import { useDispatch, useSelector } from "react-redux";
import { IRootState } from "../redux/store";
import { useEffect, useState } from "react";
import { Button, Card, CardMedia, Grid } from "@mui/material";
import { addItemAction, removeItemAction } from "../redux/order/action";
import { getAllDishesThunk } from "../redux/order/thunk";
import { DishList } from "../redux/order/state";
// import {createTheme, ThemeProvider} from "@mui/material/styles"
export interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}
const { REACT_APP_API_SERVER } = process.env;
export function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 1 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

export function a11yProps(index: number) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

export default function VerticalTabs() {
  const [value, setValue] = React.useState(0);
  const [dishArr, setDishArr] = useState<DishList>([]);
  const [categoryArr, setCategoryArr] = useState<string[]>([]);
  const dispatch = useDispatch();

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  const allDishlist = useSelector((state: IRootState) => state.order.list);
  useEffect(() => {
    dispatch(getAllDishesThunk(allDishlist));
    // console.log(`allDishlist: `, allDishlist);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (!allDishlist || allDishlist.length === 0) {
      setDishArr([]);
      return;
    }

    // const is_ordering = allDishlist.filter((v: any) => v.order >= 1);
    // if (is_ordering.length >= 1) {
    //   return;
    // }
    // console.log(`1234`);

    let uniqueCategory = Array.from(
      new Set(allDishlist.map((dish) => dish.category))
    ) as string[];
    setCategoryArr(uniqueCategory);

    let dishArr: any = [];
    for (let item of uniqueCategory) {
      dishArr.push(allDishlist.filter((dish: any) => dish.category === item));
    }

    setDishArr(dishArr);
  }, [allDishlist]);

  // console.log("initial:", dishArr);

  const inc = (foodID: number) => {
    dispatch(addItemAction(foodID));
    // console.log(allDishlist);
    // setCount(count + 1);
  };
  const dec = (foodID: number) => {
    dispatch(removeItemAction(foodID));
    // console.log(allDishlist);
  };

  function GenTabPanel({ data, index }: any) {
    // console.log("data, index?:", { data, index });

    return (
      <TabPanel value={value} index={index}>
        {data.map((dish: any, index: number) => (
          <Card key={index} sx={{ mb: 1, py: 1 }}>
            {/* <Grid container spacing={1}> */}
            <Grid
              container
              spacing={1}
              sx={{
                display: "flex",
                justifyContent: "space-around",
                alignItems: "center",
              }}
            >
              <Grid
                item
                xs={4}
                sx={{
                  borderColor: "divider",
                  display: "flex",
                  justifyContent: "center",
                }}
              >
                <CardMedia
                  style={{
                    width: "auto",
                    maxHeight: "80px",
                    padding: "20px",
                  }}
                  component="img"
                  image={`${REACT_APP_API_SERVER}/` + dish.img}
                  className="dishImg"
                />
              </Grid>

              <Grid
                item
                xs={4}
                sx={{
                  fontSize: "0.9rem",
                  // borderLeft: 1,
                  // borderRight: 1,
                  display: "flex",
                  borderColor: "divider",
                  justifyContent: "center",
                }}
              >
                <Grid className="namePrice">
                  <Grid sx={{ fontSize: "1.02rem" }}>{dish.name}</Grid>
                  <Grid>$ {dish.price}</Grid>
                </Grid>
              </Grid>

              <Grid
                item
                xs={4}
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  borderColor: "divider",
                }}
              >
                <Button
                  sx={{ maxWidth: "64", minWidth: "40px" }}
                  disabled={dish.order <= 0}
                  onClick={() => dec(dish.id)}
                >
                  –
                </Button>
                <p>{+dish.order}</p>
                <Button
                  sx={{ minWidth: "40px", maxWidth: "64px" }}
                  onClick={() => inc(dish.id)}
                >
                  ＋
                </Button>
              </Grid>
            </Grid>
          </Card>
        ))}
      </TabPanel>
    );
  }

  function GenTabPanelList({ dishArr }: any): any {
    // console.log("dishArr?:", dishArr); // 一次顯示所有餸的詳細內容

    return dishArr.map((dish: any, i: number) => (
      <GenTabPanel data={dish} index={i} key={i} />
    ));
  }

  return (
    <>
      <Box
        sx={{
          flexGrow: 1,
          display: "inline-grid",
          bgcolor: "background.paper",
          maxWidth: { xs: 320, sm: 720 },
          // height: "100%",
        }}
      >
        <Tabs
          orientation="horizontal"
          variant="scrollable"
          value={value}
          scrollButtons
          onChange={handleChange}
          aria-label="visible arrows tabs example"
          sx={{
            [`& .${tabsClasses.scrollButtons}`]: {
              "&.Mui-disabled": { opacity: 0.3 },
            },
          }}
        >
          {categoryArr.map((category: any, i: number) => (
            <Tab key={i} label={category} {...a11yProps(i)} />
          ))}
        </Tabs>
      </Box>
      <Box>
        <GenTabPanelList dishArr={dishArr} />
      </Box>
    </>
  );
}
