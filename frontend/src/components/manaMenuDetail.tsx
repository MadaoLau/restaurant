import { IRootState } from "../redux/store";
import { useDispatch, useSelector } from "react-redux";
import { MenuState } from "../redux/manaMenuList/state";
import {
  updateCategoryThunk,
  updateImgThunk,
  updateNameThunk,
  updatePriceThunk,
} from "../redux/manaMenuList/thunk";
import { Button, Grid, TextField } from "@mui/material";

export function GenDishDetail() {
  const dispatch = useDispatch();
  const mayBeDish = useSelector((state: IRootState) =>
    state.menuList.selectedDishId
      ? state.menuList.allDishes.find(
          (dish) => dish.id === state.menuList.selectedDishId
        )
      : null
  );

  if (!mayBeDish) {
    return <div>No dish Selected</div>;
  }
  const targetDish = mayBeDish;
  console.log("targetDish:", targetDish);

  function Field(
    feature: keyof MenuState,
    field: string,
    onChange?: (value: string) => void
  ) {
    let id = "dish-detail--" + field;
    return (
      <Grid>
        <Grid sx={{ mb: 1.5 }}>
          <TextField
            // readOnly={!onChange}
            size="small"
            onChange={(e) => {
              console.log("changing:", e.currentTarget.value);
              onChange?.(e.currentTarget.value);
            }}
            type="text"
            id={id}
            value={targetDish[feature]}
            placeholder={`輸入${field}`}
            label={field}
          />
        </Grid>
      </Grid>
    );
  }
  function ImageField(
    field: keyof MenuState,
    onChange?: (value: File) => void
  ) {
    let id = "dish-detail--" + field;

    return (
      <Grid sx={{textAlign:'center'}}>
        <Button variant="outlined" component="label">
          更改圖片
          <input
            // readOnly={!onChange}
            hidden
            onChange={(e) => {
              let file = e.currentTarget.files?.[0];
              if (!file) {
                return;
              }
              console.log("changing:", file);
              onChange?.(file);
            }}
            accept="image/*"
            type="file"
            id={id}
            name="img"
          />
        </Button>
      </Grid>
    );
  }

  return (
    <div>
      <Grid sx={{ px: 6, py: 4 }}>
        <h2>編輯菜式資料</h2>
        <Grid>
          {Field("id", "編號")}
          {Field("name", "名稱", (name) =>
            dispatch(updateNameThunk(targetDish.id, name))
          )}
          {Field("price", "價錢", (price) =>
            dispatch(
              // updatePriceThunk(targetDish.id, price.toString() as string)
              updatePriceThunk(targetDish.id, price)
            )
          )}
          {Field("category", "種類", (category) =>
            dispatch(updateCategoryThunk(targetDish.id, category))
          )}
          {ImageField("img", (img: File) =>
            dispatch(updateImgThunk(targetDish.id, img))
          )}
        </Grid>
      </Grid>
    </div>
  );
}

// const theme = createTheme({
//   breakpoints: {
//     values: {
//  // small phone
//       xs: 300, // phone
//       sm: 600, // tablets
//       md: 900, // small laptop
//       lg: 1200, // desktop
//       xl: 1536 // large screens
//     }
//   }
// });

// <ThemeProvider theme={theme}></ThemeProvider>
