import { useDispatch, useSelector } from "react-redux";
import { selectDishAction } from "../redux/manaMenuList/action";
import Box from "@mui/material/Box";
import DeleteIcon from "@mui/icons-material/Delete";
import { Button, Card, CardMedia, Tabs } from "@mui/material";
import React, { useEffect, useState } from "react";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import { Grid } from "@mui/material";
import { MenuState } from "../redux/manaMenuList/state";
import { IRootState } from "../redux/store";
import {
  deleteDishThunk,
  getAllDishesThunk,
} from "../redux/manaMenuList/thunk";
import { styled } from "@mui/material/styles";
import { AddNewDish } from "./addNewDish";
// import Button from '@mui/material/Button';
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
// import DialogContent from '@mui/material/DialogContent';
// import DialogContentText from '@mui/material/DialogContentText';
// import DialogTitle from '@mui/material/DialogTitle';
import Slide from "@mui/material/Slide";
import { TransitionProps } from "@mui/material/transitions";
import { GenDishDetail } from "./manaMenuDetail";
import { createTheme } from "@mui/material";

const { REACT_APP_API_SERVER } = process.env;

const theme = createTheme({
  palette: {
    secondary: {
      main: "#388e3c",
    },
  },
});

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const Img = styled("img")({
  margin: "auto",
  display: "block",
  maxWidth: "100%",
  maxHeight: "100%",
});

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`horizontal-tabpanel-${index}`}
      aria-labelledby={`horizontal-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `vertical-tab-${index}`,
    "aria-controls": `vertical-tabpanel-${index}`,
  };
}

export function GenMenuList() {
  const [valueCateIndex, setValueCateIndex] = React.useState(0);
  const [allDish, setAllDish] = useState<MenuState[]>([]);
  const [categories, setCategories] = useState<string[]>([]);
  const [open, setOpen] = React.useState(false);
  const dishes = useSelector((state: IRootState) => state.menuList.allDishes);

  const dispatch = useDispatch();

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    // console.log("selected tab index", newValue);
    setValueCateIndex(newValue); //what is this for?
  };

  const handleClose = () => {
    setOpen(false);
  };

  const selectItem = (id: number) => {
    setOpen(true);
    dispatch(selectDishAction(id));
    // console.log("selected????:", dispatch(selectDishAction(id)).id);
  };

  const deleteDish = (id: number) => {
    dispatch(deleteDishThunk(id));
  };

  useEffect(() => {
    if (!dishes) {
      setAllDish([]);
      return;
    }
    let uniqueCategory = Array.from(
      new Set(dishes.map((dish) => dish.category) as string[])
    );
    console.log("uniqueCategory:", uniqueCategory);
    setCategories(uniqueCategory);

    let allDish: any = [];
    for (let cate of uniqueCategory) {
      allDish.push(dishes.filter((dish: any) => dish.category === cate));
    }
    // console.log("allllllllllDish", allDish);
    setAllDish(allDish);
  }, [dishes, deleteDishThunk, selectDishAction]);

  useEffect(() => {
    dispatch(getAllDishesThunk());
  }, []);

  //index=categoryIndex
  function GenMenuDetail({ data, index }: any) {
    // console.log("data, index:", { data, index });
    return (
      <TabPanel value={valueCateIndex} index={index}>
        {data.map((dish: any) => (
          <Card
            key={dish.id}
            sx={{ bgcolor: "background.paper", borderRadius: "16px", m: 1 }}
          >
            <Grid
              sx={{
                display: "flex",
                justifyContent: "space-around",
                alignItems: "center",
              }}
            >
              <Grid
                item
                xs={4}
                sx={{
                  borderColor: "divider",
                  display: "flex",
                  justifyContent: "center",
                }}
              >
                <CardMedia
                  style={{
                    width: "auto",
                    maxHeight: "80px",
                    padding: "20px",
                  }}
                  component="img"
                  image={`${REACT_APP_API_SERVER}/` + dish.img}
                ></CardMedia>
                {/* <img className="dishImg" src={`${REACT_APP_API_SERVER}/${dish.img}`} /> */}
              </Grid>
              <Grid
                item
                xs={4}
                sx={{ borderRight: 1, borderLeft: 1, borderColor: "divider" }}
              >
                <Grid sx={{ m: 1 }}>{dish.name}</Grid>
                <Grid sx={{ m: 1 }}>$ {dish.price}</Grid>
              </Grid>
              <Grid item xs={4} sx={{}}>
                <Button
                  sx={{ m: 1 }}
                  variant="outlined"
                  size="small"
                  color="primary"
                  onClick={() => selectItem(dish.id)}
                >
                  Edit
                </Button>
                <Button
                  sx={{ m: 1 }}
                  startIcon={<DeleteIcon />}
                  variant="outlined"
                  size="small"
                  color="error"
                  onClick={() => deleteDish(dish.id)}
                >
                  Delete
                </Button>
              </Grid>
            </Grid>
          </Card>
        ))}
      </TabPanel>
    );
  }

  function GenMenuList({ dataArr }: any): any {
    // console.log("dataArr:", dataArr);
    return dataArr.map((dish: any, i: number) => (
      <GenMenuDetail key={dish.name} data={dish} index={i} />
    ));
  }

  return (
    <>
      <AddNewDish />
      <Box
        sx={{
          maxWidth: { xs: 300, sm: 580 },
          flexGrow: 1,
          bgcolor: "background.paper",
          display: "inline-grid",
          width: 500,
        }}
      >
        <Tabs
          orientation="horizontal"
          variant="scrollable"
          scrollButtons
          value={valueCateIndex}
          aria-label="scrollable force tabs example"
          sx={{ borderColor: "divider" }}
          onChange={handleChange}
        >
          {categories.map((category: any, i: number) => (
            <Tab key={category} label={category} {...a11yProps(i)} />
          ))}
        </Tabs>
      </Box>

      <GenMenuList dataArr={allDish} />
      <div>
        <Dialog
          open={open}
          TransitionComponent={Transition}
          keepMounted
          onClose={handleClose}
          aria-describedby="alert-dialog-slide-description"
        >
          <GenDishDetail />
          <DialogActions>
            <Button onClick={handleClose}>返回</Button>
            <Button onClick={handleClose}>確認</Button>
          </DialogActions>
        </Dialog>
      </div>
    </>
  );
}

{
  /* // const theme = createTheme({
//   breakpoints: {
//     values: {
//  // small phone
//       xs: 300, // phone
//       sm: 600, // tablets
//       md: 900, // small laptop
//       lg: 1200, // desktop
//       xl: 1536 // large screens
//     }
//   }
// }); */
}
