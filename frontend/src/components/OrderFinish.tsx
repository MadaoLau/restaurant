import { Box, Grid } from "@mui/material";
import { Item } from "./OrderByTable";

function OrderFinishHelper({ data }: any) {
  return (
    <Grid item md={3}>
      <Item
        sx={{
          bgcolor: "warning.light",
          color: "black",
        }}
      >
        <Box
          sx={{
            fontWeight: "bold",
            fontSize: "3em",
          }}
        >
          {data.name}號枱
        </Box>
        {data.value.map((v: any) => (
          <Box
            key={v.id}
            sx={{
              fontWeight: "bold",
              fontSize: "1.5em",
            }}
          >
            <span>
              {" "}
              {v.name} : {v.total_quantity}{" "}
            </span>
          </Box>
        ))}
      </Item>
    </Grid>
  );
}

function OrderFinish({ data }: { data: any }): any {
  let newData: any = new Map();

  data.forEach((v: any, i: number) => {
    if (newData.has(v.table_no)) {
      let temp = newData.get(v.table_no);
      temp.push({
        name: v.name,
        id: v.id,
        total_quantity: v.total_quantity,
        client_id: v.client_id,
      });
      newData.set(v.table_no, temp);
    } else {
      newData.set(v.table_no, [
        {
          name: v.name,
          id: v.id,
          total_quantity: v.total_quantity,
          client_id: v.client_id,
        },
      ]);
    }
  });

  // turn map to a normal array
  const arr = [...newData].map(([name, value]) => ({ name, value }));

  return arr.map((item: any) => (
    <OrderFinishHelper data={item} key={item.index} />
  ));
}

export default OrderFinish;
