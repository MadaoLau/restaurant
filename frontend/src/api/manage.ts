const { REACT_APP_API_SERVER } = process.env

export async function fetchGetAllDishes() {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/manageMenu`);
  return res;
}

export async function fetchAddDish(newDish: FormData) {
  console.log("fetch newDish", newDish);

  const res = await fetch(`${REACT_APP_API_SERVER}/api/manageMenu`, {
    method: "POST",
    // headers: {
    //   // "Content-Type": "application/json",
    //   // "Content-Type": "multipart/form-data",
    // },
    // body: JSON.stringify(newDish),
    body: newDish,
  });
  return res;
}

// export async function fetchUpdateName(targetDish: MenuState) {
export async function fetchUpdate(data: any) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/manageMenu`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data),
  });
  return res;
}

export async function fetchDeleteDish(id: number) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/manageMenu`, {
    method: "DELETE",
    body: JSON.stringify({ id }),
    headers: {
      "Content-Type": "application/json",
    },
  });
  return res;
}

export async function fetchUpdateImg(data: FormData) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/manageMenu/updateImg`, {
    method: "PUT",
    body: data,
  });
  return res;
}

