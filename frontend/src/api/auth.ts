
const { REACT_APP_API_SERVER } = process.env
export async function fetchLogin(username: string, password: string) {
    const res = await fetch(`${REACT_APP_API_SERVER}/api/user/login`, {
        method: "POST",
        headers : {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            username,
            password
        })
    })
    return res;
}

export async function fetchRegister(
    username: string, 
    password: string,
    email: string,
    gender: string,
    contact: string,
    ) {
    const res = await fetch(`${REACT_APP_API_SERVER}/api/user/register`, {
        method: "POST",
        headers : {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            username,
            password,
            email,
            gender,
            contact,
        })
    })
    return res;
}

export async function fetchGetStaff(){
    const res = await fetch(`${REACT_APP_API_SERVER}/api/user/staff`);
    return res;
}

export async function fetchDeleteStaff(staffId:number){
    const res = await fetch(`${REACT_APP_API_SERVER}/api/user/staff/${staffId}`,{
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
        },
        body:JSON.stringify({
            staffId: staffId
        }),
    })
    return res;
}

export async function fetchUpdateStaff(
    staffId:number,
    username: string, 
    email: string,
    gender: string,
    status: string,
    contact: string,
    shift: string,
    is_admin: string
    ){
        const res = await fetch(`${REACT_APP_API_SERVER}/api/user/staff/${staffId}`,{
            method: "PUT",
            headers:{
                "Content-Type": "application/json",
            },
            body:JSON.stringify({
                staffId,
                username,
                email,
                gender,
                status,
                contact,
                shift,
                is_admin
            })
        })
        return res
}

//Beeno ver

// async function handleResponse(resP: Promise<Response>) {
//     try {
//       let res = await resP
//       let json = await res.json()
//       return json
//     } catch (error) {
//       return { error: String(error) }
//     }
//   }
  
//   export function post(url: string, body?: any) {
//     let resP = fetch(REACT_APP_API_SERVER + url, {
//       method: 'POST',
//       headers: {
//         'Content-Type': 'application/json',
//       },
//       body: JSON.stringify({body}),
//     })
//     return handleResponse(resP)
//   }

//   export async function postWithToken(
//     token: string | null | undefined,
//     url: string,
//     body?: any,
//   ) {
//     if (!token) {
//       return { error: 'This API is not available to guest' }
//     }
//     let resP = fetch(`${REACT_APP_API_SERVER}/api/user/login`, {
//       method: 'POST',
//       headers: {
//         'Content-Type': 'application/json',
//         Authorization: 'Bearer ' + token,
//       },
//       body: JSON.stringify(body),
//     })
//     return handleResponse(resP)
//   }
  
//   export async function get(url: string) {
//     let resP = fetch(`${REACT_APP_API_SERVER}/api/user/login`, {
//       method: 'GET',
//     })
//     return handleResponse(resP)
//   }
  
//   export async function getWithToken(
//     token: string | null | undefined,
//     url: string,
//   ) {
//     if (!token) {
//       return { error: 'This API is not available to guest' }
//     }
//     let resP = fetch(`${REACT_APP_API_SERVER}/api/user/login`, {
//       method: 'GET',
//       headers: {
//         Authorization: 'Bearer ' + token,
//       },
//     })
//     return handleResponse(resP)
//   }
