import { DishList } from "../redux/order/state";

const { REACT_APP_API_SERVER } = process.env;

export async function fetchGetAllDishes() {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/order`);
  console.log(`${REACT_APP_API_SERVER}/api/order`);
  return res;
}

export async function fetchSendOrder(
  newOrder: DishList,
  tableNo: string,
  clientId: string,
  specialReq:string,
) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/order/sendOrder`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ newOrder, tableNo, clientId,specialReq }),
  });
  if (res.ok) {
    await fetch(`${REACT_APP_API_SERVER}/api/order/getOrder`);
    await fetch(`${REACT_APP_API_SERVER}/api/order/getClientOrder/${clientId}`);
    return;
  }
}

export async function fetchFinishOrder(clientId: string) {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/order/finishOrder`, {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ clientId }),
  });
  if (res.ok) {
    const getRes = await fetch(`${REACT_APP_API_SERVER}/api/order/getOrder`);
    const result = await getRes.json();
    console.log(result);
    return result;
  }
}

export async function fetchGetAllDishesToKitchen() {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/order/getOrder`);
  console.log(`${REACT_APP_API_SERVER}/api/order`);
  return res;
}

export async function fetchFinishOrderToKitchen() {
  const res = await fetch(`${REACT_APP_API_SERVER}/api/order/getFinishOrder`);
  const result = await res.json();
  return result;
}

export async function fetchGetOrderToClient(clientId: string) {
  const res = await fetch(
    `${REACT_APP_API_SERVER}/api/order/getClientOrder/${clientId}`
  );
  const result = await res.json();
  console.log('client order sent from server:', result);
  return result;
}

export async function fetchDeleteOrder(clientId: string) {
  const res = await fetch(
    `${REACT_APP_API_SERVER}/api/order/deleteOrder/${clientId}`,
    {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        clientId: clientId,
      }),
    }
  );
  return res;
}

export async function fetchDeleteOrderEachDish(
  clientId: string,
  dishId: number
) {
  const res = await fetch(
    `${REACT_APP_API_SERVER}/api/order/deleteOrderByEachDish/${clientId}`,
    {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        clientId: clientId,
        dishId: dishId,
      }),
    }
  );
  return res;
}

