const { REACT_APP_API_SERVER } = process.env;

export async function fetchGetAllFeedback() {
    const res = await fetch(`${REACT_APP_API_SERVER}/api/feedback`);
    const result = await res.json();
    return result;
}

export async function fetchGetScore() {
    const res = await fetch(`${REACT_APP_API_SERVER}/api/feedback/score`);
    const result = await res.json();
    return result;
}

export async function fetchSortingFeedback(sorting:string) {
    const res = await fetch(`${REACT_APP_API_SERVER}/api/feedback/sorting`,{
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ 
            sorting
        }),
    });
    return res;
}

export async function fetchSelectFeedback(select:string) {
    const res = await fetch(`${REACT_APP_API_SERVER}/api/feedback/select`,{
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ 
            select
        }),
    });
    return res;
}

export async function fetchPostFeedback(food_quality:string, service:string, cleanness:string, suggestion:string, client_id:string) {
    const res = await fetch(`${REACT_APP_API_SERVER}/api/feedback`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ 
            food_quality,
            service,
            cleanness,
            suggestion,
            client_id
        }),
    });
    if(res.ok){
        console.log("Fetch Post Feedback Success")
        return res;
    }
}

export async function fetchDeleteFeedbackById(clientId:string){
    const res = await fetch(`${REACT_APP_API_SERVER}/api/feedback`,{
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
        },
        body:JSON.stringify({
            clientId: clientId
        }),
    })
    return res;
}